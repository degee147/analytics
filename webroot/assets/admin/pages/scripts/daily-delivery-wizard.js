var FormWizard = function () {


	return {
		//main function to initiate the module
		init: function () {
			if (!jQuery().bootstrapWizard) {
				return;
			}


			var form = $('#submit_form');
			var error = $('.alert-danger', form);
			var success = $('.alert-success', form);

			form.validate({
				doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
				errorElement: 'span', //default input error message container
				errorClass: 'help-block help-block-error', // default input error message class
				focusInvalid: false, // do not focus the last invalid input
				rules: {
					//account
					firstname: {
						minlength: 3,
						required: true
					},

				},

				messages: { // custom messages for radio buttons and checkboxes
					'payment[]': {
						required: "Please select at least one option",
						minlength: jQuery.validator.format("Please select at least one option")
					}
				},

				errorPlacement: function (error, element) { // render error placement for each input type
					if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
						error.insertAfter("#form_gender_error");
					} else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
						error.insertAfter("#form_payment_error");
					} else {
						error.insertAfter(element); // for other inputs, just perform default behavior
					}
				},

				invalidHandler: function (event, validator) { //display error alert on form submit
					success.hide();
					error.show();
					Metronic.scrollTo(error, -100);
				},

				highlight: function (element) { // hightlight error inputs
					$(element)
						.closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
				},

				unhighlight: function (element) { // revert the change done by hightlight
					$(element)
						.closest('.form-group').removeClass('has-error'); // set error class to the control group
				},

				success: function (label) {
					if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
						label
							.closest('.form-group').removeClass('has-error').addClass('has-success');
						label.remove(); // remove error label here
					} else { // display success icon for other inputs
						label
							.addClass('valid') // mark the current input as valid and display OK icon
							.closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
					}
				},

				submitHandler: function (form) {
					//success.show();
					error.hide();
					//add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax

					//                    form.submit();



				}

			});

			var displayConfirm = function() {

				optionsConfirm();
				/*
                $('#tab6 .form-control-static', form).each(function(){
                    // var input = $('[name="'+$(this).attr("data-display")+'"]', form);
                    var target = $(this).attr("data-display");

                    // console.log(target);
                    //  displayItemConfirm(target);


                    $(this).html(displayItemConfirm(target));

                });
*/
			}

			var handleTitle = function(tab, navigation, index) {
				var total = navigation.find('li').length;
				var current = index + 1;
				// set wizard title
				$('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
				// set done steps
				jQuery('li', $('#form_wizard_1')).removeClass("done");
				var li_list = navigation.find('li');
				for (var i = 0; i < index; i++) {
					jQuery(li_list[i]).addClass("done");
				}

				if (current == 1) {
					$('#form_wizard_1').find('.button-previous').hide();
				} else {
					$('#form_wizard_1').find('.button-previous').show();
				}

				if (current >= total) {
					$('#form_wizard_1').find('.button-next').hide();
					$('#form_wizard_1').find('.button-submit').show();
					$('#button-submit-main').show();

					displayConfirm();

				} else {
					$('#form_wizard_1').find('.button-next').show();
					$('#form_wizard_1').find('.button-submit').hide();
					$('#button-submit-main').hide();
				}
				Metronic.scrollTo($('.page-title'), -100);
			}

			// default form wizard
			$('#form_wizard_1').bootstrapWizard({
				'nextSelector': '.button-next',
				'previousSelector': '.button-previous',
				onTabClick: function (tab, navigation, index) {
					//                    return false;
					return true;

					/*success.hide();
                    error.hide();
                    if (form.valid() == false) {
                        return false;
                    }
                    handleTitle(tab, navigation, clickedIndex);*/

				},
				onNext: function (tab, navigation, index) {
					/* console.log(tab);
                    console.log(navigation);*/
					//saveCurrentDay(day);
					success.hide();
					error.hide();

					if (form.valid() == false) {
						return false;
					}
					//alert(index);
					saveCurrentDay(index);
					handleTitle(tab, navigation, index);
				},
				onPrevious: function (tab, navigation, index) {
					success.hide();
					error.hide();

					handleTitle(tab, navigation, index);
				},
				onTabShow: function (tab, navigation, index) {

					$('#form_wizard_1').find('.days_index').hide();
					$('#form_wizard_1').find('.index'+index).show();

					$('.select2_sample').select2({
						placeholder: "Select or search for item",
						allowClear: true,
						maximumSelectionLength: 7,
					});

					var total = navigation.find('li').length;
					var current = index + 1;
					var $percent = (current / total) * 100;
					$('#form_wizard_1').find('.progress-bar').css({
						width: $percent + '%'
					});
					//                    alert(index);
					if(index == 5){
						//displayConfirm();
					}

					handleTitle(tab, navigation, index);
				}
			});

			$('#form_wizard_1').find('.button-previous').hide();
			$('#form_wizard_1 .button-submit').click(function (e) {
				e.preventDefault();
				//alert('Finished! Hope you like it :)');
				saveAll();
			}).hide();

			//apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
			$('#country_list', form).change(function () {
				form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
			});
		}

	};

}();

