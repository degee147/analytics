var FormWizard = function () {


    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().bootstrapWizard) {
                return;
            }


            var form = $('#submit_form');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);

            form.validate({
                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    //account
                    firstname: {
                        minlength: 3,
                        required: true
                    },

                },

                messages: { // custom messages for radio buttons and checkboxes
                    'payment[]': {
                        required: "Please select at least one option",
                        minlength: jQuery.validator.format("Please select at least one option")
                    }
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form_gender_error");
                    } else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
                        error.insertAfter("#form_payment_error");
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit
                    success.hide();
                    error.show();
                    Metronic.scrollTo(error, -100);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                        label
                            .closest('.form-group').removeClass('has-error').addClass('has-success');
                        label.remove(); // remove error label here
                    } else { // display success icon for other inputs
                        label
                            .addClass('valid') // mark the current input as valid and display OK icon
                            .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    }
                },

                submitHandler: function (form) {
                    //success.show();
                    error.hide();
                    //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax

                    //                    form.submit();

                    var breakfast_menu = $('select[name^="breakfast_menu"]').val();
                    var breakfast_custom = $('select[name^="breakfast_custom"]').val();
                    var breakfast_products = [];
                    $('select[name^="breakfast_products"] :selected').each(function(i, selected){
                        if($(selected).val().trim()){
                            breakfast_products[i] = $(selected).val();
                        }
                    });

                    if(!breakfast_menu.trim() && !breakfast_custom.trim() && jQuery.isEmptyObject(breakfast_products) ){
                        toastr.error("You haven't selected a breakfast menu", 'Error');
                        return;
                    }

                    var lunch_menu = $('select[name^="lunch_menu"]').val();
                    var lunch_custom = $('select[name^="lunch_custom"]').val();
                    var lunch_products = [];
                    $('select[name^="lunch_products"] :selected').each(function(i, selected){
                        lunch_products[i] = $(selected).val();
                    });

                    if(!lunch_menu.trim() && !lunch_custom.trim() && jQuery.isEmptyObject(lunch_products) ){
                        toastr.error("You haven't selected a lunch menu", 'Error');
                        return;
                    }

                    var dinner_menu = $('select[name^="dinner_menu"]').val();
                    var dinner_custom = $('select[name^="dinner_custom"]').val();
                    var dinner_products = [];
                    $('select[name^="dinner_products"] :selected').each(function(i, selected){
                        dinner_products[i] = $(selected).val();
                    });
                    if(!dinner_menu.trim() && !dinner_custom.trim() && jQuery.isEmptyObject(dinner_products) ){
                        toastr.error("You haven't selected a dinner menu", 'Error');
                        return;
                    }

                    // this calls the ajaxSubmit function in the script tag in daily view of Default deliveery add
                    // the name on the left hand side of the params data is so the ajax method in the controller can easily pick up the data
                    ajaxSubmit({
                        "day":"all",
                        "mode":"meal",

                        "var_1_menu": breakfast_menu,
                        "var_1_custom": breakfast_custom,
                        "var_1_products": breakfast_products,
                        "var_2_menu": lunch_menu,
                        "var_2_custom": lunch_custom,
                        "var_2_products": lunch_products,
                        "var_3_menu": dinner_menu,
                        "var_3_custom": dinner_custom,
                        "var_3_products": dinner_products
                    });


                }

            });

            var displayConfirm = function() {
                $('#tab2 .form-control-static', form).each(function(){
                    var input = $('[name="'+$(this).attr("data-display")+'"]', form);
                    if (input.is(":radio")) {
                        input = $('[name="'+$(this).attr("data-display")+'"]:checked', form);
                    }
                    if (input.is(":text") || input.is("textarea")) {
                        $(this).html(input.val());
                        $(this).append("&nbsp;");
                    } else if (input.is("select[multiple]")) {
                        var text = "";
                        var total = input.find('option:selected').length;
                        input.find('option:selected').each(function(i, selected){
                            //  console.log(total);
                            if($(selected).text().trim()){
                                if (i === total - 1) {
                                    //this is the last one
                                    text += $(selected).text();
                                }else{
                                    text += $(selected).text() + ", ";
                                }
                            }
                        });
                        if(text == ""){
                            $(this).html('<span class="label label-info">N/A</span>');
                        }else{
                            $(this).html(text);
                        }
                        $(this).append("&nbsp;");
                    } else if (input.is("select")) {
                        if(input.find('option:selected').text() == ""){
                            $(this).html('<span class="label label-info">N/A</span>');
                        }else{
                            // $(this).html('<span class="label label-success">'+input.find('option:selected').text()+'</span>');
                            $(this).html(input.find('option:selected').text());
                        }
                        $(this).append("&nbsp;");
                    } else if (input.is(":radio") && input.is(":checked")) {
                        $(this).html(input.attr("data-title"));
                        $(this).append("&nbsp;");
                    } else if (input.is(":radio") && input.is(":checked")) {
                        $(this).html(input.attr("data-title"));
                        $(this).append("&nbsp;");
                    }  else if ($(this).attr("data-display") == 'payment[]') {
                        var payment = [];
                        $('[name="payment[]"]:checked', form).each(function(){
                            payment.push($(this).attr('data-title'));
                        });
                        $(this).html(payment.join("<br>"));
                    }
                });
            }

            var handleTitle = function(tab, navigation, index) {
                var total = navigation.find('li').length;
                var current = index + 1;
                // set wizard title
                $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
                // set done steps
                jQuery('li', $('#form_wizard_1')).removeClass("done");
                var li_list = navigation.find('li');
                for (var i = 0; i < index; i++) {
                    jQuery(li_list[i]).addClass("done");
                }

                if (current == 1) {
                    $('#form_wizard_1').find('.button-previous').hide();
                } else {
                    $('#form_wizard_1').find('.button-previous').show();
                }

                if (current >= total) {
                    $('#form_wizard_1').find('.button-next').hide();
                    $('#form_wizard_1').find('.button-submit').show();
                    //   $('#button-submit-main').show();
                    displayConfirm();
                } else {
                    $('#form_wizard_1').find('.button-next').show();
                    $('#form_wizard_1').find('.button-submit').hide();
                    //  $('#button-submit-main').hide();
                }
                Metronic.scrollTo($('.page-title'), -100);
            }

            // default form wizard
            $('#form_wizard_1').bootstrapWizard({
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onTabClick: function (tab, navigation, index, clickedIndex) {
                    return false;
                    //                    return true;

                    /*success.hide();
                    error.hide();
                    if (form.valid() == false) {
                        return false;
                    }
                    handleTitle(tab, navigation, clickedIndex);*/

                },
                onNext: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    if (form.valid() == false) {
                        return false;
                    }

                    handleTitle(tab, navigation, index);
                },
                onPrevious: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    handleTitle(tab, navigation, index);
                },
                onTabShow: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    var $percent = (current / total) * 100;
                    $('#form_wizard_1').find('.progress-bar').css({
                        width: $percent + '%'
                    });
                }
            });

            $('#form_wizard_1').find('.button-previous').hide();
            $('#form_wizard_1 .button-submit').click(function () {



                //alert('Finished! Hope you like it :)');
            }).hide();

            //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
            $('#country_list', form).change(function () {
                form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });
        }

    };

}();

