<?php
namespace App\Test\TestCase\Controller;

use App\Controller\AtmChannelsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\AtmChannelsController Test Case
 */
class AtmChannelsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.atm_channels'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
