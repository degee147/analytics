<?php
namespace App\Test\TestCase\Controller;

use App\Controller\FraudAnalysisController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\FraudAnalysisController Test Case
 */
class FraudAnalysisControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fraud_analysis'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
