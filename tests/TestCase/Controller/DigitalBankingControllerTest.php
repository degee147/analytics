<?php
namespace App\Test\TestCase\Controller;

use App\Controller\DigitalBankingController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\DigitalBankingController Test Case
 */
class DigitalBankingControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.digital_banking'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
