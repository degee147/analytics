<?php
namespace App\Test\TestCase\Controller;

use App\Controller\CardUsageController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\CardUsageController Test Case
 */
class CardUsageControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.card_usage'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
