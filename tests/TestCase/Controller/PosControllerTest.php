<?php
namespace App\Test\TestCase\Controller;

use App\Controller\PosController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\PosController Test Case
 */
class PosControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.pos'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
