<?php
namespace App\Test\TestCase\Controller;

use App\Controller\AllChannelsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\AllChannelsController Test Case
 */
class AllChannelsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.all_channels'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
