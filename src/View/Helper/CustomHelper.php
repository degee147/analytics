<?php
namespace App\View\Helper;

use App\Utility\Custom;
use Cake\View\Helper;
use Cake\View\View;
use DateTime;
use Cake\I18n\Time;

//for use with custom utility

/**
 * Custom helper
 */
class CustomHelper extends Helper
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public $helpers = ['Html', 'Url'];

    public $customUtil; //for use with custom utility



    public function getPictureDisplayUrl($image_url, $size = null)
    {
        if (!empty($image_url)) {
            if (file_exists(WWW_ROOT . 'uploads' . DS . 'profile_pictures' . DS . $image_url)) {
                return $this->Url->build(DS, true) . 'uploads' . DS . 'profile_pictures' . DS . $image_url;
            }
            if ($this->remoteFileExists($image_url)) {
                return $image_url;
            }
        }
        return 'http://www.placehold.it/' . (!empty($size) ? $size : '150x150') . '/EFEFEF/AAAAAA&amp;text=no+image';

    }
    public function remoteFileExists($url)
    {
        if (@get_headers($url)[0] == 'HTTP/1.1 404 Not Found') {
            // The image doesn't exist
            return false;
        } else {
            // The image exists
            return true;
        }

    }

    // Execute any other additional setup for your helper.
    public function initialize(array $config)
    {
        $this->customUtil = new Custom();
    }

    public function canShowLogLink($log)
    {
        return $this->customUtil->canShowLogLink($log);
    }

    public function getLogLinkParams($log)
    {
        return $this->customUtil->getLogLinkParams($log);
    }

    public function showLogLevel($level)
    {

        if ($level == 1) {
            return '<span class="label label-info">Info</span>';
        } elseif ($level == 2) {
            return '<span class="label label-warning">Warning</span>';
        } elseif ($level == 3) {
            return '<span class="label label-danger">Danger</span>';
        } else {
            return $level;
        }
    }

    public function showDrives()
    {
        $settings = [];
        $settings['show']['hd'] = true;
        $info = [];
        $info['HD'] = $this->_View->get('hd');

        if (!empty($settings['show']['hd'])) {

            // Should we not show the Reads and Writes columns?
            // $show_stats = array_key_exists('drives_rw_stats', $info['contains']) ? ($info['contains']['drives_rw_stats'] === false ? false : true) : true;
            $show_stats = false;

            // Or vendor columns?
            // $show_vendor = array_key_exists('drives_vendor', $info['contains']) ? ($info['contains']['drives_vendor'] === false ? false : true) : true;
            $show_vendor = false;

            echo '
		<div class="infoTable">
			<h4>Drives</h4>
			<table class="table table-condensed table-hover">
				<tr>
					<th>Path</th>
					', $show_vendor ? '<th>Vendor' : '', '</th>
					<th>Name</th>
					', $show_stats ? '<th>Reads</th>
					<th>Writes</th>' : '', '
					<th>Size</th>
				</tr>';
            if (count($info['HD']) > 0) {
                foreach ($info['HD'] as $drive) {
                    echo '
				<tr>
					<td>' . $drive['device'] . '</td>
					', $show_vendor ? '<td>' . ($drive['vendor'] ? $drive['vendor'] : "Unknown") . '</td>' : '', '
					<td>', $drive['name'] ? $drive['name'] : "Unknown", '</td>
					', $show_stats ? '<td>' . ($drive['reads'] !== false ? number_format($drive['reads']) : "Unknown") . '</td>
					<td>' . ($drive['writes'] !== false ? number_format($drive['writes']) : "Unknown") . '</td>' : '', '
					<td>', $drive['size'] ? $this->byteConvert($drive['size']) : "Unknown", '</td>
				</tr>';

                    // If we've got partitions for this drive, show them too
                    if (array_key_exists('partitions', $drive) && is_array($drive['partitions']) && count($drive['partitions']) > 0) {
                        echo '
				<tr>
					<td colspan="6">';

                        // Each
                        foreach ($drive['partitions'] as $partition) {
                            //echo '&#9492; ' . (isset($partition['number']) ? $drive['device'] . $partition['number'] : $partition['name']) . ' - ' . $this->byteConvert($partition['size']) . '<br />';
                        }

                        echo '
					</td>
				</tr>
				';
                    }
                }
            } else {
                echo '<tr><td colspan="6" class="none">None Found</td></tr>';
            }

            echo '
			</table>
		</div>';
        }

    }

    public function showfileSystems()
    {
        $settings = [];
        $settings['show']['mounts_options'] = false;
        $mounts = $this->_View->get('mounts');
        // Show file system mounts?
        if (!empty($mounts)) {
            $has_devices = false;
            $has_labels = false;
            $has_types = false;
            foreach ($mounts as $mount) {
                if (!empty($mount['device'])) {
                    $has_devices = true;
                }
                if (!empty($mount['label'])) {
                    // $has_labels = true;
                }
                if (!empty($mount['devtype'])) {
                    $has_types = true;
                }
            }
            $addcolumns = 0;
            if ($settings['show']['mounts_options']) { //$settings['show']['mounts_options'] //defaults to false
                $addcolumns++;
            }
            if ($has_devices) {
                $addcolumns++;
            }
            if ($has_labels) {
                $addcolumns++;
            }
            if ($has_types) {
                $addcolumns++;
            }
            echo '
<div class="infoTable filesystem_mounts">
	<h4>File Systems</h4>
	<table class="table table-condensed table-hover">
		<tr>';
            if ($has_types) {
                echo '<th>Type</th>';
            }
            if ($has_devices) {
                echo '<th>Device</th>';
            }
            echo '<th>Mount Point</th>';
            if ($has_labels) {
                echo '<th>Label</th>';
            }
            echo '
			<th>File System</th>', $settings['show']['mounts_options'] ? '
			<th>Mount Options</th>' : '', '
			<th>Size</th>
			<th>Used</th>
			<th>Free</th>
			<th style="width: 12%;">Percentage</th>
		</tr>
		';

            // Calc totals
            $total_size = 0;
            $total_used = 0;
            $total_free = 0;

            // Don't add totals for duplicates. (same filesystem mount twice in different places)
            $done_devices = array();

            // Are there any?
            if (count($mounts) > 0) {

                // Go through each
                foreach ($mounts as $mount) {

                    // Only add totals for this device if we haven't already
                    if (!in_array($mount['device'], $done_devices)) {
                        $total_size += $mount['size'];
                        $total_used += $mount['used'];
                        $total_free += $mount['free'];
                        if (!empty($mount['device'])) {
                            $done_devices[] = $mount['device'];
                        }
                    }

                    // Possibly don't show this twice
                    elseif (array_key_exists('duplicate_mounts', $settings['show']) && empty($settings['show']['duplicate_mounts'])) {
                        continue;
                    }

                    // If it's an NFS mount it's likely in the form of server:path (without a trailing slash),
                    // but if the path is just / it likely just shows up as server:,
                    // which is vague. If there isn't a /, add one
                    if (preg_match('/^.+:$/', $mount['device']) == 1) {
                        $mount['device'] .= DIRECTORY_SEPARATOR;
                    }

                    echo '<tr>';
                    if ($has_types) {
                        echo '<td>' . $mount['devtype'] . '</td>';
                    }
                    if ($has_devices) {
                        echo '<td>' . $mount['device'] . '</td>';
                    }
                    echo '<td>' . $mount['mount'] . '</td>';
                    if ($has_labels) {
                        echo '<td>' . $mount['label'] . '</td>';
                    }
                    echo '
					<td>' . $mount['type'] . '</td>', $settings['show']['mounts_options'] ? '
					<td>' . (empty($mount['options']) ? '<em>unknown</em>' : '<ul><li>' . implode('</li><li>', $mount['options']) . '</li></ul>') . '</td>' : '', '
					<td>' . $this->byteConvert($mount['size']) . '</td>
					<td>' . $this->byteConvert($mount['used']) .
                    ($mount['used_percent'] !== false ? ' <span class="perc">(' . $mount['used_percent'] . '%)</span>' : '') . '</td>
					<td>' . $this->byteConvert($mount['free']) .
                    ($mount['free_percent'] !== false ? ' <span class="perc">(' . $mount['free_percent'] . '%)</span>' : '') . '</td>
					<td>
						' . $this->generateBarChart((int) $mount['used_percent'], $mount['used_percent'] ? $mount['used_percent'] . '%' : 'N/A') . '
					</td>
				</tr>';
                }
            } else {
                echo '<tr><td colspan="', 6 + $addcolumns, '" class="none">None found</td></tr>';
            }

            // Show totals and finish table
            $total_used_perc = $total_size > 0 && $total_used > 0 ? round($total_used / $total_size, 2) * 100 : 0;
            echo '
		<tr class="alt">
			<td colspan="', 2 + $addcolumns, '">Totals: </td>
			<td>' . $this->byteConvert($total_size) . '</td>
			<td>' . $this->byteConvert($total_used) . '</td>
			<td>' . $this->byteConvert($total_free) . '</td>
			<td>
				' . $this->generateBarChart($total_used_perc, $total_used_perc . '%') . '
			</td>
		</tr>
	</table>
</div>';
        }

    }

    /**
     * Create a progress bar looking thingy.
     * @param $percent
     * @param bool $text
     * @return string
     */
    public function generateBarChart($percent, $text = false)
    {
        return '
			<div class="new_bar_outer">
				<div class="new_bar_bg" style="width: ' . $percent . '%; "></div>
				<div class="new_bar_text">' . ($text ?: $percent . '%') . '</div>
			</div>
		';
    }

    // Convert bytes to stuff like KB MB GB TB etc
    public static function byteConvert($size, $precision = 2)
    {

        // Sanity check
        if (!is_numeric($size)) {
            return '?';
        }

        // Get the notation
        $notation = 1024; // Either 1024 or 1000; defaults to 1024

        // Fixes large disk size overflow issue
        // Found at http://www.php.net/manual/en/function.disk-free-space.php#81207
        $types = array('B', 'KB', 'MB', 'GB', 'TB');
        $types_i = array('B', 'KiB', 'MiB', 'GiB', 'TiB');
        for ($i = 0; $size >= $notation && $i < (count($types) - 1); $size /= $notation, $i++);

        return (round($size, $precision) . ' ' . ($notation == 1000 ? $types[$i] : $types_i[$i]));
    }

    public function showNextOrder($subscription_id)
    {
        /*
        return monday lunch
        Plan: Yes | N/A
        Price: 112 | N/A

         */
        if ($this->_View->get('subscriptions_next_order_prices')[$subscription_id]['active']) {

            $day = ucfirst($this->getDay($this->_View->get('subscriptions_next_order_prices')[$subscription_id]['next_day_id']));
            /*var_dump($this->_View->get('subscriptions_next_order_prices')); die();
            var_dump($this->_View->get('subscriptions_next_order_prices')[$subscription_id]); die();
            var_dump($day); die();*/
            $meal = ucfirst($this->getMealType($this->_View->get('subscriptions_next_order_prices')[$subscription_id]['next_meal_id']));
            $plan = $this->_View->get('subscriptions_next_order_prices')[$subscription_id]['has_plan'] ? 'Plan: <i class="fa fa-thumbs-o-up"></i>' : 'Plan: <i class="fa fa-thumbs-o-down"></i><br><span class="label label-warning">Order will not be created</span>';
            $price = $this->_View->get('subscriptions_next_order_prices')[$subscription_id]['price'] > 0 ? "Price:&nbsp;" . $this->_View->get('currency') . number_format($this->_View->get('subscriptions_next_order_prices')[$subscription_id]['price']) : null;

            //return $day . "<br>". $price;
            return $day . "&nbsp;" . $meal . "<br>" . $plan . "<br>" . $price;

        } else {

            if (!empty($this->_View->get('site_template')) && $this->_View->get('site_template') == "apex") {
                return '<span class="bg-danger" style="color: white; padding: 3px;">N/A</span>';
            }

            return '<span class="label label-danger">N/A</span>';
        }
    }

    public function showDefaultDeliveryTime($meal)
    {
        return $this->_View->get('siteDefaultTime')[$meal]->start . ' - ' . $this->_View->get('siteDefaultTime')[$meal]->end . '<br>(Default Time)';
    }

    public function showDeliveryTime($location, $meal)
    {
        $enabled = $meal . "_enabled";
        $default = "default_" . $meal . "_time";
        $start = $meal . "_start";
        $end = $meal . "_start";
        return !empty($location->$enabled) && $location->$enabled ? ($location->$default ? $this->showDefaultDeliveryTime($meal) : $location->$start . " - " . $location->$end) : (!empty($location->name) ? '<span class="label label-danger">Disabled In ' . $location->name . '</span>' : '<span class="label label-danger">No location selected</span>');
    }

    public function getNotificationClass($id = null)
    {
        if (!empty($id)) {
            if ($id == 1) {
                return 'info';
            }
            if ($id == 2) {
                return 'success';
            }
            if ($id == 3) {
                return 'warning';
            }
            if ($id == 4) {
                return 'danger';
            }
            return "";
        }
    }

    public function showLogIcon($id = null)
    {
        if ($id == 1) {
            return '<span class="label label-sm label-info"><i class="fa fa-bullhorn"></i></span>';
        }
        if ($id == 2) {
            return '<div class="label label-sm label-warning"><i class="fa fa-warning"></i></div>';
        }
        if ($id == 3) {
            return '<div class="label label-sm label-danger"><i class="fa fa-bolt"></i></div>';
        }
        return '<div class="label label-sm label-success"><i class="fa fa-bell-o"></i></div>';

    }

    public function showNotificationIcon($id = null)
    {

        if (!empty($id)) {

            if (!empty($this->_View->get('site_template')) && $this->_View->get('site_template') == "Apex") {

                if ($id == 1) {
                    return '<i class="ft-bell info float-left d-block font-large-1 mt-1 mr-2"></i>';
                }
                if ($id == 2) {
                    return '<i class="ft-bell success float-left d-block font-large-1 mt-1 mr-2"></i>';
                }
                if ($id == 3) {
                    return '<i class="ft-bell warning float-left d-block font-large-1 mt-1 mr-2"></i>';
                }
                if ($id == 4) {
                    return '<i class="ft-bell danger float-left d-block font-large-1 mt-1 mr-2"></i>';
                }

                return '<i class="ft-bell info float-left d-block font-large-1 mt-1 mr-2"></i>';
            } elseif (!empty($this->_View->get('site_template')) && $this->_View->get('site_template') == "Metronic4") {

                if ($id == 1) {
                    return '<span class="label label-sm label-icon label-info"><i class="fa fa-bullhorn"></i>&nbsp;</span>';
                }
                if ($id == 2) {
                    return '<span class="label label-sm label-icon label-success"><i class="fa fa-bell-o"></i>&nbsp;</span>';
                }
                if ($id == 3) {
                    return '<span class="label label-sm label-icon label-warning"><i class="fa fa-warning"></i>&nbsp;</span>';
                }
                if ($id == 4) {
                    return '<span class="label label-sm label-icon label-danger"><i class="fa fa-bolt"></i>&nbsp;</span>';
                }
                return '<span class="label label-sm label-icon label-success"><i class="fa fa-bell-o"></i>&nbsp;</span>';
            } else {

                if ($id == 1) {
                    return '<span class="label label-sm label-info"><i class="fa fa-bullhorn"></i></span>';
                }
                if ($id == 2) {
                    return '<div class="label label-sm label-success"><i class="fa fa-bell-o"></i></div>';
                }
                if ($id == 3) {
                    return '<div class="label label-sm label-warning"><i class="fa fa-warning"></i></div>';
                }
                if ($id == 4) {
                    return '<div class="label label-sm label-danger"><i class="fa fa-bolt"></i></div>';
                }
                return '<div class="label label-sm label-success"><i class="fa fa-bell-o"></i></div>';
            }
        }

    }

    /*
    Not usefull
     */
    public function checkActiveMenu($value, $class = null)
    {

        // debug($this->Url->build()); die();
        //if($this->Url->build() == $value){
        if ($this->request->url == $value) {
            return $class;
        }

    }

    public function showEnabledStatus($item)
    {
        /*
        request can either be pending, denied, completed
         */
        if ($item->request_status == "pending") {
            if ($item->requested_action == "enable") {
                if (!$item->enabled) {
                    return '<span class="label label-info">Enable Requested</span>';
                }
                return '<span class="label label-success">Enabled</span>';
            }
            if ($item->requested_action == "disable") {
                if ($item->enabled) {
                    return '<span class="label label-info">Disable Requested</span>';
                }
                return '<span class="label label-warning">Disabled</span>';
            }
            return '<span class="label label-success">Enabled</span>';
        }
        if ($item->enabled) {
            return '<span class="label label-success">Enabled</span>';
        } else {
            return '<span class="label label-warning">Disabled</span>';
        }
    }

    public function getWings($json)
    {

        if (!empty($json)) {

            $data = json_decode($json, true);
            $string = "";

            foreach ($data as $value) {
                //debug($value['name']); die();
                //$string .= (!empty($value['name']) ? $value['name'] : "") . " (". (!empty($value['floors']) ? $value['floors'] : ''). "), ";

                $string .= (!empty($value['name']) ? $value['name'] : "");
                $string .= " (";
                $string .= (!empty($value['floors']) ? $value['floors'] : '');
                $string .= "), ";
            }

            //debug($string); die();
            return $this->str_lreplace(",", "", $string);

        }
        return 'N/A';

    }

    //find and replace last occurence in string
    public function str_lreplace($search, $replace, $subject)
    {
        $pos = strrpos($subject, $search);

        if ($pos !== false) {
            $subject = substr_replace($subject, $replace, $pos, strlen($search));
        }

        return $subject;
    }

    public function getLocality($json)
    { //locality can either be city or area

        $data = json_decode($json, true);
        //debug($data); die();
        return !empty($data['vicinity']) ? $data['vicinity'] : (!empty($data['formatted_address']) ? $data['formatted_address'] : $data['adr_address']);
        //return $data[1]['long_name'];
        /*
    debug($data[1]['long_name']); die();

    return [
    "street" => "street",
    "neighborhood" => "street",
    "area" =>  "street",
    "city" =>  "street",
    "state" =>  "street",
    "country" =>  "street",
    ];
     */
    }

    //not sure this guy is in use
    public function ShowMealTypes($meal_type)
    {
        return "yes";
    }
    public function CheckedMealTypes($breakfast, $lunch, $dinner)
    {

        $value = "";

        if ($breakfast) {
            $value = "Breakfast";
        }
        if ($lunch) {
            $value .= ", Lunch";
        }
        if ($dinner) {
            $value .= ", Dinner";
        }
        if ($value == ", Dinner") {
            $value = "Dinner";
        }
        if ($value == ", Lunch") {
            $value = "Lunch";
        }

        $prefix = ', ';
        // to remove the comma and space only if it appears in the beginning
        $str = preg_replace('/^' . preg_quote($prefix, '/') . '/', '', $value);

        return $str;

    }

    public function getDashboardTicketStatus($status)
    {
        if ($status == "pending") {
            return '<span class="item-status" style="top: 0"><span class="badge badge-empty badge-warning"></span> pending</span>';
        } elseif ($status == "closed") {
            return '<span class="item-status" style="top: 0"><span class="badge badge-empty badge-success"></span> answered</span>';
        } else {
            return '<span class="item-status" style="top: 0"><span class="badge badge-empty badge-danger"></span> closed</span>';
        }

    }

    public function showSubscriptionStatus($status)
    {

        if (!empty($this->_View->get('site_template')) && $this->_View->get('site_template') == "apex") {

            if ($status == "active") {
                return '<span class="bg-success" style="color: white; padding: 3px;">ACTIVE</span>';

            } elseif ($status == "paused") {
                return '<span class="bg-danger" style="color: white; padding: 3px;">PAUSED</span>';

            } elseif ($status == "completed") {
                return '<span class="bg-primary" style="color: white; padding: 3px;">COMPLETED</span>';
            } else {
                return '<span class="bg-warning" style="color: white; padding: 3px;">CANCELLED</span>';
            }

        } else {

            if ($status == "active") {
                return '<span class="label label-success">Active</span>';

            } elseif ($status == "paused") {
                return '<span class="label label-danger">Paused</span>';

            } elseif ($status == "completed") {
                return '<span class="label label-info">Completed</span>';
            } else {
                return '<span class="label label-warning">Canceled</span>';
            }
        }

    }

    public function showTicketStatus($status)
    {
        if ($status == "pending") {
            return '<span class="label label-info">Pending</span>';
        } else {
            return '<span class="label label-success">Closed</span>';
        }
    }

    public function ShowOrderStatus($status)
    {

    }

    /*    public function objectToArray($obj) {

    if(is_object($obj)) $obj = (array) $obj;
    if(is_array($obj)) {
    $new = array();
    foreach($obj as $key => $val) {
    $new[$key] = $this->objectToArray($val);
    }
    }else{
    $new = $obj;
    }
    return $new;
    }*/

    public function displayDeliveryPlan($my_array)
    {

        $menu = "";
        $custom_menu = "";
        $products = "";

        if (!empty($my_array['menu'])) {
            $menu = $my_array['menu']['name'] . "<br>";
        }
        if (!empty($my_array['custom_menu'])) {
            $custom_menu = $my_array['custom_menu']['name'] . "<br>";
        }

        if (!empty($my_array['products'])) {
            foreach ($my_array['products'] as $key => $value) {
                //here we're checking for the last item so as not to display the comma after it
                if ($key === count($my_array['products']) - 1) {
                    $products .= $value->name;
                } else {
                    $products .= $value->name . ", ";
                }
            }
        }

        //  debug($products); die();
        return [
            "menu" => $menu,
            "custom_menu" => $custom_menu,
            "products" => $products,
        ];
    }

    public function getSelectedDeliveryPlan($day, $meal, $item)
    {

        $day_id = $this->getDayReversed($day);
        $meal_id = $this->getMealTypeReversed($meal);

        $values = [];
        if (!empty($this->_View->get('deliveryPlans'))) {
            foreach ($this->_View->get('deliveryPlans') as $value) {
                if ($value->day_id == $day_id && $value->meal_id == $meal_id) {
                    if (!empty($value->$item)) {
                        foreach ($value->$item as $value2) {
                            array_push($values, $value2->id);
                        }
                    }
                }
            }
        }
        return $values;
    }

    /* No longer in use. Replaced this with getSelectedDeliveryPlan above */
    public function selectSelected($array_index)
    {

        //"value" => (!empty($deliveryPlans[$indexes[1]]) ? $this->Custom->selectSelected($deliveryPlans[$indexes[1]]->menus) : null )

        $values = [];
        if (!empty($array_index)) {
            foreach ($array_index as $value) {
                array_push($values, $value->id);
            }
        }
        return $values;
    }

    public function userHasRole($user_roles, $action_roles)
    {
        return $this->customUtil->userHasRole($user_roles, $action_roles);
    }

    public function getSumOfProducts($products, $notes, $config_quantity = null)
    {
        return $this->customUtil->getSumOfProducts($products, $notes, $config_quantity);
    }

    public function checkSubscriptionDeliveryConfig($subscription_id, $subscriptions_delivery_plans, $balance = null, $currency = null, $head_count = 1)
    {
        return (isset($subscriptions_delivery_plans[$subscription_id]['plan']) && $subscriptions_delivery_plans[$subscription_id]['plan'] == true ? [
            'plan' => '<span class="label label-success">Yes</span>',
            'weekly_price' => $balance > ($subscriptions_delivery_plans[$subscription_id]['weekly_price'] * $head_count)
            ? '<span class="label label-success" style="font-size: 1.3em;">' . $currency . number_format($subscriptions_delivery_plans[$subscription_id]['weekly_price'] * $head_count) . '</span>'
            : '<span class="label label-danger" style="font-size: 1.3em;">' . $currency . number_format($subscriptions_delivery_plans[$subscription_id]['weekly_price'] * $head_count) . '</span>',
        ] : [
            'plan' => '<span class="label label-danger">No</span>',
            'weekly_price' => 0,
        ]);
    }

    public function niceDate($dob)
    {
        if (!empty($dob)) {
            $test = new DateTime($dob);
            return date_format($test, 'l jS \of F Y');
        }
        return "N/A";
    }

    /*
    Not sure still in use. Was formerly in use order view, I guess
     */
    public function displayQuantity($product_id, $notes_json)
    {
        $array_variable = json_decode($notes_json, true);
        if (!empty($product_id) && !empty($array_variable['products_quantity']) && array_key_exists($product_id, $array_variable['products_quantity'])) {
            return $array_variable['products_quantity'][$product_id];
        }
        return 1;
    }

    public function orderStatus($status)
    {

        //debug($status); die();
        if ($status['id'] == 1 || $status['id'] == 2) {
            $label = "warning";
        } elseif ($status['id'] == 3) {
            $label = "info";
        } elseif ($status['id'] == 4) {
            $label = "success";
        } elseif ($status['id'] == 5) {
            $label = "danger";
        } else {
            return ucwords($status['name']);
        }
        return '<span class="label label-' . $label . '">' . ucwords($status['name']) . '</span>';
    }

    public function mealYesOrNo($value)
    {
        return ($value ? '<span class="label label-success">Yes</span>' : '<span class="label label-danger">No</span>');
    }

    public function deleteTableSessionData($meal)
    {
        if (!empty($this->request->session()->read('products_ids'))) {
            $products_ids = $this->request->session()->read('products_ids');

            //debug($products_ids); die();
            unset($products_ids[$meal]);
            $this->request->session()->write('products_ids', $products_ids);
        }

    }

    public function getDay($index)
    {
        return $this->customUtil->getDay($index);
    }

    public function getMealType($index)
    {
        return $this->customUtil->getMealType($index);
    }

    public function getDayReversed($day)
    {
        return $this->customUtil->getDayReversed($day);
    }

    public function getMealTypeReversed($meal)
    {
        return $this->customUtil->getMealTypeReversed($meal);
    }

    public function explodeMeal($meal)
    {
        $pieces = explode("_", $meal);
        return $pieces[1];
    }

    /*   public function displayFilterDropDowns($categories, $subcategories, $input_namw){
$html = '<div class="col-md-12">
<div class="col-md-6">
<select name="'.$input_namw.'_subcategory" class="input-sm">
<option value="">All</option>';

foreach ($categories as $key => $value){
$html .= '<option value="'.$key.'">'.$value.'</option>';
}

$html .= '</select> <br>
<span>Category</span>
</div>';

$html .= '<div class="col-md-6">
<select name="'.$input_namw.'_subcategory" class="input-sm">
<option value="">All</option>';

foreach ($subcategories as $key => $value){
$html .= '<option value="'.$key.'">'.$value.'</option>';
}

$html .= '</select> <br>
<span>Subcategory</span>
</div>
</div>';

return $html;
}*/

}
