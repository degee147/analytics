<?php $this->layout = false;?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <title>Login | Big Data Analytics Demo By Cybernek</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="<?=$this->Url->build('/', true);?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"
        type="text/css" />
    <link href="<?=$this->Url->build('/', true);?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
        rel="stylesheet" type="text/css" />
    <link href="<?=$this->Url->build('/', true);?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet"
        type="text/css" />
    <link href="<?=$this->Url->build('/', true);?>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet"
        type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?=$this->Url->build('/', true);?>assets/admin/pages/css/login.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME STYLES -->
    <link href="<?=$this->Url->build('/', true);?>assets/global/css/components-md.css" id="style_components" rel="stylesheet"
        type="text/css" />
    <link href="<?=$this->Url->build('/', true);?>assets/global/css/plugins-md.css" rel="stylesheet" type="text/css" />
    <link href="<?=$this->Url->build('/', true);?>assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css" />
    <link href="<?=$this->Url->build('/', true);?>assets/admin/layout/css/themes/default.css" rel="stylesheet" type="text/css"
        id="style_color" />
    <link href="<?=$this->Url->build('/', true);?>assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css" />
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="favicon.ico" />
    <style>

        .login .logo {
                margin: 0;
                padding: 0;
            }
        </style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->

<body class="page-md login">
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
    <div class="menu-toggler sidebar-toggler">
    </div>
    <!-- END SIDEBAR TOGGLER BUTTON -->
    <!-- BEGIN LOGO -->
    <div class="logo" style="max-width: 10%;  margin: auto; margin-top: 50px;">
        <a href="<?=$this->Url->build('/', true);?>">
            <img src="<?=$this->Url->build('/', true);?>logo.png" alt="" /> 
        </a>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN LOGIN -->
    <div class="content">

        <?=$this->Flash->render()?>
        <?=$this->Flash->render('auth')?>
        <!-- BEGIN LOGIN FORM -->
        <?=$this->Form->create(null, ['class' => 'login-form'])?>

        <h3 class="form-title" style="margin: 10px;">Big Data Analytics Demo</h3>
        <p style="text-align:center;margin:0">Please use the demo info below to sign in</p>
        <div style="text-align:center">
            <p style="margin:0"><strong>username: bigdata </strong> </p>
            <p style="margin:0; margin-bottom:10px;"><strong>password: analysis </strong> </p>
        </div>

        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span>
                Enter an Email and password. </span>
        </div>

        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Email or Username</label>
            <?=$this->Form->control('username_or_email', ['label' => false, 'placeholder' => 'Email or Username', 'class' => 'form-control placeholder-no-fix', 'autocomplete' => 'off'])?>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <?=$this->Form->control('password', ['label' => false, 'placeholder' => 'Password', 'class' => 'form-control placeholder-no-fix', 'autocomplete' => 'off'])?>
        </div>
        <div class="form-actions">
            <?=$this->Form->button(__('Login <i class="m-icon-swapright m-icon-white"></i>'), ['class' => 'btn btn-success uppercase'])?>
            <!--  <label class="rememberme check">
<input type="checkbox" name="remember" value="1"/>Remember </label>-->
            <!--                <a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>-->
        </div>
        <div class="create-account" style="padding-top: 8px;">
            <!-- <p>&nbsp;</p> -->
        </div>
        <?=$this->Form->end()?>

        <!-- END LOGIN FORM -->
        <!-- BEGIN FORGOT PASSWORD FORM -->
        <form class="forget-form" action="index.html" method="post">
            <h3>Forget Password ?</h3>
            <p>
                Enter your e-mail address below to reset your password.
            </p>
            <div class="form-group">
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" />
            </div>
            <div class="form-actions">
                <button type="button" id="back-btn" class="btn btn-default">Back</button>
                <button type="submit" class="btn btn-success uppercase pull-right">Submit</button>
            </div>
        </form>
        <!-- END FORGOT PASSWORD FORM -->
    </div>
    <div class="copyright">
        <?=date('Y');?> Big Data Analytics Demo Solution by <a href="http://www.cybernek.com" target="_blank">Cybernek Solutions Limited</a>.
        <!-- <?=date('Y');?> © All Rights Reserved. -->
    </div>
    <!-- END LOGIN -->
    <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
    <!-- BEGIN CORE PLUGINS -->
    <!--[if lt IE 9]>
<script src="<?=$this->Url->build('/', true);?>assets/global/plugins/respond.min.js"></script>
<script src="<?=$this->Url->build('/', true);?>assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
    <script src="<?=$this->Url->build('/', true);?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="<?=$this->Url->build('/', true);?>assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="<?=$this->Url->build('/', true);?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?=$this->Url->build('/', true);?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="<?=$this->Url->build('/', true);?>assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
    <script src="<?=$this->Url->build('/', true);?>assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="<?=$this->Url->build('/', true);?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js"
        type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?=$this->Url->build('/', true);?>assets/global/scripts/metronic.js" type="text/javascript"></script>
    <script src="<?=$this->Url->build('/', true);?>assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
    <script src="<?=$this->Url->build('/', true);?>assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
    <script src="<?=$this->Url->build('/', true);?>assets/admin/pages/scripts/login.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
        jQuery(document).ready(function () {
            Metronic.init(); // init metronic core components
            Layout.init(); // init current layout
            //Login.init(); //I commented this out to disable validation for now
            Demo.init();
        });

    </script>
    <!-- END JAVASCRIPTS -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-125714207-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-125714207-1');

    </script>

</body>
<!-- END BODY -->

</html>
