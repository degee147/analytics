<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Pos Controller
 *
 */
class PosController extends AppController
{
    public function howBusy()
    {
        $this->set('page', 'howBusy');
    }

    public function failingTransactions()
    {
        $this->set('page', 'failingTransactions');
    }

    public function lostRevenue()
    {
        $this->set('page', 'lostRevenue');
    }

    public function customerTransactions()
    {
        $this->set('page', 'customerTransactions');
    }
    public function mostFrequentMerchants()
    {
        $this->set('page', 'mostFrequentMerchants');
    }
}
