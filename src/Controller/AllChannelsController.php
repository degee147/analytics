<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * AllChannels Controller
 *
 */
class AllChannelsController extends AppController
{
    public function performance() 
    {
        $this->set('page', 'all_performance');
    }

    public function customerTransactions()
    {
        $this->set('page', 'all_transactions');
    }

}
