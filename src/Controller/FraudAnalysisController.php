<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * FraudAnalysis Controller
 *
 */
class FraudAnalysisController extends AppController
{
    public function cardFallbacks()
    {
        $this->set('page', 'cardFallbacks');
    }

    public function excessiveTransactions()
    {
        $this->set('page', 'excessiveTransactions');
    }

    public function flaggedCustomers()
    {
        $this->set('page', 'flaggedCustomers');
    }

    public function unusualTransactions()
    {
        $this->set('page', 'unusualTransactions');
    }
}
