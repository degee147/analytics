<?php
namespace App\Controller\Component;

use App\Utility\Custom;
use Cake\Controller\Component;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

//for use with custom utility

/**
 * Custom component
 */
class CustomComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public $components = ['Flash', 'Auth', 'Notify', 'Email'];

    public $customUtil; //for use with custom utility

    public function userHasRole($user_roles, $action_roles)
    {
        return $this->customUtil->userHasRole($user_roles, $action_roles);
    }

    public function createLinfo()
    {
        $linfo = new \Linfo\Linfo;
        $parser = $linfo->getParser();
        //debug($parser->getNet());die();
        return $parser;
    }
    
    public function getPendingNotifications($id, $startDate = null, $endDate = null)
    {   


        if ($this->request->getParam('prefix') == "admin") {

            return !empty($startDate) && !empty($endDate) ? $this->Notifications->find('all', [
                'order' => ['created' => 'DESC'],
                'limit' => 50,
            ])->where(['user_id' => $id, "viewed" => 0, 'role_id >' => 3, function ($exp, $q) use ($startDate, $endDate) {
                return $exp->between('Notifications.created', $startDate, $endDate);
            }])->toArray() : $this->Notifications->find('all', [
                'order' => ['created' => 'DESC'],
                'limit' => 50,
            ])->where(['user_id' => $id, "viewed" => 0, 'role_id >' => 3])->toArray();

            // return $this->Notifications->find('all', [
            //     'order' => ['created' => 'DESC'],
            //     'limit' => 50,
            // ])->where(['user_id' => $id, "viewed" => 0, 'role_id >' => 3])->toArray();

        } else {
            return $this->Notifications->find('all', [
                'order' => ['created' => 'DESC'],
                'limit' => 50,
            ])->where(['user_id' => $id, "viewed" => 0, 'role_id <=' => 3])->toArray();

        }

    }

    public function generateRandomAlphanumeric($size)
    {
        $alpha_key = '';
        $keys = range('A', 'Z');

        for ($i = 0; $i < 2; $i++) {
            $alpha_key .= $keys[array_rand($keys)];
        }

        $length = $size - 2;

        $key = '';
        $keys = range(0, 9);

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $alpha_key . $key;
    }


    public function cakeTime($time)
    {
        $time = new Time($time);
        return $time->timeAgoInwords();
    }


    //find and replace last occurence in string
    public function str_lreplace($search, $replace, $subject)
    {
        $pos = strrpos($subject, $search);
        if ($pos !== false) {
            $subject = substr_replace($subject, $replace, $pos, strlen($search));
        }
        return $subject;
    }
    /*
     * Create a random string
     * @author    XEWeb <>
     * @param $length the length of the string to create
     * @return $str the string
     */
    public function randomString($length = 6)
    {
        $str = "";
        $characters = array_merge(range('A', 'Z'));
        // $characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }

    public function uploadFile($file, $folder) //file is an upload array

    {
        // $file = $this->request->data['image']; //put the data into a var for easy use

        //debug($file); die();

        if (!empty($file['name'])) {

            $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
            $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions
            $setNewFileName = date("YFj") . "_" . rand(000000, 999999);

            //only process if the extension is valid
            if (in_array($ext, $arr_ext)) {

                if (!file_exists(WWW_ROOT . "uploads" . DS . $folder)) {
                    mkdir(WWW_ROOT . "uploads" . DS . $folder, 0777, true);
                }

                //do the actual uploading of the file. First arg is the tmp name, second arg is

                //where we are putting it, //move_uploaded_file returns true on success
                if (move_uploaded_file($file['tmp_name'], WWW_ROOT . "uploads" . DS . $folder . DS . $setNewFileName . '.' . $ext)) {

                    //then we can prepare the filename for database entry
                    $imageFileName = $setNewFileName . '.' . $ext;
                    return ["response" => "success", "upload" => true, "filename" => $imageFileName];
                }

            } else {
                // $this->Flash->error('File Extension not supported');
                return ["response" => "File Extension not supported", "upload" => false];
            }
        } else {
            // $this->Flash->error('No file uploaded');
            return ["response" => "No file selected", "upload" => false];
        }

    }

    public function getDay($index)
    {
        return $this->customUtil->getDay($index);
    }

    public function getMealType($index)
    {
        return $this->customUtil->getMealType($index);
    }

    public function getDayReversed($day)
    {
        return $this->customUtil->getDayReversed($day);
    }

    public function getMealTypeReversed($meal)
    {
        return $this->customUtil->getMealTypeReversed($meal);
    }


    public function initialize(array $config)
    {
        $this->customUtil = new Custom();

     

    }

}
