<?php
namespace App\Controller\Component;

use App\Utility\Custom;
use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

//for use with custom utility

/**
 * Notify component
 */
class NotifyComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];
    public $components = ['Custom', 'Flash', 'Auth'];


    public function notify($data)
    {
        $notification = $this->Notifications->newEntity();
        $notification = $this->Notifications->patchEntity($notification, $data);
        if ($this->Notifications->save($notification)) {
            return true;
        }
        //debug($notification); die();
        return false;
    }

    public function initialize(array $config)
    {
        $this->customUtil = new Custom();
     
    }
}
