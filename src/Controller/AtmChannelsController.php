<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * AtmChannels Controller
 *
 */
class AtmChannelsController extends AppController
{
    public function profile()
    {
        $this->set('page', 'atm_profile'); 
    }

    public function performance()
    {
        $this->set('page', 'atm_performance');
    }

    public function placement()
    {
        $this->set('page', 'atm_placement');
    }

    public function failures()
    {
        $this->set('page', 'atm_failures');
    }
    
    public function transactionAnalyzer()
    {
        $this->set('page', 'atm_anaylzer');
    }
}
