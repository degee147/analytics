<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CardUsage Controller
 *
 */
class CardUsageController extends AppController
{   
    public function performance() 
    {
        $this->set('page', 'card_performance');
    }

    public function topMerchants()
    {
        $this->set('page', 'top_merchants');
    }

    public function customerFrustrations()
    {
        $this->set('page', 'customer_frustrations');
    }
    
    public function customerLocations()
    {
        $this->set('page', 'customer_locations');
    }

    public function customerTransactions ()
    {
        $this->set('page', 'card_transactions');
    }
}
