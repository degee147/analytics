<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Login Controller
 *
 */
class LoginController extends AppController
{
    public function beforeFilter(\Cake\Event\Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow();
    }

    public function index()
    {
        if ($this->request->is('post')) {

            //dd($this->request->getData());

            if ($this->request->getData()['username_or_email'] == 'bigdata' && $this->request->getData()['password'] == 'analysis') {
                $user = (object) [];
                $user->username = 'bigdata';
                $user->firstname = "Big";
                $user->lastname = "Data";
              
                $this->Auth->setUser($user);
                
                $this->Flash->default(__('Welcome back, ' . ucfirst($user->firstname . " ". $user->lastname) . '.'));
                //$this->Custom->sendLog("logged in", 1, null, null, false);

                //dd($this->Auth->redirectUrl());
                if ($this->Auth->redirectUrl() != '/') {
                    return $this->redirect($this->Auth->redirectUrl());
                }

                return $this->redirect([
                    'prefix' => false,
                    'controller' => 'Dashboard',
                    'action' => 'index',
                ]);

            }else{
                $this->Flash->error(__('Username or password is incorrect'), [
                    'key' => 'auth',
                ]);
            }

        }
    }
}
