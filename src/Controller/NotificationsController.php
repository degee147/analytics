<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Notifications Controller
 *
 */
class NotificationsController extends AppController
{
    public function index()
    {
        $this->set('page', 'notifications');
    }
}
