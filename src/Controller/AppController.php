<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    public $helpers = ['AssetCompress.AssetCompress'];

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]); 
        $this->loadComponent('Flash');
        $this->loadComponent('Custom');
        $this->loadComponent('Notify');
        $this->loadComponent('Auth', [
            // 'authorize' => ['Controller'],
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        //'username' => 'email'
                        'username' => 'username_or_email',
                    ],
                    'finder' => 'auth',
                ],
            ],
            'loginAction' => [
                'prefix' => false,
                'controller' => 'Login',
                'action' => 'index',
                //  'plugin' => 'Users'
            ],
            'authError' => 'Login to continue.',
            'storage' => 'Session',
        ]); 

        $this->naira = "&#8358;";
        $this->set("naira", $this->naira);

        $this->customUtil = new \App\Utility\Custom();
        $this->site_template = "Metronic";
        $this->viewBuilder()->setLayout('Metronic.metronic');
        $this->viewBuilder()->setTheme($this->site_template);
        $this->session = $this->getRequest()->getSession();

        $this->set("pending_notifications", null);
        //dd($this->Auth->user());
        $this->set("user", !empty($this->Auth->user()) ? $this->Auth->user() : null);


        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
    }
}
