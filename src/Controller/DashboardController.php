<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Dashboard Controller
 *
 */
class DashboardController extends AppController
{
    public function index()
    {
        // $parser = $this->Custom->createLinfo();
        // $ram = $parser->getRam();
        // $os = $parser->getOS();
        // $distro = $parser->getDistro();
        // $uptime = $parser->getUpTime();
        // $hd = $parser->getHD();
        // // $mounts = $parser->getMounts(); //causes trouble on linux
        // $processes = $parser->getProcessStats();
        // $phpVersion = $parser->getPhpVersion();
        // //dd($ram);

        // $this->set(compact('ram', 'os', 'distro', 'uptime', 'hd', 'processes', 'phpVersion'));

        $this->set('page', 'dashboard');
    }
}
