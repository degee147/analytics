<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DigitalBanking Controller
 *
 */
class DigitalBankingController extends AppController
{
    public function performance()
    {
        $this->set('page', 'digital_performance'); 
    }

    public function mobileTransactions()
    {
        $this->set('page', 'digital_mobile');
    }

}
