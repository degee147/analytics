<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Logout Controller
 *
 */
class LogoutController extends AppController
{
    public function index()
    {
        /*  $this->Auth->logout();
        return $this->redirect([
        'controller'=> 'Dashboard',
        'action' => 'index'
        ]);*/
        // $this->Custom->sendLog("logged out", 1);
        return $this->redirect($this->Auth->logout());
    }
}
