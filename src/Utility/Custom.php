<?php

namespace App\Utility;

use App\Controller\Component\CustomComponent; //we're just using this so we can load model
//use App\Controller\AppController; //we're just using this so we can load model
use Cake\Console\Shell;
use Cake\Controller\ComponentRegistry;

// <- resides in your app's src/Controller/Component folder

//use Goutte\Client;
//use Symfony\Component\DomCrawler\Crawler;
//use GuzzleHttp\Psr7;
//use GuzzleHttp\Exception\RequestException;

class Custom extends Shell
//class Custom  extends AppController

{
    public function initialize()
    {
        $this->CustomComp = new CustomComponent(new ComponentRegistry());
    }

    public function canShowLogLink($log)
    {
        $url = json_decode($log->url);
        $entity = json_decode($log->entity);

        if ($log->show_url == 0) {
            //dd("got here");
            return false;
        }

        // if (!empty($url) && $url->action != "add" && $url->isAjax == false) {
        //     return true;
        // }
        if (!empty($url)) {
            if (!($url->action == "add" && empty($entity))) {
                if ($url->isAjax == false) {
                    //dd("got here 2");
                    return true;
                }
            }
        }
        //debug($log);
        //debug($url);
        //debug($entity);
        //die();
        return false;

    }

    public function getLogLinkParams($log)
    {
        $url = json_decode($log->url);
        $entity = json_decode($log->entity);

        return [
            'prefix' => !empty($url->prefix) ? $url->prefix : null,
            'controller' => $url->controller,
            'action' => ($url->action == 'add' || $url->action == 'edit') && !empty($entity) ? 'view' : $url->action,
            (!empty($url->pass[0]) ? $url->pass[0] : ($url->action == 'add' && !empty($entity) ? $entity->id : '')),
        ];

    }

    public function getDay($index)
    {
        $day = null;
        if ($index == 1) {
            $day = "monday";
        } elseif ($index == 2) {
            $day = "tuesday";
        } elseif ($index == 3) {
            $day = "wednesday";
        } elseif ($index == 4) {
            $day = "thursday";
        } elseif ($index == 5) {
            $day = "friday";
        }
        return $day;
    }

    public function getMealType($index)
    {
        $meal = null;
        if ($index == 1) {
            $meal = "breakfast";
        } elseif ($index == 2) {
            $meal = "lunch";
        } elseif ($index == 3) {
            $meal = "dinner";
        }
        return $meal;
    }

    public function getDayReversed($day)
    {
        $index = null;
        if ($day == "monday") {
            $index = 1;
        } elseif ($day == "tuesday") {
            $index = 2;
        } elseif ($day == "wednesday") {
            $index = 3;
        } elseif ($day == "thursday") {
            $index = 4;
        } elseif ($day == "friday") {
            $index = 5;
        }
        return $index;
    }

    public function getMealTypeReversed($meal)
    {
        $index = null;
        if ($meal == "breakfast") {
            $index = 1;
        } elseif ($meal == "lunch") {
            $index = 2;
        } elseif ($meal == "dinner") {
            $index = 3;
        }
        return $index;
    }

    public function userHasRole($user_roles, $action_roles)
    {
        //get user with roles
        //check if any of the roles in roles_array is in array of user roles
        //if any exist at all, return true

        /*

        No need for this logic. If we add a new role, we'd go into all mentions of userHasRole function and add the new role
        Or better still we create an actions model and associate actions with allowed roles to perform them

        if($task == ">"){
        //check if max user role is grater than or equal to min allowed roles
        //find the maximum role in $action_roles and check if user's maximum role is greater than or equals to maximum access_roles

        }elseif($task == "<"){

        }else{

        }
         */
        //$result = !empty(array_intersect($user_roles, $action_roles));

        $bFound = (count(array_intersect($user_roles, $action_roles))) ? true : false;

        return $bFound;
    }

    public function getSumOfProducts($products, $notes, $config_quantity = null)
    {
        $price = 0;
        $array_variable = json_decode($notes, true);

        //debug($notes);
        //debug($array_variable); die();

        if (!empty($products)) {
            foreach ($products as $value) {

                $value = (object) $value;

                $product_price = 0;

                //this is for the first people using this function
                if (!empty($array_variable) && array_key_exists('products_quantity', $array_variable)) {
                    if (array_key_exists($value->id, $array_variable['products_quantity'])) {
                        $product_price += ($value->price * $array_variable['products_quantity'][$value->id]);
                        //continue; //his should skip any more lines below and contnue the loop
                    } else {
                        $product_price += $value->price;
                    }
                } else {
                    $product_price += $value->price;
                }

                if (!empty($config_quantity['products']) && array_key_exists($value->id, $config_quantity['products'])) {
                    $product_price = $product_price * $config_quantity['products'][$value->id];
                }

                /*
                Duplicated for backwards compatibility reasons
                Had to change the config quantity key we store in db from products to product

                 */
                if (!empty($config_quantity['product']) && array_key_exists($value->id, $config_quantity['product'])) {
                    $product_price = $product_price * $config_quantity['product'][$value->id];
                }

                $price += $product_price;
                //this is subscription weekly price logic after delivery plan has been configured
                /*if(!empty($array_variable) && array_key_exists('config_quantity', $array_variable)){
                if(array_key_exists('products', $array_variable['config_quantity'])){
                if(array_key_exists($value->id, $array_variable['config_quantity']['product'])){
                $price += ($value->price * $array_variable['config_quantity']['product'][$value->id]);
                continue;
                }
                }
                }*/
                //$price += $value->price;
            }

        }
        return $price;
    }

}
