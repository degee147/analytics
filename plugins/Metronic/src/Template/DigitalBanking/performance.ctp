<?php $this->assign('title', 'Performance | Digital Banking Analysis ');?>
<!-- BEGIN PAGE HEAD-->
<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Performance
            <!-- <small>charting library & maps. Where all data goes visual with amChart plugin</small> -->
        </h1>
    </div>
    <!-- END PAGE TITLE -->

</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <?=$this->Html->link('Dashboard', ['prefix' => false, 'controller' => 'dashboard', 'action' => 'index'], ['escape' => false]);?>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        Digital Banking Analysis
        <i class="fa fa-circle"></i>
    </li>
    <li>
        Performance
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN ROW -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN CHART PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>
                            <span class="caption-subject bold uppercase font-green-haze"> Digital Banking Performance</span>
                            <!-- <span class="caption-helper">column and line mix</span> -->
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="fullscreen"> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4 style="text-align:center">Last Week Summary</h4>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Channels </th>
                                                <th> Value </th>
                                                <th> Average </th>
                                                <th> Difference </th>
                                                <th> Volume </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Online</td>
                                                <td> <span class="label label-danger"> 8.46M </span> </td>
                                                <td> <span class="label label-danger"> 19.46M </span> </td>
                                                <td> <span class="label label-danger"> -58.46 </span> </td>
                                                <td><span class="label label-danger"> 67,323 </span> </td>
                                            </tr>

                                            <tr>
                                                <td>Online</td>
                                                <td> <span class="label label-success"> 340M </span> </td>
                                                <td> <span class="label label-success"> 305M </span> </td>
                                                <td> <span class="label label-success"> 11.57 </span> </td>
                                                <td><span class="label label-success"> 92,323 </span> </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label has-info">
                                    <select class="form-control" id="form_control_1">
                                        <option value=""></option>
                                        <option value="1">All</option>
                                        <option value="2">Option 2</option>
                                        <option value="3">Option 3</option>
                                        <option value="4">Option 4</option>
                                        <option value="4">Etc</option>
                                    </select>
                                    <label for="form_control_1"><span style="font-size: .75em;">Transaction
                                            Type</span></label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label has-info">
                                    <select class="form-control" id="form_control_1">
                                        <option value=""></option>
                                        <option value="">Yearly</option>
                                        <option value="1">Monthly</option>
                                        <option value="2">Weekly</option>
                                        <option value="3">Daily</option>
                                        <option value="4">Etc</option>
                                    </select>
                                    <label for="form_control_1"><span style="font-size: .75em;">View Type</span></label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p>Transaction Volume Over Time </p>
                                <div id="threelines" style="height: 350px;width	: 100%;"></div>
                            </div>
                            <div class="col-md-12">
                                <p>Transaction Value Over Time </p>
                                <div id="threelines2" style="height: 350px;width	: 100%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CHART PORTLET-->
        </div>
    </div>
    <!-- END ROW -->
</div>
</div>
<!-- END PAGE BASE CONTENT -->
<?php echo $this->element('am_3lines')?>
<script>
    jQuery(document).ready(function () {

        //initChartSample10();
        // initCountryMap();
        //startCountriesMap("countryMapDiv");
        //startCustomHTMLMap("countryMapDiv");
        //startCountriesMap("countryMapDiv", "percent");
        start3LinesChart('threelines');
        start3LinesChart('threelines2');
        //initDateRange('#defaultrange');




    });

</script>
