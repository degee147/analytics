<?php $this->assign('title', 'Mobile Transactions | Digital Banking Analysis ');?>

<!-- BEGIN PAGE HEAD-->
<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Mobile Customer Transactions
            <!-- <small>charting library & maps. Where all data goes visual with amChart plugin</small> -->
        </h1>
    </div>
    <!-- END PAGE TITLE -->
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <?=$this->Html->link('Dashboard', ['prefix' => false, 'controller' => 'dashboard', 'action' => 'index'], ['escape' => false]);?>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        Digital Banking Analysis
        <i class="fa fa-circle"></i>
    </li>
    <li>
        Mobile Customer Transactions
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- BEGIN PAGE BASE CONTENT -->

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN ROW -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN CHART PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>
                            <span class="caption-subject bold uppercase font-green-haze"> Mobile Customer Transactions</span>
                            <!-- <span class="caption-helper">column and line mix</span> -->
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="fullscreen"> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Participation Summary</h4>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Channels </th>
                                                <th> Number of Customers Last Week </th>
                                                <th> Average Number of Customers in Previous Weeks</th>
                                                <th> Percentage Difference </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>ATM</td>
                                                <td> <span class="label label-success"> 234,453 </span> </td>
                                                <td> <span class="label label-success"> 124,243 </span> </td>
                                                <td> <span class="label label-success"> 8.46% </span> </td>
                                            </tr>

                                            <tr>
                                                <td>POS</td>
                                                <td> <span class="label label-success"> 24,453 </span> </td>
                                                <td> <span class="label label-success"> 12,232 </span> </td>
                                                <td> <span class="label label-success"> 3.34% </span> </td>
                                            </tr>
                                            <tr>
                                                <td>Online</td>
                                                <td> <span class="label label-success"> 657,565 </span> </td>
                                                <td> <span class="label label-success"> 64,243 </span> </td>
                                                <td> <span class="label label-danger"> -21.34% </span> </td>
                                            </tr>
                                            <tr>
                                                <td>Mobile</td>
                                                <td> <span class="label label-success"> 644,334 </span> </td>
                                                <td> <span class="label label-success"> 234,244 </span> </td>
                                                <td> <span class="label label-success"> 11.34% </span> </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h4>Transaction Volume Hourly</h4>
                                <div id="timeMapDiv" class="chart" style="height: 400px;width:100%"> </div>
                            </div>
                            <div class="col-md-12">
                                <h4>Transaction Volume Weekly</h4>
                                <div id="weeklydiv" class="chart" style="height: 400px;width:100%"> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CHART PORTLET-->
            </div>
        </div>
        <!-- END ROW -->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
<?php echo $this->element('am_timebasedmap')?> 
<?php echo $this->element('am_3dcolumnchart')?>
<script>
    jQuery(document).ready(function () {

        //initDateRange('#defaultrange');

        //startVerticalHorizontalBarChart('memmbermap', true);
        //startVerticalHorizontalBarChart('memmbermap2', true);
        //start3LinesChart('threelines');

        //startCustomHTMLMap("countryMapDiv", null, "User Transaction Locations");
        //startCountriesMap("countryMapDiv", "percent");
        //start3LinesChart('threelines');
        initTimeBasedMap('timeMapDiv', 'Transaction Volumne');
        init3DColumnChart('weeklydiv', false, true);

    });

</script>
