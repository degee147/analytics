<?php $this->assign('title', 'Dashboard');?>
<!-- BEGIN PAGE HEAD-->
<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1> Dashboard
            <!-- <small>statistics, charts, recent events and reports</small> -->
        </h1>
    </div>
    <!-- END PAGE TITLE -->
    <!-- BEGIN PAGE TOOLBAR -->

    <!-- END PAGE TOOLBAR -->
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="<?=$this->Url->build('/', true);?>">Home</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active">Dashboard</span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<div class="row">
    <div class="col-md-4">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-dark bold uppercase">POS CHANNELS</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="dropdown inline clearfix">
                    <!-- Link or button to toggle dropdown -->
                    <ul class="dropdown-menu" role="menu">
                        <li role="presentation">
                            <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Pos', 'action' => 'howBusy'])?>" role="menuitem" class="popovers" data-container="body" data-trigger="hover"
                                data-placement="right" data-content="How busy are POS terminals" data-original-title="How Busy">How
                                Busy</a>
                        </li>
                        <li role="presentation">
                            <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Pos', 'action' => 'failingTransactions'])?>" role="menuitem" class="popovers" data-container="body" data-trigger="hover"
                                data-placement="right" data-content="Why are card transactions at the POS failing"
                                data-original-title="Failing Transactions">Failing Transactions</a>
                        </li>
                        <li role="presentation">
                            <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Pos', 'action' => 'lostRevenue'])?>" role="menuitem" class="popovers" data-container="body" data-trigger="hover"
                                data-placement="right" data-content="How much revenue have is lost due to incomplete transactions or availability issues"
                                data-original-title="Lost POS Revenue">Lost POS Revenue</a>
                        </li>
                        <li role="presentation">
                            <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Pos', 'action' => 'customerTransactions'])?>" role="menuitem" class="popovers" data-container="body" data-trigger="hover"
                                data-placement="right" data-content="How various customer segments choose to interact"
                                data-original-title="Customer Transactions">Customer Transactions</a>
                        </li>
                        <li role="presentation">
                            <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Pos', 'action' => 'mostFrequentMerchants'])?>" role="menuitem" class="popovers" data-container="body" data-trigger="hover"
                                data-placement="right" data-content="Which merchants are frequented the most"
                                data-original-title="Most Frequent Merchants">Most Frequent Merchants</a>
                        </li>

                    </ul>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-4">

        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-dark bold uppercase">Fraud Analysis</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="dropdown inline clearfix">
                    <!-- Link or button to toggle dropdown -->
                    <ul class="dropdown-menu" role="menu">
                        <li role="presentation">
                            <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'FraudAnalysis', 'action' => 'cardFallbacks'])?>" role="menuitem" class="popovers" data-container="body" data-trigger="hover"
                                data-placement="top" data-content="When are card fallbacks occuring"
                                data-original-title="Card Fallbacks">Card
                                Fallbacks</a>
                        </li>
                        <li role="presentation">
                            <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'FraudAnalysis', 'action' => 'excessiveTransactions'])?>" role="menuitem" class="popovers" data-container="body" data-trigger="hover"
                                data-placement="top" data-content="When is excessive transaction activity occurring"
                                data-original-title="Excessive Transactions">Excessive Transactions</a>
                        </li>
                        <li role="presentation">
                            <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'FraudAnalysis', 'action' => 'flaggedCustomers'])?>" role="menuitem" class="popovers" data-container="body" data-trigger="hover"
                                data-placement="top" data-content="What customers have been flagged due to high-risk transactions"
                                data-original-title="Flagged Customers">Flagged Customers</a>
                        </li>

                    </ul>
                </div>


            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-dark bold uppercase">ATM Channels</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="dropdown inline clearfix">
                    <!-- Link or button to toggle dropdown -->
                    <ul class="dropdown-menu" role="menu">
                        <li role="presentation">
                            <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'AtmChannels', 'action' => 'profile'])?>" role="menuitem" class="popovers" data-container="body" data-trigger="hover"
                                data-placement="left" data-content="Overall performance of ATM fleet"
                                data-original-title="Profile">Profile </a>
                        </li>
                        <li role="presentation">
                            <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'AtmChannels', 'action' => 'performance'])?>" role="menuitem" class="popovers" data-container="body" data-trigger="hover"
                                data-placement="left" data-content="How Profitable are the ATMs" data-original-title="Performance">Performance</a>
                        </li>
                        <li role="presentation">
                            <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'AtmChannels', 'action' => 'placement'])?>" role="menuitem" class="popovers" data-container="body" data-trigger="hover"
                                data-placement="left" data-content="Where should the next ATMs be placed"
                                data-original-title="Placement ">Placement </a>
                        </li>
                        <li role="presentation">
                            <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'AtmChannels', 'action' => 'failures'])?>" role="menuitem" class="popovers" data-container="body" data-trigger="hover"
                                data-placement="left" data-content="What ATM processes need improvement"
                                data-original-title="Failures ">Failures </a>
                        </li>
                        <li role="presentation">
                            <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'AtmChannels', 'action' => 'transactionAnalyzer'])?>" role="menuitem" class="popovers" data-container="body" data-trigger="hover"
                                data-placement="left" data-content="What transaction task flow do my customers prefer at the ATM"
                                data-original-title="Transaction Analyzer ">Transaction Analyzer </a>
                        </li>

                    </ul>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-dark bold uppercase">DIGITAL BANKING</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="dropdown inline clearfix">
                    <!-- Link or button to toggle dropdown -->
                    <ul class="dropdown-menu" role="menu">
                        <li role="presentation">
                            <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'DigitalBanking', 'action' => 'performance'])?>" role="menuitem" class="popovers" data-container="body" data-trigger="hover"
                                data-placement="right" data-content="Digital Banking Performance"
                                data-original-title="Performance">Performance</a>
                        </li>
                        <li role="presentation">
                            <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'DigitalBanking', 'action' => 'mobileTransactions'])?>" role="menuitem" class="popovers" data-container="body" data-trigger="hover"
                                data-placement="right" data-content=""
                                data-original-title="Mobile Customer Transactions">Mobile Customer Transactions</a>
                        </li>
                    

                    </ul>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-4">

        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-dark bold uppercase">CARD USAGE</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="dropdown inline clearfix">
                    <!-- Link or button to toggle dropdown -->
                    <ul class="dropdown-menu" role="menu">
                        <li role="presentation">
                            <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'CardUsage', 'action' => 'performance'])?>" role="menuitem" class="popovers" data-container="body" data-trigger="hover"
                                data-placement="top" data-content="How is the performance of cards"
                                data-original-title="Performance ">Performance </a>
                        </li>
                        <li role="presentation">
                            <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'CardUsage', 'action' => 'topMerchants'])?>" role="menuitem" class="popovers" data-container="body" data-trigger="hover"
                                data-placement="top" data-content="Which merchants are frequented the most"
                                data-original-title="Top Merchants">Top Merchants</a>
                        </li>
                        <li role="presentation">
                            <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'CardUsage', 'action' => 'customerFrustrations'])?>" role="menuitem" class="popovers" data-container="body" data-trigger="hover"
                                data-placement="top" data-content="What issues are causing our customers pain"
                                data-original-title="Customer Frustrations">Customer Frustrations</a>
                        </li>
                        <li role="presentation">
                            <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'CardUsage', 'action' => 'customerTransactions'])?>" role="menuitem" class="popovers" data-container="body" data-trigger="hover"
                                data-placement="top" data-content="Customer card transaction records"
                                data-original-title="Customer Transactions">Customer Transactions</a>
                        </li>

                    </ul>
                </div>


            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-dark bold uppercase">OTHER</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="dropdown inline clearfix">
                    <!-- Link or button to toggle dropdown -->
                    <ul class="dropdown-menu" role="menu">
                        <li role="presentation">
                            <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Notifications', 'action' => 'index'])?>" role="menuitem" class="popovers" data-container="body" data-trigger="hover"
                                data-placement="left" data-content="View Recent Activities and Notifications"
                                data-original-title="Notifications">Notifications</a>
                        </li>


                    </ul>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
