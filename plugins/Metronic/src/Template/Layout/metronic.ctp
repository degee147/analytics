<?php
$siteDescription = 'Big Data Analysis Demo Solution';
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title>
        <?=$this->fetch('title')?> |
        <?=$siteDescription?>
    </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="FoodSub" name="description" />
    <meta content="" name="author" />


    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="<?=$this->Url->build('/assets/', true);?>global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"
        type="text/css" />
    <link href="<?=$this->Url->build('/assets/', true);?>global/plugins/simple-line-icons/simple-line-icons.min.css"
        rel="stylesheet" type="text/css" />

    <?php //echo $this->AssetCompress->css('global-combined');?>

    <link href="<?=$this->Url->build('/assets/', true);?>compressed/global-combined.css" rel="stylesheet" type="text/css" />


    <!-- <script src="<?=$this->Url->build('/', true);?>assets/global/plugins/jquery.repeater.min.js" type="text/javascript"></script> -->
    <!-- <link rel="stylesheet" href="<?=$this->Url->build('/', true);?>assets/global/plugins/jquery-confirm/css/jquery-confirm.css"> -->


    <style>
        td,
            th {
                text-align: center;
            }

            .select2-container--open {
                z-index: 100000;
            }

            .error-message {
                color: red;
            }

            /* Always set the map height explicitly to define the size of the div
			* element that contains the map. */

            #map {
                height: 100%;
            }

            /* Optional: Makes the sample page fill the window. */

            /*html, body {
			height: 100%;
			margin: 0;
			padding: 0;
			}*/

            .controls {
                background-color: #fff;
                border-radius: 2px;
                border: 1px solid transparent;
                box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
                box-sizing: border-box;
                font-family: Roboto;
                font-size: 15px;
                font-weight: 300;
                height: 29px;
                margin-left: 17px;
                margin-top: 10px;
                outline: none;
                padding: 0 11px 0 13px;
                text-overflow: ellipsis;
                width: 400px;
            }

            .controls:focus {
                border-color: #4d90fe;
            }

            .title {
                font-weight: bold;
            }

            #infowindow-content {
                display: none;
            }

            #map #infowindow-content {
                display: inline;
            }

            /* This is supposed to fix the required attribute pop up in bootstrap select */

            /* But I don't think it's working */

            .btn-group.bootstrap-select select {
                width: 1px !important;
            }

            .bs-select {
                //width: 1px !important;
            }

            .portlet>.portlet-title>.tools>a.reload {
             background-image: url(<?=$this->Url->build('/assets/global/', true);?>/img/portlet-reload-icon.png);
                width: 13px;
            }

            .page-header.navbar .menu-toggler {
                background-image: url(<?=$this->Url->build('/assets/admin/layout4/', true);?>img/sidebar-toggle-light.png);
            }
            
            .daterangepicker.dropdown-menu {
                z-index: 9999999;
            }

            /* .form .form-actions {
			padding: 20px 10px;
			margin: 0;
			background-color: #f5f5f5;
			}
			*/

        </style>

    <script>
        function singularise(count, word) {
            var str = word;
            if (count < 2) {
                if (word.endsWith("s")) {
                    str = word.replace(new RegExp('s' + '$'), '');
                }
                if (word.endsWith("ies")) {
                    str = word.replace(new RegExp('ies' + '$'), 'y');
                }
            }
            return str;
        }

        function getMainAssetsPath() {
            return "<?=$this->Url->build('/assets/', true);?>";
        }

        function getLoadingImage() {
            return "<?=$this->Url->build('/assets/global/img/loading-spinner-grey.gif', true);?>";

        }
        //php's number format equivalent in js
        function number_format(x) {
            return isNaN(x) ? "" : x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        function isNumber(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        }

    </script>
    <!-- <script src="<?=$this->Url->build('/assets/', true);?>global/plugins/jquery.min.js" type="text/javascript"></script>
                <script src="<?=$this->Url->build('/assets/', true);?>global/scripts/app.js" type="text/javascript"></script> -->

    <?php //echo $this->AssetCompress->script('js-combined'); ?>

    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/ammaps/3.13.0/ammap.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ammaps/3.13.0/maps/js/worldLow.js"></script> -->

    <script src="<?=$this->Url->build('/assets/', true);?>compressed/js-combined.js" type="text/javascript"></script>

    <link rel="shortcut icon" href="<?=$this->Url->build('/', true);?>favicon.ico" />
</head>
<!-- END HEAD -->

<body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
    <!-- BEGIN HEADER -->
    <?=$this->element('header')?>
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <?=$this->element('sidebar', ["page" => $page]);?>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <?=$this->Flash->render()?>
                <?=$this->fetch('content')?>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->
        <!-- END QUICK SIDEBAR -->
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <?=$this->element('footer')?>
    <!-- END FOOTER -->
    <!-- BEGIN QUICK NAV -->
    <?php //echo $this->element('quicknav'); ?>
    <!-- END QUICK NAV -->

    <?php //echo $this->AssetCompress->script('jscombined2'); ?>
    <script src="<?=$this->Url->build('/assets/', true);?>compressed/jscombined2.js" type="text/javascript"></script>



    <script>
        jQuery(document).ready(function () {

            $('#clickmewow').click(function () {
                $('#radio1003').attr('checked', 'checked');
            });



            $('.date-picker').datepicker({
                //rtl: Metronic.isRTL(),
                orientation: "left",
                autoclose: true
            });

            $('.timepicker-no-seconds').timepicker({
                autoclose: true,
                minuteStep: 5
            });

            $('.spinner3').spinner({
                value: 0,
                min: 0,
                max: 20
            });



            $('.bs-select').selectpicker({
                iconBase: 'fa',
                tickIcon: 'fa-check'
            });

            jQuery(document).on('click', '.notification', function (e) {
                //e.preventDefault();
                var id = $(this).attr("notification_id");

                //alert(id);
                //return;
                //var id = $(this).attr('pid');

                // $.post(
                //     "", {
                //         'id': id,
                //     }).done(function () {}).fail(function () {});

            });
        });

    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-125714207-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-125714207-1');

    </script>

</body>

</html>
