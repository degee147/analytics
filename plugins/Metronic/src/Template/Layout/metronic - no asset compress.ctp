<?php
$siteDescription = 'Big Data Analysis Demo Solution';
?>
    <!DOCTYPE html>
    <!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
    <!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
    <!--[if !IE]><!-->
    <html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>
            <?=$this->fetch('title')?> |
                <?=$siteDescription?>
        </title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="FoodSub" name="description"  />
        <meta content="" name="author" />


        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Url->build('/assets/', true);?>global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Url->build('/assets/', true);?>global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Url->build('/assets/', true);?>global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"  />
        <link href="<?=$this->Url->build('/assets/', true);?>global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet"  type="text/css" />

        <!-- END GLOBAL MANDATORY STYLES -->

        <link href="<?=$this->Url->build('/', true);?>assets/global/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Url->build('/', true);?>assets/global/plugins/bootstrap-table/extensions/bootstrap-editable.css" rel="stylesheet" type="text/css" />


        <link rel="stylesheet" type="text/css" href="<?=$this->Url->build('/', true);?>assets/global/plugins/select22/css/select2.css"   />
        <link rel="stylesheet" type="text/css" href="<?=$this->Url->build('/', true);?>assets/global/plugins/select22/css/select2-bootstrap.min.css"   />
        <!-- <link rel="stylesheet" type="text/css" href="<?=$this->Url->build('/', true);?>assets/global/plugins/select2/select2.css"   /> -->
        <!-- <link rel="stylesheet" type="text/css" href="<?=$this->Url->build('/', true);?>assets/global/plugins/select2-4.0.3/select2.css"   /> -->
        <link rel="stylesheet" type="text/css" href="<?=$this->Url->build('/', true);?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
        <link rel="stylesheet" type="text/css" href="<?=$this->Url->build('/', true);?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"   />
        <link rel="stylesheet" type="text/css" href="<?=$this->Url->build('/', true);?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"       />
        <link rel="stylesheet" type="text/css" href="<?=$this->Url->build('/', true);?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"       />
        <link href="<?=$this->Url->build('/', true);?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet"        type="text/css" />
        <link rel="stylesheet" type="text/css" href="<?=$this->Url->build('/', true);?>assets/global/plugins/bootstrap-toastr/toastr.min.css"    />
        <link rel="stylesheet" type="text/css" href="<?=$this->Url->build('/', true);?>assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"
        />

        <link href="<?=$this->Url->build('/', true);?>assets/pages/css/profile.min.css" rel="stylesheet" type="text/css"
        />
        <link rel="stylesheet" type="text/css" href="<?=$this->Url->build('/', true);?>assets/global/plugins/bootstrap-select/bootstrap-select.min.css"
        />
        <link rel="stylesheet" type="text/css" href="<?=$this->Url->build('/', true);?>assets/global/plugins/typeahead/typeahead.css">
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?=$this->Url->build('/assets/', true);?>global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
<!-- <link href="<?=$this->Url->build('/assets/', true);?>global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
<link href="<?=$this->Url->build('/assets/', true);?>global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" /> -->
        <!--		<link href="<?=$this->Url->build('/assets/', true);?>global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />-->
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?=$this->Url->build('/assets/', true);?>global/css/components-md.min.css" rel="stylesheet" id="style_components"
            type="text/css" />
        <link href="<?=$this->Url->build('/assets/', true);?>global/css/plugins-md.min.css" rel="stylesheet" type="text/css"
        />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?=$this->Url->build('/assets/', true);?>layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css"
        />
        <link href="<?=$this->Url->build('/assets/', true);?>layouts/layout4/css/themes/default.min.css" rel="stylesheet"
            type="text/css" id="style_color" />
        <link href="<?=$this->Url->build('/assets/', true);?>layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css"
        />
        <!-- END THEME LAYOUT STYLES -->

        <link href="<?=$this->Url->build('/', true);?>assets/global/css/hover-min.css" rel="stylesheet">
        <script src="<?=$this->Url->build('/assets/', true);?>global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?=$this->Url->build('/', true);?>assets/global/plugins/jquery.repeater.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="<?=$this->Url->build('/', true);?>assets/global/plugins/jquery-confirm/css/jquery-confirm.css">


        <style>
            td,
            th {
                text-align: center;
            }

            .select2-container--open {
                z-index: 100000;
            }

            .error-message {
                color: red;
            }

            /* Always set the map height explicitly to define the size of the div
			* element that contains the map. */

            #map {
                height: 100%;
            }

            /* Optional: Makes the sample page fill the window. */

            /*html, body {
			height: 100%;
			margin: 0;
			padding: 0;
			}*/

            .controls {
                background-color: #fff;
                border-radius: 2px;
                border: 1px solid transparent;
                box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
                box-sizing: border-box;
                font-family: Roboto;
                font-size: 15px;
                font-weight: 300;
                height: 29px;
                margin-left: 17px;
                margin-top: 10px;
                outline: none;
                padding: 0 11px 0 13px;
                text-overflow: ellipsis;
                width: 400px;
            }

            .controls:focus {
                border-color: #4d90fe;
            }

            .title {
                font-weight: bold;
            }

            #infowindow-content {
                display: none;
            }

            #map #infowindow-content {
                display: inline;
            }

            /* This is supposed to fix the required attribute pop up in bootstrap select */

            /* But I don't think it's working */

            .btn-group.bootstrap-select select {
                width: 1px !important;
            }

            .bs-select {
                //width: 1px !important;
            }


            /* .form .form-actions {
			padding: 20px 10px;
			margin: 0;
			background-color: #f5f5f5;
			}
			*/

        </style>

        <script>

            function singularise(count, word){
                var str = word;
                if(count < 2){
                    if (word.endsWith("s")){
                        str = word.replace(new RegExp('s' + '$'), '');
                    }
                    if (word.endsWith("ies")){
                        str = word.replace(new RegExp('ies' + '$'), 'y');
                    }
                }
                return str;
            }

            function getMainAssetsPath() {
                return "<?=$this->Url->build('/assets/', true);?>";
            }

            function getLoadingImage() {
                return "<?=$this->Url->build('/assets/global/img/loading-spinner-grey.gif', true);?>";

            }
            //php's number format equivalent in js
            function number_format(x) {
                return isNaN(x) ? "" : x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }

            function isNumber(n) {
                return !isNaN(parseFloat(n)) && isFinite(n);
            }

            function getDeliveryTime(address_id, meal) {

                }

                </script>
                <script src="<?=$this->Url->build('/assets/', true);?>global/scripts/app.js" type="text/javascript"></script>
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <!-- BEGIN HEADER -->
        <?=$this->element('header')?>
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                <?=$this->element('sidebar', ["page" => $page]);?>
                    <!-- END SIDEBAR -->
                    <!-- BEGIN CONTENT -->
                    <div class="page-content-wrapper">
                        <!-- BEGIN CONTENT BODY -->
                        <div class="page-content">
                            <?=$this->Flash->render()?>
                                <?=$this->fetch('content')?>
                        </div>
                        <!-- END CONTENT BODY -->
                    </div>
                    <!-- END CONTENT -->
                    <!-- BEGIN QUICK SIDEBAR -->
                    <!-- END QUICK SIDEBAR -->
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            <?=$this->element('footer')?>
                <!-- END FOOTER -->
                <!-- BEGIN QUICK NAV -->
                <?php //echo $this->element('quicknav'); ?>
                <!-- END QUICK NAV -->
                <!--[if lt IE 9]>
<script src="<?=$this->Url->build('/assets/', true);?>global/plugins/respond.min.js"></script>
<script src="<?=$this->Url->build('/assets/', true);?>global/plugins/excanvas.min.js"></script>
<script src="<?=$this->Url->build('/assets/', true);?>global/plugins/ie8.fix.min.js"></script>
<![endif]-->
                <!-- BEGIN CORE PLUGINS -->

                <script src="<?=$this->Url->build('/assets/', true);?>global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
                <script src="<?=$this->Url->build('/assets/', true);?>global/plugins/js.cookie.min.js" type="text/javascript"></script>
                <script src="<?=$this->Url->build('/assets/', true);?>global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
                <script src="<?=$this->Url->build('/assets/', true);?>global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
                <script src="<?=$this->Url->build('/assets/', true);?>global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
                <!-- END CORE PLUGINS -->
                <!-- BEGIN PAGE LEVEL PLUGINS -->

 <script src="<?=$this->Url->build('/assets/', true);?>global/plugins/moment.min.js" type="text/javascript"></script>
<script src="<?=$this->Url->build('/assets/', true);?>global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
<!--<script src="<?=$this->Url->build('/assets/', true);?>global/plugins/morris/raphael-min.js" type="text/javascript"></script>
<script src="<?=$this->Url->build('/assets/', true);?>global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="<?=$this->Url->build('/assets/', true);?>global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script> -->


                <script src="<?=$this->Url->build('/assets/', true);?>global/plugins/morris/morris.min.js" type="text/javascript"></script>

                <script src="<?=$this->Url->build('/assets/', true);?>global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
                <script src="<?=$this->Url->build('/assets/', true);?>global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
                <!-- END PAGE LEVEL PLUGINS -->

                <script src="<?=$this->Url->build('/', true);?>assets/global/plugins/bootstrap-table/bootstrap-table.min.js" type="text/javascript"></script>
                <script src="<?=$this->Url->build('/', true);?>assets/global/plugins/bootstrap-table/extensions/export/bootstrap-table-export.js"
                    type="text/javascript"></script>




                <script src="<?=$this->Url->build('/', true);?>assets/global/plugins/bootstrap-table/extensions/tableExport.js" type="text/javascript"></script>
                <!-- <script src="<?=$this->Url->build('/', true);?>assets/global/plugins/bootstrap-table/extensions/bootstrap-editable.js" type="text/javascript"></script> -->

                <!-- <script src="<?=$this->Url->build('/', true);?>assets/global/plugins/bootstrap-table/extensions/FileSaver.min.js" type="text/javascript"></script>
<script src="<?=$this->Url->build('/', true);?>assets/global/plugins/bootstrap-table/extensions/jspdf.min.js" type="text/javascript"></script>
<script src="<?=$this->Url->build('/', true);?>assets/global/plugins/bootstrap-table/extensions/jspdf.plugin.autotable.js" type="text/javascript"></script>-->
                <script src="<?=$this->Url->build('/', true);?>assets/global/plugins/bootstrap-table/extensions/bootstrap-editable.min.js"
                    type="text/javascript"></script>
                <script src="<?=$this->Url->build('/', true);?>assets/global/plugins/bootstrap-table/extensions/bootstrap-table-editable.js"
                    type="text/javascript"></script>

                <script type="text/javascript" src="<?=$this->Url->build('/', true);?>assets/global/plugins/select22/js/select2.min.js"></script>
                <!-- <script type="text/javascript" src="<?=$this->Url->build('/', true);?>assets/global/plugins/select2/select2.min.js"></script> -->
                <!-- <script type="text/javascript" src="<?=$this->Url->build('/', true);?>assets/global/plugins/select2-4.0.3/select2.min.js"></script> -->
                <!-- <script type="text/javascript" src="<?=$this->Url->build('/', true);?>assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script> -->

                <!-- <script type="text/javascript" src="<?=$this->Url->build('/', true);?>assets/global/plugins/select2-4.0.3/select2.min.js"></script> -->
                <script type="text/javascript" src="<?=$this->Url->build('/', true);?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
                <script type="text/javascript" src="<?=$this->Url->build('/', true);?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
                <script type="text/javascript" src="<?=$this->Url->build('/', true);?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
                <script type="text/javascript" src="<?=$this->Url->build('/', true);?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
                <script type="text/javascript" src="<?=$this->Url->build('/', true);?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
                <script src="<?=$this->Url->build('/', true);?>assets/global/scripts/datatable.js"></script>
                <script type="text/javascript" src="<?=$this->Url->build('/', true);?>assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>

                <script type="text/javascript" src="<?=$this->Url->build('/', true);?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
                <!--<script type="text/javascript" src="<?=$this->Url->build('/', true);?>assets/global/plugins/fuelux/js/spinner.min.js"></script>-->
                <script src="<?=$this->Url->build('/', true);?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
                <script src="<?=$this->Url->build('/', true);?>assets/global/plugins/bootbox/bootbox.min.js"></script>
                <script type="text/javascript" src="<?=$this->Url->build('/', true);?>assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
                <script type="text/javascript" src="<?=$this->Url->build('/', true);?>assets/global/plugins/fuelux/js/spinner.min.js"></script>

                <script src="<?=$this->Url->build('/', true);?>assets/global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
                <script src="<?=$this->Url->build('/', true);?>assets/global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>



                <!-- BEGIN THEME GLOBAL SCRIPTS -->
                <!-- <script src="<?=$this->Url->build('/assets/', true);?>global/scripts/app.min.js" type="text/javascript"></script> -->
                <!-- END THEME GLOBAL SCRIPTS -->
                <!-- BEGIN PAGE LEVEL SCRIPTS -->
                <!-- <script src="<?=$this->Url->build('/assets/', true);?>pages/scripts/dashboard.min.js" type="text/javascript"></script> -->
                <!-- END PAGE LEVEL SCRIPTS -->
                <!-- BEGIN THEME LAYOUT SCRIPTS -->
                <script src="<?=$this->Url->build('/assets/', true);?>layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
                <script src="<?=$this->Url->build('/assets/', true);?>layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
                <!--<script src="<?=$this->Url->build('/assets/', true);?>layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>-->
                <script src="<?=$this->Url->build('/assets/', true);?>layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
                <!-- END THEME LAYOUT SCRIPTS -->
                <script src="<?=$this->Url->build('/', true);?>assets/global/plugins/jquery-confirm/js/jquery-confirm.js"></script>
                <script async src="<?=$this->Url->build('/', true);?>assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>


        <!-- AMCHART PLUGINS -->
 <script src="<?=$this->Url->build('/assets/', true);?>global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
        <script src="<?=$this->Url->build('/assets/', true);?>global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
        <script src="<?=$this->Url->build('/assets/', true);?>global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
        <script src="<?=$this->Url->build('/assets/', true);?>global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
        <script src="<?=$this->Url->build('/assets/', true);?>global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
        <script src="<?=$this->Url->build('/assets/', true);?>global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
        <script src="<?=$this->Url->build('/assets/', true);?>global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
        <script src="<?=$this->Url->build('/assets/', true);?>global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
        <script src="<?=$this->Url->build('/assets/', true);?>global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
        <script src="<?=$this->Url->build('/assets/', true);?>global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
        <!-- END AMCHART  PLUGINS -->


                <script>
                    jQuery(document).ready(function () {

                        $('#clickmewow').click(function () {
                            $('#radio1003').attr('checked', 'checked');
                        });



                        $('.date-picker').datepicker({
                            //rtl: Metronic.isRTL(),
                            orientation: "left",
                            autoclose: true
                        });

                        $('.timepicker-no-seconds').timepicker({
                            autoclose: true,
                            minuteStep: 5
                        });

                        $('.spinner3').spinner({
                            value: 0,
                            min: 0,
                            max: 20
                        });



                        $('.bs-select').selectpicker({
                            iconBase: 'fa',
                            tickIcon: 'fa-check'
                        });

                        jQuery(document).on('click', '.notification', function (e) {
                            //e.preventDefault();
                            var id = $(this).attr("notification_id");

                            //alert(id);
                            //return;
                            //var id = $(this).attr('pid');

                            // $.post(
                            //     "", {
                            //         'id': id,
                            //     }).done(function () {}).fail(function () {});

                        });
                    });

                </script>
    </body>

    </html>
