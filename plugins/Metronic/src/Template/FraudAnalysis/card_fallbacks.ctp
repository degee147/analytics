<?php $this->assign('title', 'Card Fallbacks | Fraud Analysis ');?>
<link href="<?=$this->Url->build('/', true);?>assets/global/plugins/jstree/dist/themes/default/style.min.css" rel="stylesheet"
    type="text/css" />

<style>

    .jstree-table-wrapper {
  border: 1px solid #ccc;
}
.cat_column{
    /* width:250px !important; */
    max-width: 150px !important;;
}
.status_column{
    /* width:250px !important; */
    max-width: 150px !important;;
}
   </style>
<!-- BEGIN PAGE HEAD-->
<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Card Fallbacks
            <small>When are card fallbacks occuring</small>
        </h1>
    </div>
    <!-- END PAGE TITLE -->

</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <?=$this->Html->link('Dashboard', ['prefix' => false, 'controller' => 'dashboard', 'action' => 'index'], ['escape' => false]);?>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        Fraud Analysis
        <i class="fa fa-circle"></i>
    </li>
    <li>
        Card Fallbacks
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN ROW -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN CHART PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>
                            <span class="caption-subject bold uppercase font-green-haze"> Card Fallbacks</span>
                            <span class="caption-helper">When are card fallbacks occuring</span>
                        </div>
                        <div class="tools">
                            <!-- <a href="javascript:;" class="collapse"> </a> -->
                            <!-- <a href="#portlet-config" data-toggle="modal" class="config"> </a> -->
                            <a href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="fullscreen"> </a>
                            <!-- <a href="javascript:;" class="remove"> </a> -->
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-scrollable">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th scope="col"> Time </th>
                                                <th scope="col"> Entity </th>
                                                <th scope="col"> ID </th>
                                                <th scope="col"> Customer </th>
                                                <th scope="col"> Priority </th>
                                                <th scope="col"> Seq Num </th>
                                                <th scope="col"> Type </th>
                                                <th scope="col"> Name </th>
                                                <th scope="col"> Details </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td> Aug 7th 12:33pm </td>
                                                <td> 1002934</td>
                                                <td> 23 </td>
                                                <td> Customer </td>
                                                <td> <span class="label label-warning"> Warning </span> </td>
                                                <td> 0147887</td>
                                                <td> Event </td>
                                                <td> Fallback </td>
                                                <td> Event in 3.23secs </td>
                                            </tr>
                                            <tr>
                                                <td> Aug 7th 12:33pm </td>
                                                <td> 1002934</td>
                                                <td> 23 </td>
                                                <td> Customer </td>
                                                <td> <span class="label label-danger"> Danger </span> </td>
                                                <td> 0147887</td>
                                                <td> Event </td>
                                                <td> Fallback </td>
                                                <td> Event in 3.23secs </td>
                                            </tr>
                                            <tr>
                                                <td> Aug 7th 12:33pm </td>
                                                <td> 1002934</td>
                                                <td> 23 </td>
                                                <td> Customer </td>
                                                <td> <span class="label label-info"> Info </span> </td>
                                                <td> 0147887</td>
                                                <td> Event </td>
                                                <td> Fallback </td>
                                                <td> Event in 3.23secs </td>
                                            </tr>
                                            <tr>
                                                <td> Aug 7th 12:33pm </td>
                                                <td> 1002934</td>
                                                <td> 23 </td>
                                                <td> Customer </td>
                                                <td> <span class="label label-warning"> Warning </span> </td>
                                                <td> 0147887</td>
                                                <td> Event </td>
                                                <td> Fallback </td>
                                                <td> Event in 3.23secs </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <h4 style="margin-left:20px;">Transaction Summaries</h4>
                            <div class="row">
                                <!-- <div class="col-md-4">
                                    <div id="tree_1" class="tree-demo" style="margin-left:20px;">
                                        <ul>
                                            <li> Withdrawals
                                                <ul>
                                                    <li data-jstree='{ "selected" : true }'>
                                                        <a href="javascript:;"> Initially selected </a>
                                                    </li>
                                                    <li data-jstree='{ "icon" : "fa fa-briefcase icon-state-success " }'>
                                                        custom icon URL </li>
                                                    <li data-jstree='{ "opened" : true }'> initially open
                                                        <ul>
                                                            <li data-jstree='{ "disabled" : true }'> Disabled Node </li>
                                                            <li data-jstree='{ "type" : "file" }'> Another node </li>
                                                        </ul>
                                                    </li>
                                                    <li data-jstree='{ "icon" : "fa fa-warning icon-state-danger" }'>
                                                        Custom icon class (bootstrap) </li>
                                                </ul>
                                            </li>
                                            <li data-jstree='{ "type" : "file" }'>
                                                <a href="http://www.jstree.com"> Clickanle link node </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div> -->
                                <div class="col-md-5" style="margin-left:20px;">
                                    <div id="jstree"></div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mt-element-list">
                                        <div class="mt-list-head list-todo red">
                                            <div class="list-head-title-container">
                                                <h3 class="list-title">Details on selected Summary</h3>                                              
                                            </div>                                          
                                        </div>
                                        <div class="mt-list-container list-todo" id="accordion1" role="tablist"
                                            aria-multiselectable="true">
                                            <div class="list-todo-line"></div>
                                            <ul>
                                                <li class="mt-list-item">
                                                    <div class="list-todo-icon bg-white">
                                                        <i class="fa fa-database"></i>
                                                    </div>
                                                    <div class="list-todo-item dark">
                                                        <a class="list-toggle-container" data-toggle="collapse"
                                                            data-parent="#accordion1" onclick=" " href="#task-1"
                                                            aria-expanded="false">
                                                            <div class="list-toggle done uppercase">
                                                                <div class="list-toggle-title bold">Content</div>
                                                                <!-- <div class="badge badge-default pull-right bold">3</div> -->
                                                            </div>
                                                        </a>
                                                        <div class="task-list panel-collapse collapse in" id="task-1">
                                                            <ul>
                                                                <li class="task-list-item done">
                                                                    <div class="task-icon">
                                                                        <a href="javascript:;">
                                                                            <i class="fa fa-database"></i>
                                                                        </a>
                                                                    </div>                                                                   
                                                                    <div class="task-content">
                                                                        <h4 class="uppercase bold">
                                                                            <a href="javascript:;">Lorem ipsum dolor</a>
                                                                        </h4>
                                                                        <p>Lorem ipsum dolor sit amet,
                                                                            consectetur adipiscing elit. Donec
                                                                            elementum gravida mauris, a
                                                                            tincidunt dolor porttitor eu. </p>
                                                                    </div>
                                                                </li>
                                                                <li class="task-list-item">
                                                                    <div class="task-icon">
                                                                        <a href="javascript:;">
                                                                            <i class="fa fa-database"></i>
                                                                        </a>
                                                                    </div>                                                                   
                                                                    <div class="task-content">
                                                                        <h4 class="uppercase bold">
                                                                            <a href="javascript:;">Lorem ipsum dolor</a>
                                                                        </h4>
                                                                        <p>Lorem ipsum dolor sit amet,
                                                                            consectetur adipiscing elit. Donec
                                                                            elementum gravida mauris, a
                                                                            tincidunt dolor porttitor eu. </p>
                                                                    </div>
                                                                </li>
                                                                <li class="task-list-item">
                                                                    <div class="task-icon">
                                                                        <a href="javascript:;">
                                                                            <i class="fa fa-database"></i>
                                                                        </a>
                                                                    </div>                                                                   
                                                                    <div class="task-content">
                                                                        <h4 class="uppercase bold">
                                                                            <a href="javascript:;">Lorem ipsum dolor</a>
                                                                        </h4>
                                                                        <p>Lorem ipsum dolor sit amet,
                                                                            consectetur adipiscing elit. Donec
                                                                            elementum gravida mauris, a
                                                                            tincidunt dolor porttitor eu. </p>
                                                                    </div>
                                                                </li>                                                                
                                                            </ul>                                                        
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="mt-list-item">
                                                    <div class="list-todo-icon bg-white">
                                                        <i class="fa fa-paint-brush"></i>
                                                    </div>
                                                    <div class="list-todo-item dark">
                                                        <a class="list-toggle-container" data-toggle="collapse"
                                                            data-parent="#accordion1" href="#task-2" aria-expanded="false">
                                                            <div class="list-toggle done uppercase">
                                                                <div class="list-toggle-title bold">Transport</div>
                                                                <div class="badge badge-default pull-right bold">3</div>
                                                            </div>
                                                        </a>
                                                        <div class="task-list panel-collapse collapse" id="task-2">
                                                        <ul>
                                                                <li class="task-list-item done">
                                                                    <div class="task-icon">
                                                                        <a href="javascript:;">
                                                                            <i class="fa fa-database"></i>
                                                                        </a>
                                                                    </div>                                                                   
                                                                    <div class="task-content">
                                                                        <h4 class="uppercase bold">
                                                                            <a href="javascript:;">Lorem ipsum dolor</a>
                                                                        </h4>
                                                                        <p>Lorem ipsum dolor sit amet,
                                                                            consectetur adipiscing elit. Donec
                                                                            elementum gravida mauris, a
                                                                            tincidunt dolor porttitor eu. </p>
                                                                    </div>
                                                                </li>
                                                                <li class="task-list-item">
                                                                    <div class="task-icon">
                                                                        <a href="javascript:;">
                                                                            <i class="fa fa-database"></i>
                                                                        </a>
                                                                    </div>                                                                   
                                                                    <div class="task-content">
                                                                        <h4 class="uppercase bold">
                                                                            <a href="javascript:;">Lorem ipsum dolor</a>
                                                                        </h4>
                                                                        <p>Lorem ipsum dolor sit amet,
                                                                            consectetur adipiscing elit. Donec
                                                                            elementum gravida mauris, a
                                                                            tincidunt dolor porttitor eu. </p>
                                                                    </div>
                                                                </li>
                                                                <li class="task-list-item">
                                                                    <div class="task-icon">
                                                                        <a href="javascript:;">
                                                                            <i class="fa fa-database"></i>
                                                                        </a>
                                                                    </div>                                                                   
                                                                    <div class="task-content">
                                                                        <h4 class="uppercase bold">
                                                                            <a href="javascript:;">Lorem ipsum dolor</a>
                                                                        </h4>
                                                                        <p>Lorem ipsum dolor sit amet,
                                                                            consectetur adipiscing elit. Donec
                                                                            elementum gravida mauris, a
                                                                            tincidunt dolor porttitor eu. </p>
                                                                    </div>
                                                                </li>                                                                
                                                            </ul>                                                        
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="mt-list-item">
                                                    <div class="list-todo-icon bg-white">
                                                        <i class="fa fa-sticky-note-o"></i>
                                                    </div>
                                                    <div class="list-todo-item dark">
                                                        <a class="list-toggle-container font-white" data-toggle="collapse"
                                                            data-parent="#accordion1" href="#task-3" aria-expanded="false">
                                                            <div class="list-toggle done uppercase">
                                                                <div class="list-toggle-title bold">Other</div>
                                                                <div class="badge badge-default pull-right bold">2</div>
                                                            </div>
                                                        </a>
                                                        <div class="task-list panel-collapse collapse" id="task-3">
                                                            <ul>
                                                                <li class="task-list-item done">
                                                                    <div class="task-icon">
                                                                        <a href="javascript:;">
                                                                            <i class="fa fa-navicon"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="task-status">
                                                                        <a class="done" href="javascript:;">
                                                                            <i class="fa fa-check"></i>
                                                                        </a>
                                                                        <a class="pending" href="javascript:;">
                                                                            <i class="fa fa-close"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="task-content">
                                                                        <h4 class="uppercase bold">
                                                                            <a href="javascript:;">Artwork
                                                                                Slicing</a>
                                                                        </h4>
                                                                        <p>Lorem ipsum dolor sit amet,
                                                                            consectetur adipiscing elit. Donec
                                                                            elementum gravida mauris, a
                                                                            tincidunt dolor porttitor eu. </p>
                                                                    </div>
                                                                </li>
                                                                <li class="task-list-item">
                                                                    <div class="task-icon">
                                                                        <a href="javascript:;">
                                                                            <i class="fa fa-cube"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="task-status">
                                                                        <a class="done" href="javascript:;">
                                                                            <i class="fa fa-check"></i>
                                                                        </a>
                                                                        <a class="pending" href="javascript:;">
                                                                            <i class="fa fa-close"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="task-content">
                                                                        <h4 class="uppercase bold">
                                                                            <a href="javascript:;">Backend
                                                                                Integration</a>
                                                                        </h4>
                                                                        <p>Lorem ipsum dolor sit amet,
                                                                            consectetur adipiscing elit. Donec
                                                                            elementum gravida mauris, a
                                                                            tincidunt dolor porttitor eu. </p>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                            <div class="task-footer bg-grey">
                                                                <div class="row">
                                                                    <div class="col-xs-6">
                                                                        <a class="task-trash" href="javascript:;">
                                                                            <i class="fa fa-trash"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <a class="task-add" href="javascript:;">
                                                                            <i class="fa fa-plus"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- END CHART PORTLET-->
            </div>
        </div>
        <!-- END ROW -->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<script src="<?=$this->Url->build('/', true);?>assets/global/plugins/jstree/dist/jstree.min.js" type="text/javascript"></script>
<script src="<?=$this->Url->build('/', true);?>assets/global/plugins/jstree/dist/jstreetable.js" type="text/javascript"></script>
<?php //echo $this->element('treejs')?>

<script>
    jQuery(document).ready(function () {

        // tree data
        var data;
        data = [{
            text: "Withdrawal",
            data: {},
            children: [{
                text: "NDC+ Transaction Request",
                data: {},
                children: [{
                        text: "TCP SYN",
                        data: {
                            price: 0.1,
                            quantity: 20
                        }
                    },
                    {
                        text: "SYN ACK",
                        data: {
                            price: 0.2,
                            quantity: 31
                        }
                    },
                    {
                        text: "ACK",
                        data: {
                            price: 1.99,
                            quantity: 34
                        }
                    },

                ],
                'state': {
                    'opened': true
                }
            }, {
                text: "NDC+ Solicitated Status",
                data: {},
                children: [{
                        text: "TCP SYN",
                        data: {
                            price: 0.1,
                            quantity: 20
                        }
                    },
                    {
                        text: "SYN ACK",
                        data: {
                            price: 0.2,
                            quantity: 31
                        }
                    },
                    {
                        text: "ACK",
                        data: {
                            price: 1.99,
                            quantity: 34
                        }
                    },

                ],
            }],
            'state': {
                'opened': true
            }
        }];

        // load jstree
        $("div#jstree").jstree({
            plugins: ["table", "dnd", "contextmenu", "sort"],
            core: {
                data: data
            },
            // configure tree table
            table: {
                columns: [{
                        width: 300,
                        columnWidth: 300,
                        columnClass: "name_column",
                        header: "Name"
                    },
                    {
                        width: 250,
                        columnWidth: 300,
                        columnClass: "cat_column",
                        value: "price",
                        header: "Category",
                        format: function (v) {
                            if (v) {
                                return 'NDC+';
                                //return '<?=$naira?>' + v.toFixed(2);
                            }
                        }
                    },
                    {
                        width: 250,
                        columnWidth: 300,
                        columnClass: "status_column",
                        value: "quantity",
                        header: "Status",
                        format: function (v) {
                            return 'Normal';
                        }
                    }
                ],
                resizable: true,
                draggable: true,
                contextmenu: true,
                width: 400,
                height: 300
            }
        });


        // initTree('#tree_1');
    });

</script>
