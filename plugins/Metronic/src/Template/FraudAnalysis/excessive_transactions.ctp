<?php $this->assign('title', 'Excessive Transactions | Fraud Analysis ');?>
<!-- BEGIN PAGE HEAD-->
<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Excessive Transactions
            <small>When is excessive transaction activity occurring</small>
        </h1>
    </div>
    <!-- END PAGE TITLE -->

</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <?=$this->Html->link('Dashboard', ['prefix' => false, 'controller' => 'dashboard', 'action' => 'index'], ['escape' => false]);?>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        Fraud Analysis
        <i class="fa fa-circle"></i>
    </li>
    <li>
        Excessive/Unusual Transactions
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->
<?= $this->element('am_naija_map_styles')?>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN ROW -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN CHART PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>
                            <span class="caption-subject bold uppercase font-green-haze"> Excessive/Unusual Transactions</span>
                            <span class="caption-helper">When is excessive transaction activity occurring</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="fullscreen"> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-4" style="text-align: right;">
                                        <label for="">Date Range</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="input-group" id="defaultrange" style="width: 100%;">
                                            <input type="text" class="form-control">
                                            <span class="input-group-btn">
                                                <button class="btn default date-range-toggle" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <!-- <span class="help-block"> Date range </span> -->
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="countryMapDiv" class="chart" style="height: 400px;width:100%"> </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p style="margin-bottom: 1px;">Color Code</p>
                                        <div class="row">
                                            <div class="table-scrollable" style="display: inline-flex;">
                                                <div class="col-md-3"><span class="badge bg-blue badge-roundless"> 
                                                    </span> <br> <?=$naira?>100,000</div>
                                                <div class="col-md-3"><span class="badge bg-red badge-roundless">
                                                         </span><br><?=$naira?>150,000</div>
                                                <div class="col-md-3"><span class="badge bg-purple badge-roundless"> 
                                                    </span><br><?=$naira?>200,000</div>
                                                

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h4>Transaction by Time on flagged ATM</h4>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="timeMapDiv" class="chart" style="height: 400px;width:100%"> </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <!-- END CHART PORTLET-->
            </div>
        </div>
        <!-- END ROW -->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->
<?php echo $this->element('date_range_script')?>
<?php echo $this->element('am_customhtmlmarkers')?>
<?php echo $this->element('am_timebasedmap')?>
<script>
    jQuery(document).ready(function () {

        //initChartSample10();
        // initCountryMap();
        //startCountriesMap("countryMapDiv");
        startCustomHTMLMap("countryMapDiv", null, "ATM Locations");
        //startCountriesMap("countryMapDiv", "percent");
        //start3LinesChart('threelines');
        initTimeBasedMap('timeMapDiv');
        initDateRange('#defaultrange','long');

        var colors = ['#3598DC', '#8E44AD', '#E7505A'];

        //document.getElementById('title').style.color = random_color;

        setTimeout(function () {

            //console.log("yes");

            //demo that gives all blinking dots random color
            $(".pulse").each(function (index) {
                //console.log( index + ": " + $( this ).text() );
                var random_color = colors[Math.floor(Math.random() * colors.length)];
                //console.log(this);
                //console.log(random_color);
                $(this).css('border-color', random_color);
            });

        }, 10000);



    });

</script>
