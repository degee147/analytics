<?php $this->assign('title', 'Flagged Customers | Fraud Analysis ');?>

<!-- BEGIN PAGE HEAD-->
<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Flagged Customers
            <small>What customers have been flagged due to high-risk transactions</small>
        </h1>
    </div>
    <!-- END PAGE TITLE -->

</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <?=$this->Html->link('Dashboard', ['prefix' => false, 'controller' => 'dashboard', 'action' => 'index'], ['escape' => false]);?>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        Fraud Analysis
        <i class="fa fa-circle"></i>
    </li>
    <li>
        Flagged Customers
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->
<?= $this->element('am_naija_map_styles')?>
<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN ROW -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN CHART PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>
                            <span class="caption-subject bold uppercase font-green-haze"> Flagged Customers</span>
                            <span class="caption-helper">What customers have been flagged due to high-risk transactions</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="fullscreen"> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row"> 
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group" id="" style="width: 100%;">
                                            <input type="number" value="14" min="10" max="30" class="form-control">
                                            <span class="input-group-btn">
                                                <button class="btn default date-range-toggle" type="button">
                                                    <i class="fa fa-users"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <span class="help-block"> Customers to Show </span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-md-line-input form-md-floating-label has-info">
                                            <select class="form-control" id="form_control_1">
                                                <option value=""></option>
                                                <option value="1">Card</option>
                                                <option value="2">Bank</option>
                                            </select>
                                            <label for="form_control_1"><span style="font-size: .75em;">Transaction
                                                    Type</span></label>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-scrollable" style="display: inline-flex;">
                                            <div class="" style="width:1600px;">
                                                <h4 style="text-align: center;">Withdrawals</h4>
                                                <div id="memmbermap" class="chart" style="height: 300px; width:400px">
                                                </div>
                                            </div>
                                            <div class="" style="width:800px;">
                                                <h4 style="text-align: center;">Deposits</h4>
                                                <div id="memmbermap2" class="chart" style="height: 300px; width:400px;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- <p style="margin-bottom: 1px;">Color Codes</p> -->
                                        <!-- <div class="row">
                                            <div class="table-scrollable" style="display: inline-flex;">
                                                <div class="col-md-6"><span class="badge bg-yellow-gold badge-roundless">
                                                    </span> &nbsp;&nbsp;Master Card</div>
                                                <div class="col-md-6"><span class="badge bg-green-meadow badge-roundless">
                                                    </span> &nbsp;&nbsp;Visa Card</div>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>Transaction Locations for selected User</h4>
                                        <!-- <p>Selected Target Average: &#8358;7,000</p> -->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="countryMapDiv" class="chart" style="height: 400px;width:100%"> </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Transactions Overtime by Selected User</h4>
                                <div id="timeMapDiv" class="chart" style="height: 400px;width:100%"> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CHART PORTLET-->
            </div>
        </div>
        <!-- END ROW -->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
<?php echo $this->element('am_verticalhosrizontalbar')?>
<?php echo $this->element('date_range_script')?>
<?php echo $this->element('am_3lines')?>
<?php echo $this->element('am_customhtmlmarkers')?>
<?php echo $this->element('am_timebasedmap')?>
<script>
    jQuery(document).ready(function () {

        initDateRange('#defaultrange');

        startVerticalHorizontalBarChart('memmbermap', true);
        startVerticalHorizontalBarChart('memmbermap2', true);
        //start3LinesChart('threelines');

        startCustomHTMLMap("countryMapDiv", null, "User Transaction Locations");
        //startCountriesMap("countryMapDiv", "percent");
        //start3LinesChart('threelines');
        initTimeBasedMap('timeMapDiv');



    });

</script>
