<?php $this->assign('title', 'Performance | Card Usage ');?>
<!-- BEGIN PAGE HEAD-->
<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Performance
            <small>How is the performance of cards</small>
        </h1>
    </div>
    <!-- END PAGE TITLE -->

</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <?=$this->Html->link('Dashboard', ['prefix' => false, 'controller' => 'dashboard', 'action' => 'index'], ['escape' => false]);?>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        Card Usage
        <i class="fa fa-circle"></i>
    </li>
    <li>
        Performance
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN ROW -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN CHART PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>
                            <span class="caption-subject bold uppercase font-green-haze"> Performance</span>
                            <span class="caption-helper">How is the performance of cards</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="fullscreen"> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group" id="defaultrange" style="width: 100%;">
                                            <input type="text" class="form-control">
                                            <span class="input-group-btn">
                                                <button class="btn default date-range-toggle" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <span class="help-block"> Date range </span>

                                    </div>
                                    <div class="col-md-12">
                                        <h4 style="text-align:center">Card Transactions</h4>
                                        <div id="cardchart" class="chart" style="height: 400px;width:100%"> </div>
                                    </div>
                                    <div class="col-md-12">
                                        <h4 style="text-align:center">Average Transactions</h4>
                                        <div id="logmap" class="chart" style="height: 400px;width:100%"> </div>
                                    </div>
                                    <div class="col-md-12">
                                        <h4 style="text-align:center">Transactions by Amount</h4>
                                        <div id="transactionsbyamount" class="chart" style="height: 400px;width:100%"> </div>
                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                    <div class="col-md-12">
                                        <h4 style="text-align:center">Card Type Usage</h4>
                                        <div id="cardtypeusage" class="chart" style="height: 400px;width:100%"> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CHART PORTLET-->
            </div>
        </div>
        <!-- END ROW -->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->
<?php echo $this->element('am_cardchart')?>
<?php echo $this->element('date_range_script')?>
<?php echo $this->element('am_logarithmic')?>
<?php echo $this->element('am_3dstackedcolumnchart')?>
<?php echo $this->element('am_3dpiechartcard')?>
<script>
    jQuery(document).ready(function () {

        initDateRange('#defaultrange', 'long');
        //start3DBarChart('map2');
        initCardChart('cardchart');
        initLogMap('logmap');
        init3dstackedColumnChart('transactionsbyamount');
        init3dPieChartCard('cardtypeusage');


    });

</script>
