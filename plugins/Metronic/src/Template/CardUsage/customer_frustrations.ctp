<?php $this->assign('title', 'Customer Frustrations | Card Usage ');?>
<!-- BEGIN PAGE HEAD-->
<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Customer Frustrations
            <small>What issues are causing our customers pain</small>
        </h1>
    </div>
    <!-- END PAGE TITLE -->

</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <?=$this->Html->link('Dashboard', ['prefix' => false, 'controller' => 'dashboard', 'action' => 'index'], ['escape' => false]);?>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        Card Usage
        <i class="fa fa-circle"></i>
    </li>
    <li>
        Customer Frustrations
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN ROW -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN CHART PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>
                            <span class="caption-subject bold uppercase font-green-haze"> Customer Frustrations</span>
                            <span class="caption-helper">What issues are causing our customers pain</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="fullscreen"> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-4">
                                <h4>Transaction Success Rate: 86%</h4>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label has-info">
                                    <select class="form-control" id="form_control_1">
                                        <option value=""></option>
                                        <option value="1">Total incidents</option>
                                        <option value="2">Transaction Type</option>
                                    </select>
                                    <label for="form_control_1"><span style="font-size: .75em;">Show Frustration By</span></label>
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="input-group" id="defaultrange" style="width: 100%;">
                                    <input type="text" class="form-control">
                                    <span class="input-group-btn">
                                        <button class="btn default date-range-toggle" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                                <span class="help-block"> Date range </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h4 style="text-align:center">Nigerian States</h4>
                                <div id="heatmap" class="chart" style="height: 400px;width:100%"> </div>
                            </div>
                            <div class="col-md-12">
                                <h4 style="text-align:center">Transaction Failures for Cities in Selected State</h4>
                                <div id="stackbar2" class="chart" style="height: 400px;width:100%"> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CHART PORTLET-->
            </div>
        </div>
        <!-- END ROW -->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->
<?php echo $this->element('date_range_script')?>
<?php echo $this->element('am_heatmap')?>
<?php echo $this->element('am_stackedbarchat3')?>
<script>
    jQuery(document).ready(function () {

        //initChartSample10();
        // initCountryMap();
        //startCountriesMap("countryMapDiv");
        //startCustomHTMLMap("countryMapDiv", null, "ATM Locations");
        //startCountriesMap("countryMapDiv", "percent");
        //start3LinesChart('threelines');

        initDateRange('#defaultrange', 'long');
        initHeatMap('heatmap', false);
        init3StackedBarChart('stackbar2', true);
        //start3DBarChart('map2');

        //init3DColumnChart('map2');
        //initLogMap('logmap');



    });

</script>
