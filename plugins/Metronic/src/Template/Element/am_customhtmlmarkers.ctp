<!-- Already included Nigeria Map JS in Asset Compress -->
<!-- Chart code -->
<script>
    /**
     * This example uses pulsating circles CSS by Kevin Urrutia
     * http://kevinurrutia.tumblr.com/post/16411271583/creating-a-css3-pulsating-circle
     */
    function startCustomHTMLMap(div, percent = null, message = null) {
        var map = AmCharts.makeChart(div, {
            "type": "map",
            "theme": "light",
            "projection": "miller",
            "titles": [{
                "text": message != null ? message : "POS Terminals Location around Nigeria",
                "size": 14
            }
            /* , {
                "text": "source: Gapminder",
                "size": 11
            } */],

            "imagesSettings": {
                "rollOverColor": "#089282",
                "rollOverScale": 3,
                "selectedScale": 3,
                "selectedColor": "#089282",
                "color": "#13564e"
            },
			"export": {
                "enabled": true,
				"position": "bottom-right"
            },
            "areasSettings": {
                "unlistedAreasColor": "#15A892"
            },

            "dataProvider": {
                //"map": "nigeriaLow",
                "map": "nigeriaHigh",
                // "map": "worldLow",
                "images": [{
                    "zoomLevel": 5,
                    "scale": 0.5,
                    "title": "Rivers State",
                    "longitude": 4.8581,
                    "latitude": 6.9209
                }, {
                    "zoomLevel": 5,
                    "scale": 0.5,
                    "title": "Nnewi",
                    "latitude": 6.010519,
                    "longitude": 6.910345
                }, {
                    "zoomLevel": 5,
                    "scale": 0.5,
                    "title": "Agbor",
                    "latitude": 6.264092,
                    "longitude": 6.201883
                }, {
                    "zoomLevel": 5,
                    "scale": 0.5,
                    "title": "Ikeja",
                    "latitude": 6.605874,
                    "longitude": 3.349149
                }, {
                    "zoomLevel": 5,
                    "scale": 0.5,
                    "title": "Ughelli",
                    "latitude": 5.500187,
                    "longitude": 5.993834
                }, {
                    "zoomLevel": 5,
                    "scale": 0.5,
                    "title": "Akure",
                    "latitude": 7.250771,
                    "longitude": 5.210266
                }, {
                    "zoomLevel": 5,
                    "scale": 0.5,
                    "title": "Calabar",
                    "latitude": 4.982873,
                    "longitude": 8.334503,
                    "url": "http://www.google.co.uk"
                }, {
                    "zoomLevel": 5,
                    "scale": 0.5,
                    "title": "Sapele",
                    "latitude": 5.879698,
                    "longitude": 5.700531
                }, {
                    "zoomLevel": 5,
                    "scale": 0.5,
                    "title": "Eruwa",
                    "latitude": 7.536318,
                    "longitude": 3.418143
                }, {
                    "zoomLevel": 5,
                    "scale": 0.5,
                    "title": "Wudil",
                    "latitude": 11.794242,
                    "longitude": 8.839032,
                    "url": "http://www.google.co.jp"
                }, {
                    "zoomLevel": 5,
                    "scale": 0.5,
                    "title": "Aba",
                    "latitude": 5.10,
                    "longitude": 7.19
                }]
            }
        });

        // add events to recalculate map position when the map is moved or zoomed
        map.addListener("positionChanged", updateCustomMarkers);

        // this function will take current images on the map and create HTML elements for them
        function updateCustomMarkers(event) {
            // get map object
            var map = event.chart;

            // go through all of the images
            for (var x in map.dataProvider.images) {
                // get MapImage object
                var image = map.dataProvider.images[x];

                // check if it has corresponding HTML element
                if ('undefined' == typeof image.externalElement)
                    image.externalElement = createCustomMarker(image);

                // reposition the element accoridng to coordinates
                var xy = map.coordinatesToStageXY(image.longitude, image.latitude);
                image.externalElement.style.top = xy.y + 'px';
                image.externalElement.style.left = xy.x + 'px';
            }
        }

        // this function creates and returns a new marker element
        function createCustomMarker(image) {
            // create holder
            var holder = document.createElement('div');
            holder.className = 'map-marker';
            holder.title = image.title;
            holder.style.position = 'absolute';

            // maybe add a link to it?
            if (undefined != image.url) {
                holder.onclick = function () {
                    window.location.href = image.url;
                };
                holder.className += ' map-clickable';
            }

            // create dot
            var dot = document.createElement('div');
            dot.className = 'dot';
            holder.appendChild(dot);

            // create pulse
            var pulse = document.createElement('div');
            pulse.className = 'pulse';
            holder.appendChild(pulse);

            // append the marker to the map container
            image.chart.chartDiv.appendChild(holder);

            return holder;
        }
    }

</script>
