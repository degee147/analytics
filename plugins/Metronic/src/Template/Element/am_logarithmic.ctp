<!-- Chart code -->
<script>
    function initLogMap(div) {
        var chart = AmCharts.makeChart(div, {
            "pathToImages": App.getGlobalPluginsPath() + "amcharts/amcharts/images/",
            "type": "serial",
            "theme": "light",
            "marginRight": 80,
            "marginTop": 17,
            "autoMarginOffset": 20,
            "dataProvider": [{
                "date": "2018-03-01",
                "price": 200
            }, {
                "date": "2018-03-02",
                "price": 750
            }, {
                "date": "2018-03-03",
                "price": 150
            }, {
                "date": "2018-03-04",
                "price": 750
            }, {
                "date": "2018-03-05",
                "price": 1580
            }, {
                "date": "2018-03-06",
                "price": 570
            }, {
                "date": "2018-03-07",
                "price": 1070
            }, {
                "date": "2018-03-08",
                "price": 890
            }, {
                "date": "2018-03-09",
                "price": 750
            }, {
                "date": "2018-03-10",
                "price": 1320
            }, {
                "date": "2018-03-11",
                "price": 1580
            }, {
                "date": "2018-03-12",
                "price": 560
            }, {
                "date": "2018-03-13",
                "price": 1690
            }, {
                "date": "2018-03-14",
                "price": 240
            }, {
                "date": "2018-03-15",
                "price": 1470
            }],
            "valueAxes": [{
                "logarithmic": true,
                "dashLength": 1,
                "guides": [{
                    "dashLength": 6,
                    "inside": true,
                    "label": "Average",
                    "lineAlpha": 1,
                    "value": 900
                },{
                    "dashLength": 6,
                    "inside": true,
                    "label": "Minimum Target",
                    "lineAlpha": 1,
                    "value": 300
                }],
                "position": "left"
            }],
            "graphs": [{
                "bullet": "round",
                "id": "g1",
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "bulletSize": 7,
                "lineThickness": 2,
                "title": "Price",
                "type": "smoothedLine",
                "useLineColorForBulletBorder": true,
                "valueField": "price"
            }],
            "chartScrollbar": {},
            "chartCursor": {
                "valueLineEnabled": true,
                "valueLineBalloonEnabled": true,
                "valueLineAlpha": 0.5,
                "fullWidth": true,
                "cursorAlpha": 0.05
            },
            "dataDateFormat": "YYYY-MM-DD",
            "categoryField": "date",
            "categoryAxis": {
                "parseDates": true
            },
            "export": {
                "enabled": true
            }
        });

        chart.addListener("dataUpdated", zoomChart);

        function zoomChart() {
            chart.zoomToDates(new Date(2018, 2, 2), new Date(2018, 2, 10));
        }
    }

</script>
