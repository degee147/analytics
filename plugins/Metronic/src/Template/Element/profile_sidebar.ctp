<div class="profile-sidebar" style="width: 250px;">
    <!-- PORTLET MAIN -->
    <div class="portlet light profile-sidebar-portlet">
        <!-- SIDEBAR USERPIC -->
        <div class="profile-userpic">
            <img src="<?= !empty($profile_picture) && file_exists(WWW_ROOT . "uploads" . DS . "profile_pictures". DS . $profile_picture) ? $this->Url->build(DS, true) . "uploads" . DS . "profile_pictures". DS . $profile_picture : "http://www.placehold.it/150x150/EFEFEF/AAAAAA&amp;text=no+image" ?>" class="img-responsive" alt="" style="width: 100px; height: 100px;">
        </div>
        <!-- END SIDEBAR USERPIC -->
        <!-- SIDEBAR USER TITLE -->
        <div class="profile-usertitle">
            <div class="profile-usertitle-name">
                <?= ucwords($user->name_desc) ?>
            </div>
            <div class="profile-usertitle-job">
                <?= $user->random_key ?>
            </div>
        </div>
        <!-- END SIDEBAR USER TITLE -->
        <!-- SIDEBAR BUTTONS -->
        <div class="profile-userbuttons">
            <?= $this->Html->link('New Address', ['controller' => 'Addresses', 'action' => 'add'],['class'=>'btn btn-circle green-haze btn-sm','style'=>'padding: 8px;','escape' => false]);?>
            <?= $this->Html->link('New Subscription', ['controller' => 'subscriptions', 'action' => 'add'],['class'=>'btn btn-circle blue-hoki btn-sm','style'=>'padding: 8px;','escape' => false]);?>
        </div>
        <!-- END SIDEBAR BUTTONS -->
        <!-- SIDEBAR MENU -->
        <div class="profile-usermenu">
            <ul class="nav">
                <li class="<?= $page == "dashboard" ? "active" : "" ?>">
                    <?= $this->Html->link('<i class="icon-home"></i>Overview', ['controller' => 'Dashboard', 'action' => 'index'], ['escape' => false]); ?>
                </li>
                <li class="<?= $page == "dashboard_settings" ? "active" : "" ?>">
                    <?= $this->Html->link('<i class="icon-settings"></i>Account Settings', ['controller' => 'Dashboard', 'action' => 'AccountSettings'],['escape' => false]); ?>
                </li>
                <li class="<?= $page == "dashboard_deposit" ? "active" : "" ?>">
                    <?= $this->Html->link('<i class="fa fa-rupee"></i>Deposit Funds', ['prefix'=>'wallet','controller' => 'Deposit', 'action' => 'index'],['escape' => false]); ?>
                </li>
                <li class="<?= $page == "dashboard_help" ? "active" : "" ?>">
                    <?= $this->Html->link('<i class="icon-info"></i>Help', ['controller' => 'Dashboard', 'action' => 'helpAndFaqs'],['escape' => false]); ?>
                </li>
            </ul>
        </div>
        <!-- END MENU -->
    </div>
    <!-- END PORTLET MAIN -->
    <!-- PORTLET MAIN -->
    <div class="portlet light">
        <!-- STAT -->
        <div class="row list-separated profile-stat">
            <div class="col-md-4 col-sm-4 col-xs-6">
                <div class="uppercase profile-stat-title">
                    <?= count($backend_variables['custom_menus']) ?>
                </div>
                <div class="uppercase profile-stat-text">
                    Custom Menus
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-6">
                <div class="uppercase profile-stat-title">
                    <?= count($backend_variables['my_active_subscriptions']) ?>
                </div>
                <div class="uppercase profile-stat-text">
                    Active <span style="font-size: .8em;">Subscriptions</span>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-6">
                <div class="uppercase profile-stat-title">
                    <?= count($backend_variables['my_orders']) ?>
                </div>
                <div class="uppercase profile-stat-text">
                    Orders
                </div>
            </div>

        </div>
        <!-- END STAT -->
        <!--    <div>
<h4 class="profile-desc-title">About Marcus Doe</h4>
<span class="profile-desc-text"> Lorem ipsum dolor sit amet diam nonummy nibh dolore. </span>
<div class="margin-top-20 profile-desc-link">
<i class="fa fa-globe"></i>
<a href="http://www.keenthemes.com">www.keenthemes.com</a>
</div>
<div class="margin-top-20 profile-desc-link">
<i class="fa fa-twitter"></i>
<a href="http://www.twitter.com/keenthemes/">@keenthemes</a>
</div>
<div class="margin-top-20 profile-desc-link">
<i class="fa fa-facebook"></i>
<a href="http://www.facebook.com/keenthemes/">keenthemes</a>
</div>
</div>-->
    </div>
    <!-- END PORTLET MAIN -->
</div>
