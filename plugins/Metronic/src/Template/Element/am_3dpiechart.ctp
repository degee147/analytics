<!-- Chart code -->
<script>
    function init3dPieChart(div, manipulate_class, atmanalyzer = false) {
        var chart = AmCharts.makeChart(div, {
            "type": "pie",
            "theme": "light",
            "dataProvider": [{
                "country": atmanalyzer == true ? "Withdrawal" : "Air Peace",
                "color": "#3598DC",
                "value": 260
            }, {
                "country": atmanalyzer == true ? "Transfer":"Obi Cars",
                "color": "#1BBC9B",
                "value": 201
            }, {
                "country":atmanalyzer == true ? "Deposit": "AB Cleaners",
                "color": "#8E44AD",
                "value": 65
            }, {
                "country": atmanalyzer == true ? "Inquiry":"Super Mall",
                "color": "#E7505A",
                "value": 39
            }, {
                "country": atmanalyzer == true ? "Mobile Topup":"Well Pharmacy",
                "color": "#22313F",
                "value": 19
            }, {
                "country": atmanalyzer == true ? "Other":"Brain School",
                "color": "#C5BF66",
                "value": 10
            }],
            "valueField": "value",
            "titleField": "country",
            "colorField": "color",
            "outlineAlpha": 0.4,
            "depth3D": 15,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 30,
            "export": {
                "enabled": true
            }
        });

        jQuery('.' + manipulate_class).off().on('input change', function () {
            var property = jQuery(this).data('property');
            var target = chart;
            var value = Number(this.value);
            chart.startDuration = 0;

            if (property == 'innerRadius') {
                value += "%";
            }

            target[property] = value;
            chart.validateNow();
        });

        // $('#' + div).closest('.portlet').find('.fullscreen').click(function () {
        //     chart.invalidateSize();
        // });


    }

</script>

