
<!-- Chart code -->
<script>
function init3dstackedColumnChart(div){
    var chart = AmCharts.makeChart(div, {
    "theme": "light",
    "type": "serial",
    "dataProvider": [{
        "country": "0-20",
        "year2004": 3.5,
        "year2005": 4.2
    }, {
        "country": "21-40",
        "year2004": 1.7,
        "year2005": 3.1
    }, {
        "country": "41-60",
        "year2004": 2.8,
        "year2005": 2.9
    }, {
        "country": "61-80",
        "year2004": 2.6,
        "year2005": 2.3
    }, {
        "country": "81-100",
        "year2004": 1.4,
        "year2005": 2.1
    }, {
        "country": "101-200",
        "year2004": 2.6,
        "year2005": 4.9
    }, {
        "country": "201-300",
        "year2004": 6.4,
        "year2005": 7.2
    }, {
        "country": "301-400",
        "year2004": 8,
        "year2005": 7.1
    }, {
        "country": "401-500",
        "year2004": 9.9,
        "year2005": 10.1
    }],
    "valueAxes": [{
        "stackType": "3d",
        "unit": "k",
        "position": "left",
        "title": "Payment vs Refund",
    }],
    "startDuration": 1,
    "graphs": [{
        "balloonText": "Refunds [[category]] : <b>[[value]]</b>",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2004",
        "type": "column",
        "valueField": "year2004"
    }, {
        "balloonText": "Payment [[category]] : <b>[[value]]</b>",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2005",
        "type": "column",
        "valueField": "year2005"
    }],
    "plotAreaFillAlphas": 0.1,
    "depth3D": 60,
    "angle": 30,
    "categoryField": "country",
    "categoryAxis": {
        "gridPosition": "start"
    },
    "export": {
    	"enabled": true
     }
});
jQuery('.chart-input').off().on('input change',function() {
	var property	= jQuery(this).data('property');
	var target		= chart;
	chart.startDuration = 0;

	if ( property == 'topRadius') {
		target = chart.graphs[0];
      	if ( this.value == 0 ) {
          this.value = undefined;
      	}
	}

	target[property] = this.value;
	chart.validateNow();
});
}
</script>