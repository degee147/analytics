<script>
    function init3StackedBarChart(div, customer = false) {
        var chart = AmCharts.makeChart(div, {
            "type": "serial",
            "theme": "light",
            "legend": {
                "horizontalGap": 10,
                "maxColumns": 1,
                "position": "right",
                "useGraphSettings": true,
                "markerSize": 10
            },
            "dataProvider": [{
                "year": customer == true ? "City 1" : "User 1",
                "lamerica": 300,
                "meast": 200,
                "africa": 50
            }, {
                "year": customer == true ? "City 2" : "User 2",
                "lamerica": 300,
                "meast": 300,
                "africa": 115
            }, {
                "year": customer == true ? "City 3" : "User 3",
                "lamerica": 300,
                "meast": 300,
                "africa": 100
            }],
            "valueAxes": [{
                "stackType": "regular",
                "axisAlpha": 0.5,
                "gridAlpha": 0
            }],
            "graphs": [{
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": customer == true ? "Transfer" : "Customer",
                "type": "column",
                "color": "#5C8FAA",
                "valueField": "lamerica"
            }, {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": customer == true ? "Purchase" : "Partner",
                "type": "column",
                "color": "#000000",
                "valueField": "meast"
            }, {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": customer == true ? "Withdrawal" : "Other",
                "type": "column",
                "color": "#000000",
                "valueField": "africa"
            }],
            "rotate": true,
            "categoryField": "year",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "gridAlpha": 0,
                "position": "left"
            },
            "export": {
                "enabled": true
            }
        });
    }

</script>
