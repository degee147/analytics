<script>
    function init2StackedBarChart(div) {
        var chart = AmCharts.makeChart(div, {
            "type": "serial",
            "theme": "light",
            "legend": {
                "horizontalGap": 10,
                "maxColumns": 1,
                "position": "right",
                "useGraphSettings": true,
                "markerSize": 10
            },
            "dataProvider": [{
                "year": "User 1",
                "europe": 25000,
                "lamerica": 5500                
            }, {
                "year": "User 2",
                "europe": 26000,
                "lamerica": 3500
            }, {
                "year": "User 3",
                "europe": 32000,
                "lamerica": 8500,
            }],
            "valueAxes": [{
                "stackType": "regular",
                "axisAlpha": 0.5,
                "gridAlpha": 0
            }],
            "graphs": [{
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "Withdrawals",
                "type": "column",
                "color": "#000000",
                "valueField": "europe"
            },  {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "Deposits",
                "type": "column",
                "color": "#000000",
                "valueField": "lamerica"
            }],
            "rotate": true,
            "categoryField": "year",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "gridAlpha": 0,
                "position": "left"
            },
            "export": {
                "enabled": true
            }
        });
    }

</script>
