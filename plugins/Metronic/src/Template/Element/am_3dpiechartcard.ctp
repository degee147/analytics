<!-- Chart code -->
<script>
    function init3dPieChartCard(div) {
        var chart = AmCharts.makeChart(div, {
            "type": "pie",
            "theme": "light",
            "dataProvider": [{
                "country": "Master Card",
                "color": "#3598DC",
                "value": 260
            }, {
                "country": "Visa",
                "color": "#1BBC9B",
                "value": 201
            }, {
                "country":"Verve",
                "color": "#8E44AD",
                "value": 65
            }],
            "valueField": "value",
            "titleField": "country",
            "colorField": "color",
            "outlineAlpha": 0.4,
            "depth3D": 15,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 30,
            "export": {
                "enabled": true
            }
        });

        // jQuery('.' + manipulate_class).off().on('input change', function () {
        //     var property = jQuery(this).data('property');
        //     var target = chart;
        //     var value = Number(this.value);
        //     chart.startDuration = 0;

        //     if (property == 'innerRadius') {
        //         value += "%";
        //     }

        //     target[property] = value;
        //     chart.validateNow();
        // });

        // $('#' + div).closest('.portlet').find('.fullscreen').click(function () {
        //     chart.invalidateSize();
        // });


    }

</script>

