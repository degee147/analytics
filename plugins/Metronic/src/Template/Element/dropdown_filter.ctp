<div class="col-md-12">
    <div class="col-md-3">
        <select name="<?= $name ?>_category" class="input-sm dropdown_filter" target="<?= $name ?>" cat="<?= $name ?>_category" subcat="<?= $name ?>_subcategory" kitchen_id="<?= $kitchen_id ?>" is_category="true" subcat_target_div="<?= $name ?>_subcat_div">
            <option value="">All</option>
            <?php  foreach ($categories as $key => $value){   echo '<option value="'.$key.'">'.$value.'</option>';  }    ?>
        </select> <br>
        <span>Category</span>
    </div>
    <div class="col-md-4" style="display:none;" id="<?= $name ?>_subcat_div">
        <select name="<?= $name ?>_subcategory" class="input-sm dropdown_filter" target="<?= $name ?>" cat="<?= $name ?>_category" subcat="<?= $name ?>_subcategory" kitchen_id="<?= $kitchen_id ?>" is_category="false">
            <option value="">All</option>
            <?php   foreach ($subcategories as $key => $value){    echo '<option value="'.$key.'">'.$value.'</option>'; }  ?>
        </select> <br>
        <span>Subcategory</span>
    </div>
    <div class="col-md-2">
        <a href="javascript:;" class="btn btn-icon-only <!--btn-circle--> purple reset_filter" target="<?= $name ?>" cat="<?= $name ?>_category" subcat="<?= $name ?>_subcategory">
            <i class="fa fa-times"></i>
        </a>
    </div>
</div>
