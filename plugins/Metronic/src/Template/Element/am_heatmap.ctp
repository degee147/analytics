<!-- Chart code -->
<script>
    function initHeatMap(div, valueLegend = true){
        var map = AmCharts.makeChart(div, {
        "type": "map",
        "theme": "light",
        "colorSteps": 10,

        "dataProvider": {
            "map": "nigeriaHigh",
            "areas": [{
                id: "NG-AB",
                "value": 444710079867
            }, {
                id: "NG-AD",
                "value": 444450
            }, {
                id: "NG-AK",
                "value": 444713453400
            }, {
                id: "NG-AN",
                "value": 453560
            }, {
                id: "NG-BA",
                "value": 444745600
            }, {
                id: "NG-BE",
                "value": 32233
            }, {
                id: "NG-BO",
                "value": 36343400
            }, {
                id: "NG-BY",
                "value": 444540
            }, {
                id: "NG-CR",
                "value": 44476575100
            }, {
                id: "NG-DE",
                "value": 444710079867
            }, {
                id: "NG-EB",
                "value": 423123
            }, {
                id: "NG-ED",
                "value": 4989899990
            }, {
                id: "NG-EK",
                "value": 4447100
            }, {
                id: "NG-EN",
                "value": 4447100
            }, {
                id: "NG-FC",
                "value": 4447190700
            }, {
                id: "NG-GO",
                "value": 444710079867
            }, {
                id: "NG-IM",
                "value": 444710079867
            }, {
                id: "NG-JI",
                "value": 4443247100
            }, {
                id: "NG-KD",
                "value": 444724100
            }, {
                id: "NG-KE",
                "value": 44471234230
            }, {
                id: "NG-KN",
                "value": 4447100
            }, {
                id: "NG-KO",
                "value": 411447100
            }, {
                id: "NG-KT",
                "value": 11444117100
            }, {
                id: "NG-KW",
                "value": 444710079867
            }, {
                id: "NG-LA",
                "value": 4423447100
            }, {
                id: "NG-NA",
                "value": 4447100
            }, {
                id: "NG-NI",
                "value": 4447100
            }, {
                id: "NG-OG",
                "value": 4447100
            }, {
                id: "NG-ON",
                "value": 444710079867
            }, {
                id: "NG-OS",
                "value": 4447100
            }, {
                id: "NG-OY",
                "value": 4447100
            }, {
                id: "NG-PL",
                "value": 444710079867
            }, {
                id: "NG-RI",
                "value": 4447100
            }, {
                id: "NG-SO",
                "value": 444710079867
            }, {
                id: "NG-TA",
                "value": 4447100
            }, {
                id: "NG-YO",
                "value": 4447100
            }, {
                id: "NG-ZA",
                "value": 4447100
            }]
        },

        "areasSettings": {
            "autoZoom": true
        },

        "valueLegend": valueLegend == true ? {
            "right": 10,
            "minValue": "little",
            "maxValue": "a lot!"
        } : false,

        "export": {
            "enabled": true
        }

    });


    }
</script>
