<?php
$class = 'message';
if (!empty($params['class'])) {
    $class .= ' ' . $params['class'];
}
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<!--<div class="<?= h($class) ?>" onclick="this.classList.add('hidden');"><?= $message ?></div>-->
<div class="alert alert-info <?= h($class) ?>" onclick="this.classList.add('hidden');">
    <strong><!--Info!--><?= $message ?></strong>
</div>
