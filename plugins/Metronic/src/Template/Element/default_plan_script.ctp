<script>
    var Index = function () {

        return {

            initCalendar: function () {
                if (!jQuery().fullCalendar) {
                    return;
                }

                var date = new Date();
                var d = date.getDate();
                var m = date.getMonth();
                var y = date.getFullYear();

                var h = {};

                if ($('#calendar').width() <= 400) {
                    $('#calendar').addClass("mobile");
                    h = {
                        left: 'title, prev, next',
                        center: '',
                        right: 'today,month,agendaWeek,agendaDay'
                    };
                } else {
                    $('#calendar').removeClass("mobile");
                    if (Metronic.isRTL()) {
                        h = {
                            right: 'title',
                            center: '',
                            left: 'prev,next,today,month,agendaWeek,agendaDay'
                        };
                    } else {
                        h = {
                            left: 'title',
                            center: '',
                            //  right: 'prev,next,today,month,agendaWeek,agendaDay',
                            right: 'prev,next,today,month,agendaWeek,agendaDay',

                        };
                    }
                }



                $('#calendar').fullCalendar('destroy'); // destroy the calendar
                $('#calendar').fullCalendar({ //re-initialize the calendar
                    disableDragging : false,
                    header: h,
                    //header: false,
                    //editable: false,
                   // defaultView: 'agendaWeek',
                   // contentHeight: 'auto',
                    weekends: false, // will hide Saturdays and Sundays
                    minTime: "07:00:00",
                    maxTime: "21:00:00",
                    events: [],
                    eventRender: function(event, element) {
                        //appending details about the meal to the normal view
                        element.find('.fc-title').append("<br/>" + event.description);

                        //here we're using bootstrap's tooltip function to display more info on mouse hover
                        // see http://getbootstrap.com/javascript/#tooltips for more options
                        $(element).tooltip({
                            title: event.description,
                            html: true
                        });
                    }
                });


                $('#calendar').fullCalendar( 'addEventSource', function(start, end, timezone, callback) {
                    // When requested, dynamically generate a
                    // repeatable event for every monday.
                    var events = [];
                    var monday = 1;
                    var tuesday = 2;
                    var wednesday = 3;
                    var thursday = 4;
                    var friday = 5;
                    var one_day = (24 * 60 * 60 * 1000);

                    for (loop = start.toDate().getTime();
                         loop <= end.toDate().getTime();
                         loop = loop + one_day) {

                        var column_date = new Date(loop);

                        if (column_date.getDay() == monday) {
                            // we're in Moday, create the event
                            events.push({
                                title: 'Breakfast',
                                start: new Date(column_date.setHours(8, 00)),
                                end: new Date(column_date.setHours(10, 00)),
                                backgroundColor: Metronic.getBrandColor('purple'),
                                allDay: false,
                                description: '<?php
                                echo $this->Custom->displayDeliveryPlan($plan[0])["menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[0])["custom_menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[0])["products"];
                                ?>'
                            },{
                                title: 'Lunch',
                                start: new Date(column_date.setHours(12, 00)),
                                end: new Date(column_date.setHours(15, 00)),
                                backgroundColor: Metronic.getBrandColor('purple'),
                                allDay: false,
                                description: '<?php
                                echo $this->Custom->displayDeliveryPlan($plan[1])["menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[1])["custom_menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[1])["products"];
                                ?>'
                            },{
                                title: 'Dinner',
                                start: new Date(column_date.setHours(18, 00)),
                                end: new Date(column_date.setHours(20, 00)),
                                backgroundColor: Metronic.getBrandColor('purple'),
                                allDay: false,
                                description: '<?php
                                echo $this->Custom->displayDeliveryPlan($plan[2])["menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[2])["custom_menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[2])["products"];
                                ?>'
                            });
                        }
                        if (column_date.getDay() == tuesday) {
                            // we're in Moday, create the event
                            events.push({
                                title: 'Breakfast',
                                start: new Date(column_date.setHours(8, 00)),
                                end: new Date(column_date.setHours(10, 00)),
                                backgroundColor: Metronic.getBrandColor('grey'),
                                allDay: false,
                                description: '<?php
                                echo $this->Custom->displayDeliveryPlan($plan[3])["menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[3])["custom_menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[3])["products"];
                                ?>'
                            },{
                                title: 'Lunch',
                                start: new Date(column_date.setHours(12, 00)),
                                end: new Date(column_date.setHours(15, 00)),
                                backgroundColor: Metronic.getBrandColor('grey'),
                                allDay: false,
                                description: '<?php
                                echo $this->Custom->displayDeliveryPlan($plan[4])["menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[4])["custom_menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[4])["products"];
                                ?>'
                            },{
                                title: 'Dinner',
                                start: new Date(column_date.setHours(18, 00)),
                                end: new Date(column_date.setHours(20, 00)),
                                backgroundColor: Metronic.getBrandColor('grey'),
                                allDay: false,
                                description: '<?php
                                echo $this->Custom->displayDeliveryPlan($plan[5])["menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[5])["custom_menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[5])["products"];
                                ?>'
                            });
                        }
                        if (column_date.getDay() == wednesday) {
                            // we're in Moday, create the event
                            events.push({
                                title: 'Breakfast',
                                start: new Date(column_date.setHours(8, 00)),
                                end: new Date(column_date.setHours(10, 00)),
                                backgroundColor: Metronic.getBrandColor('black'),
                                allDay: false,
                                description: '<?php
                                echo $this->Custom->displayDeliveryPlan($plan[6])["menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[6])["custom_menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[6])["products"];
                                ?>'
                            },{
                                title: 'Lunch',
                                start: new Date(column_date.setHours(12, 00)),
                                end: new Date(column_date.setHours(15, 00)),
                                backgroundColor: Metronic.getBrandColor('black'),
                                allDay: false,
                                description: '<?php
                                echo $this->Custom->displayDeliveryPlan($plan[7])["menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[7])["custom_menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[7])["products"];
                                ?>'
                            },{
                                title: 'Dinner',
                                start: new Date(column_date.setHours(18, 00)),
                                end: new Date(column_date.setHours(20, 00)),
                                backgroundColor: Metronic.getBrandColor('black'),
                                allDay: false,
                                description: '<?php
                                echo $this->Custom->displayDeliveryPlan($plan[8])["menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[8])["custom_menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[8])["products"];
                                ?>'
                            });
                        }
                        if (column_date.getDay() == thursday) {
                            // we're in Moday, create the event
                            events.push({
                                title: 'Breakfast',
                                start: new Date(column_date.setHours(8, 00)),
                                end: new Date(column_date.setHours(10, 00)),
                                backgroundColor: Metronic.getBrandColor('green'),
                                allDay: false,
                                description: '<?php
                                echo $this->Custom->displayDeliveryPlan($plan[9])["menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[9])["custom_menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[9])["products"];
                                ?>'
                            },{
                                title: 'Lunch',
                                start: new Date(column_date.setHours(12, 00)),
                                end: new Date(column_date.setHours(15, 00)),
                                backgroundColor: Metronic.getBrandColor('green'),
                                allDay: false,
                                description: '<?php
                                echo $this->Custom->displayDeliveryPlan($plan[10])["menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[10])["custom_menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[10])["products"];
                                ?>'
                            },{
                                title: 'Dinner',
                                start: new Date(column_date.setHours(18, 00)),
                                end: new Date(column_date.setHours(20, 00)),
                                backgroundColor: Metronic.getBrandColor('green'),
                                allDay: false,
                                description: '<?php
                                echo $this->Custom->displayDeliveryPlan($plan[11])["menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[11])["custom_menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[11])["products"];
                                ?>'
                            });
                        }
                        if (column_date.getDay() == friday) {
                            // we're in Moday, create the event
                            events.push({
                                title: 'Breakfast',
                                start: new Date(column_date.setHours(8, 00)),
                                end: new Date(column_date.setHours(10, 00)),
                                backgroundColor: Metronic.getBrandColor('red'),
                                allDay: false,
                                description: '<?php
                                echo $this->Custom->displayDeliveryPlan($plan[12])["menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[12])["custom_menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[12])["products"];
                                ?>'
                            },{
                                title: 'Lunch',
                                start: new Date(column_date.setHours(12, 00)),
                                end: new Date(column_date.setHours(15, 00)),
                                backgroundColor: Metronic.getBrandColor('red'),
                                allDay: false,
                                description: '<?php
                                echo $this->Custom->displayDeliveryPlan($plan[13])["menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[13])["custom_menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[13])["products"];
                                ?>'
                            },{
                                title: 'Dinner',
                                start: new Date(column_date.setHours(18, 00)),
                                end: new Date(column_date.setHours(20, 00)),
                                backgroundColor: Metronic.getBrandColor('red'),
                                allDay: false,
                                description: '<?php
                                echo $this->Custom->displayDeliveryPlan($plan[14])["menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[14])["custom_menu"];
                                echo $this->Custom->displayDeliveryPlan($plan[14])["products"];
                                ?>'
                            });
                        }
                    } // for loop

                    // return events generated
                    callback( events );
                });

            },

        };

    }();

</script>
