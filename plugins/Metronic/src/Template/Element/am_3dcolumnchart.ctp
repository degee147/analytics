<script>
    function init3DColumnChart(div, atm = false, weekly = false) {
        var chart = AmCharts.makeChart(div, {
            "theme": "light",
            "type": "serial",
            "startDuration": 2,
            "dataProvider": [{
                "country": atm == true ? "ATM0001" : (weekly == true ? "Week 1": "Jan"),
                "visits": 40250,
                "color": "#FF0F00"
            }, {
                "country": atm == true ? "ATM0002" : (weekly == true ? "Week 2": "Feb"),
                "visits": 18820,
                "color": "#FF6600"
            }, {
                "country": atm == true ? "ATM0003" : (weekly == true ? "Week 3": "Mar"),
                "visits": 18090,
                "color": "#FF9E01"
            }, {
                "country": atm == true ? "ATM0004" :(weekly == true ? "Week 4": "Apr"),
                "visits": 13220,
                "color": "#FCD202"
            }, {
                "country": atm == true ? "ATM0005" : (weekly == true ? "Week 5": "May"),
                "visits": 11220,
                "color": "#F8FF01"
            }, {
                "country": atm == true ? "ATM0006" : (weekly == true ? "Week 6": "Jun"),
                "visits": 11140,
                "color": "#B0DE09"
            }, {
                "country": atm == true ? "ATM0007" : (weekly == true ? "Week 7": "Jul"),
                "visits": 9840,
                "color": "#04D215"
            }, {
                "country": atm == true ? "ATM0008" : (weekly == true ? "Week 8": "Aug"),
                "visits": 7110,
                "color": "#0D8ECF"
            }, {
                "country": atm == true ? "ATM0009" : (weekly == true ? "Week 9": "Sep"),
                "visits": 6650,
                "color": "#0D52D1"
            }, {
                "country": atm == true ? "ATM0010" : (weekly == true ? "Week 10": "Oct"),
                "visits": 5800,
                "color": "#2A0CD0"
            }, {
                "country": atm == true ? "ATM0011" : (weekly == true ? "Week 11": "Nov"),
                "visits": 4430,
                "color": "#8A0CCF"
            }, {
                "country": atm == true ? "ATM0012" : (weekly == true ? "Week 12": "Dec"),
                "visits": 44100,
                "color": "#CD0D74"
            }],
            "valueAxes": [{
                "position": "left",
                "title": "Amount"
            }],
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "fillColorsField": "color",
                "fillAlphas": 1,
                "lineAlpha": 0.1,
                "type": "column",
                "valueField": "visits"
            }],
            "depth3D": 20,
            "angle": 30,
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "country",
            "categoryAxis": {
                "gridPosition": "start",
                "labelRotation": 90
            },
            "export": {
                "enabled": true
            }

        });

    }

</script>
