<!-- Chart code -->
<script>
    function start3DBarChart(div) {
        //alert("got here");
        var chart = AmCharts.makeChart(div, {
            "theme": "light",
            "type": "serial",
            "dataProvider": [{
                "year": "User Error",
                "income": 53.5
            }, {
                "year": "Unavailable Service",
                "income": 36.2
            }, {
                "year": "Card Error",
                "income": 29.5
            }, {
                "year": "Account Error",
                "income": 24.6
            }, {
                "year": "Other",
                "income": 12.1
            }],
            "valueAxes": [{
                "title": "Income in millions, Naira"
            }],
            "graphs": [{
                "balloonText": "Income in [[category]]:[[value]]",
                "fillAlphas": 1,
                "lineAlpha": 0.2,
                "title": "Income",
                "type": "column",
                "valueField": "income"
            }],
            "depth3D": 20,
            "angle": 30,
            "rotate": true,
            "categoryField": "year",
            "categoryAxis": {
                "gridPosition": "start",
                "fillAlpha": 0.05,
                "position": "left"
            },
            "export": {
                "enabled": true
            }
        });
    }

</script>
