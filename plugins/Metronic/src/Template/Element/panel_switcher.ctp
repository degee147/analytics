<?php if(1==2): ?>
<?php if($this->Custom->userHasRole($user_roles, [2,3,4,5])): ?>
<div class="page-toolbar" > <!--margin-right: 100px;-->

	<div class="btn-group btn-group-circle" style=" position: relative;">
		<button type="button" class="btn purple"  data-toggle="dropdown" ><span class="md-click-circle" style="height: 73px; width: 73px; top: -14.5px; left: 2.04688px;"></span>Switch Panel</button>
		<button type="button" class="btn purple dropdown-toggle btn-circle-right" data-toggle="dropdown" aria-expanded="true"><span class="md-click-circle" style="height: 34px; width: 34px; top: 2px; left: 2px;"></span><i class="fa fa-angle-down"></i></button>
		<ul class="dropdown-menu" role="menu" style=" left: -32px;">
			<li>
				<?= $this->Html->link('Client',['prefix'=>'client','controller' => 'dashboard', 'action' => 'index'],['escape' => false]); ?>
			</li>
			<?php if ($isDeliveryPerson): ?>
			<li>
				<?= $this->Html->link('Delivery Person',['prefix'=>'delivery_person','controller' => 'orders', 'action' => 'pending'],['escape' => false]); ?>
			</li>
			<?php endif; ?>
			<?php if ($isKitchenOwner): ?>
			<li>
				<?= $this->Html->link('Kitchen Owner',['prefix'=>'kitchen_owner','controller' => 'dashboard', 'action' => 'index'],['escape' => false]); ?>
			</li>
			<?php endif; ?>
			<?php if ($this->Custom->userHasRole($user_roles, [4,5])): ?>
			<li class="divider">
			</li>
			<li>
				<?= $this->Html->link('Admin',['prefix'=>'admin','controller' => 'dashboard', 'action' => 'index'],['escape' => false]); ?>
			</li>
			<?php endif; ?>
		</ul>
	</div>
</div>
<?php endif; ?>
<?php endif; ?>
