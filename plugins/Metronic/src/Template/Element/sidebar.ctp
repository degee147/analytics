<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="nav-item start <?=$page == "dashboard" ? 'active open' : ''?>">
                <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Dashboard', 'action' => 'index'])?>" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span> 
                    <span class="selected"></span>
                </a>

            </li>
            <li class="nav-item <?=$page == "howBusy" || $page=="failingTransactions" || $page=="lostRevenue" || $page=="customerTransactions"  || $page=="mostFrequentMerchants"? 'active open' : '' ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-notebook"></i>
                    <span class="title">POS Channels</span>
                    <span class="arrow <?=$page == "howBusy" || $page=="failingTransactions" || $page=="lostRevenue" || $page=="customerTransactions"  || $page=="mostFrequentMerchants" ? "open" : "" ?>"></span>
                </a>
                <ul class="sub-menu" style="display:<?=$page == "howBusy" || $page=="failingTransactions" || $page=="lostRevenue" || $page=="customerTransactions"  || $page=="mostFrequentMerchants"  ? 'block'
                    : '' ?>;"> 
					
                    <li <?=$page=="howBusy" ? 'class="active"' : '' ?>>
                        <?=$this->Html->link('<i class="icon-equalizer"></i> How Busy', ['prefix' => false, 'controller' => 'Pos', 'action' => 'howBusy'], ['escape' => false]);?>
                    </li>
                    <li <?=$page=="failingTransactions" ? 'class="active"' : '' ?>>
                        <?=$this->Html->link('<i class="icon-equalizer"></i> Failing Transactions', ['prefix' => false, 'controller' => 'Pos', 'action' => 'failingTransactions'], ['escape' => false]);?>
                    </li>
                    <li <?=$page=="lostRevenue" ? 'class="active"' : '' ?>>
                        <?=$this->Html->link('<i class="icon-equalizer"></i> Lost Revenue', ['prefix' => false, 'controller' => 'Pos', 'action' => 'lostRevenue'], ['escape' => false]);?>
                    </li>
                    <li <?=$page=="customerTransactions" ? 'class="active"' : '' ?>>
                        <?=$this->Html->link('<i class="icon-equalizer"></i> Customer Transactions', ['prefix' => false, 'controller' => 'Pos', 'action' => 'customerTransactions'], ['escape' => false]);?>
                    </li>
                    <li <?=$page=="mostFrequentMerchants" ? 'class="active"' : '' ?>>
                        <?=$this->Html->link('<i class="icon-equalizer"></i> Frequent Merchants', ['prefix' => false, 'controller' => 'Pos', 'action' => 'mostFrequentMerchants'], ['escape' => false]);?>
                    </li>

                </ul>
            </li>
            <li class="nav-item <?=$page == "cardFallbacks" || $page=="excessiveTransactions"  || $page=="flaggedCustomers"  || $page=="unusualTransactions" ? 'active open' : '' ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-notebook"></i>
                    <span class="title">Fraud Analysis</span>
                    <span class="arrow <?=$page == "cardFallbacks" || $page=="excessiveTransactions"  || $page=="flaggedCustomers"  || $page=="unusualTransactions"  ? "open" : "" ?>"></span>
                </a>
                <ul class="sub-menu" style="display:<?=$page == "cardFallbacks" || $page=="excessiveTransactions"  || $page=="flaggedCustomers"  || $page=="unusualTransactions"  ? 'block' : '' ?>;">
                    <li <?=$page=="cardFallbacks" ? 'class="active"' : '' ?>>
                        <?=$this->Html->link('<i class="icon-equalizer"></i> Card Fallbacks', ['prefix' => false, 'controller' => 'FraudAnalysis', 'action' => 'cardFallbacks'], ['escape' => false]);?>
                    </li>
                    <li <?=$page=="excessiveTransactions" ? 'class="active"' : '' ?>>
                        <?=$this->Html->link('<i class="icon-equalizer"></i> Excessive Transactions', ['prefix' => false, 'controller' => 'FraudAnalysis', 'action' => 'excessiveTransactions'], ['escape' => false]);?>
                    </li>
                    <!-- <li <?=$page=="unusualTransactions" ? 'class="active"' : '' ?>>
                        <?=$this->Html->link('<i class="icon-equalizer"></i> Unusual Transactions', ['prefix' => false, 'controller' => 'FraudAnalysis', 'action' => 'unusualTransactions'], ['escape' => false]);?>
                    </li> -->
                    <li <?=$page=="flaggedCustomers" ? 'class="active"' : '' ?>>
                        <?=$this->Html->link('<i class="icon-equalizer"></i> Flagged Customers', ['prefix' => false, 'controller' => 'FraudAnalysis', 'action' => 'flaggedCustomers'], ['escape' => false]);?>
                    </li>
                </ul>
            </li>
            <li class="nav-item <?=$page == "atm_profile" || $page=="atm_performance"  || $page=="atm_placement"  || $page=="atm_failures"  || $page=="atm_anaylzer" ? 'active open' : '' ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-notebook"></i>
                    <span class="title">ATM Channels</span>
                    <span class="arrow <?=$page == "atm_profile" || $page=="atm_performance"  || $page=="atm_placement"  || $page=="atm_failures"  || $page=="atm_anaylzer" ? "open" : "" ?>"></span>
                </a>
                <ul class="sub-menu" style="display:<?=$page == "atm_profile" || $page=="atm_performance"  || $page=="atm_placement"  || $page=="atm_failures"  || $page=="atm_anaylzer" ? 'block'
                    : '' ?>;">
                    <li <?=$page=="atm_profile" ? 'class="active"' : '' ?>>
                        <?=$this->Html->link('<i class="icon-equalizer"></i> Profile', ['prefix' => false, 'controller' => 'AtmChannels', 'action' => 'profile'], ['escape' => false]);?>
                    </li>
                    <li <?=$page=="atm_performance" ? 'class="active"' : '' ?>>
                        <?=$this->Html->link('<i class="icon-equalizer"></i> Performance', ['prefix' => false, 'controller' => 'AtmChannels', 'action' => 'performance'], ['escape' => false]);?>
                    </li>
                    <li <?=$page=="atm_placement" ? 'class="active"' : '' ?>>
                        <?=$this->Html->link('<i class="icon-equalizer"></i> Placement', ['prefix' => false, 'controller' => 'AtmChannels', 'action' => 'placement'], ['escape' => false]);?>
                    </li>
                    <li <?=$page=="atm_failures" ? 'class="active"' : '' ?>>
                        <?=$this->Html->link('<i class="icon-equalizer"></i> Failures', ['prefix' => false, 'controller' => 'AtmChannels', 'action' => 'failures'], ['escape' => false]);?>
                    </li>
                    <li <?=$page=="atm_anaylzer" ? 'class="active"' : '' ?>>
                        <?=$this->Html->link('<i class="icon-equalizer"></i> Transaction Analyzer', ['prefix' => false, 'controller' => 'AtmChannels', 'action' => 'transactionAnalyzer'], ['escape' => false]);?>
                    </li>

                </ul>
            </li>
            <li class="nav-item <?=$page == "digital_performance" || $page=="digital_mobile" ? 'active open' : '' ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-notebook"></i>
                    <span class="title">Digital Banking</span>
                    <span class="arrow <?=$page == "digital_performance" || $page=="digital_mobile" ? "open" : "" ?>"></span>
                </a>
                <ul class="sub-menu" style="display:<?=$page == "digital_performance" || $page=="digital_mobile" ? 'block'
                    : '' ?>;">
                    <li <?=$page=="digital_performance" ? 'class="active"' : '' ?>>
                        <?=$this->Html->link('<i class="icon-equalizer"></i> Performance', ['prefix' => false, 'controller' => 'DigitalBanking', 'action' => 'performance'], ['escape' => false]);?>
                    </li>
                    <li <?=$page=="digital_mobile" ? 'class="active"' : '' ?>>
                        <?=$this->Html->link('<i class="icon-equalizer"></i> Mobile Transactions', ['prefix' => false, 'controller' => 'DigitalBanking', 'action' => 'mobileTransactions'], ['escape' => false]);?>
                    </li>

                </ul>
            </li>
            <li class="nav-item <?=$page == "card_performance" || $page=="top_merchants" || $page=="customer_frustrations" || $page=="customer_locations"|| $page=="card_transactions" ? 'active open' : '' ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-notebook"></i>
                    <span class="title">Card Usage</span>
                    <span class="arrow <?=$page == "card_performance" || $page=="top_merchants" || $page=="customer_frustrations" || $page=="customer_locations"|| $page=="card_transactions"  ? "open" : "" ?>"></span>
                </a>
                <ul class="sub-menu" style="display:<?=$page == "card_performance" || $page=="top_merchants" || $page=="customer_frustrations" || $page=="customer_locations"|| $page=="card_transactions" ? 'block'
                    : '' ?>;">
                    <li <?=$page=="card_performance" ? 'class="active"' : '' ?>>
                        <?=$this->Html->link('<i class="icon-equalizer"></i> Performance', ['prefix' => false, 'controller' => 'CardUsage', 'action' => 'performance'], ['escape' => false]);?>
                    </li>
                    <li <?=$page=="top_merchants" ? 'class="active"' : '' ?>>
                        <?=$this->Html->link('<i class="icon-equalizer"></i> Top Merchants', ['prefix' => false, 'controller' => 'CardUsage', 'action' => 'topMerchants'], ['escape' => false]);?>
                    </li>
                    <li <?=$page=="customer_frustrations" ? 'class="active"' : '' ?>>
                        <?=$this->Html->link('<i class="icon-equalizer"></i> Customer Frustrations', ['prefix' => false, 'controller' => 'CardUsage', 'action' => 'customerFrustrations'], ['escape' => false]);?>
                    </li>
                    <!-- <li <?=$page=="customer_locations" ? 'class="active"' : '' ?>>
                        <?=$this->Html->link('<i class="icon-equalizer"></i> Customer Locations', ['prefix' => false, 'controller' => 'CardUsage', 'action' => 'customerLocations'], ['escape' => false]);?>
                    </li> -->
                    <li <?=$page=="card_transactions" ? 'class="active"' : '' ?>>
                        <?=$this->Html->link('<i class="icon-equalizer"></i> Customer Transactions', ['prefix' => false, 'controller' => 'CardUsage', 'action' => 'customerTransactions'], ['escape' => false]);?>
                    </li>
                </ul>
            </li>
           
            <!-- <li class="nav-item <?=$page == "all_performance" || $page=="all_transactions" ? 'active open' : '' ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-notebook"></i>
                    <span class="title">All Channels</span>
                    <span class="arrow <?=$page == "all_performance" || $page=="all_transactions" ? "open" : "" ?>"></span>
                </a>
                <ul class="sub-menu" style="display:<?=$page == "all_performance" || $page=="all_transactions" ? 'block'
                    : '' ?>;">
                    <li <?=$page=="all_performance" ? 'class="active"' : '' ?>>
                        <?=$this->Html->link('<i class="icon-equalizer"></i> Performance', ['prefix' => false, 'controller' => 'AllChannels', 'action' => 'performance'], ['escape' => false]);?>
                    </li>
                    <li <?=$page=="all_transactions" ? 'class="active"' : '' ?>>
                        <?=$this->Html->link('<i class="icon-equalizer"></i> Customer Transactions', ['prefix' => false, 'controller' => 'AllChannels', 'action' => 'customerTransactions'], ['escape' => false]);?>
                    </li>

                </ul>
            </li>
            -->
			<li class="nav-item start <?=$page == "notifications" ? 'active open' : ''?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-bell"></i>
					<span class="badge bg-blue-chambray"> 20 </span>
                    <span class="title">Notifications </span>
                    <span class="selected"></span>
                </a>
            </li>
           
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->
