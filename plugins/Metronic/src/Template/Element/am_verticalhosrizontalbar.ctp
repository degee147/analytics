<script>
    function startVerticalHorizontalBarChart(div, rotation = false, multicolor = true) {

        var chart = AmCharts.makeChart(div, {
            "type": "serial",
            "theme": "light",
            "dataProvider": [{
                "country": "vhg",
                "color": "#E87E04",
                "visits": 2025
            }, {
                "country": "pxa",
                "color": multicolor == false ? "#E87E04": "#1BBC9B",
                "visits": 1882
            }, {
                "country": "pqh",
                "color": "#E87E04",
                "visits": 1809
            }, {
                "country": "hvv",
                "color": "#E87E04",
                "visits": 1322
            }, {
                "country": "alq",
                "color": "#E87E04",
                "visits": 1122
            }, {
                "country": "zob",
                "color": "#E87E04",
                "visits": 1114
            }, {
                "country": "rsc",
                "color": "#E87E04",
                "visits": 984
            }, {
                "country": "toh",
                "color": multicolor == false ? "#E87E04": "#1BBC9B",
                "visits": 711
            }, {
                "country": "yae",
                "color": "#E87E04",
                "visits": 665
            }, {
                "country": "uut",
                "color": multicolor == false ? "#E87E04": "#1BBC9B",
                "visits": 580
            }, {
                "country": "qzi",
                "color": "#E87E04",
                "visits": 443
            }, {
                "country": "exl",
                "color": multicolor == false ? "#E87E04": "#1BBC9B",
                "visits": 441
            }, {
                "country": "cwf",
                "color": multicolor == false ? "#E87E04": "#1BBC9B",
                "visits": 395
            }],
            "valueAxes": [{
                "gridColor": "#FFFFFF",
                // "gridColor": "#3B3F51",
                "gridAlpha": 0.2,
                "dashLength": 0
            }],
            "gridAboveGraphs": true, 
            "startDuration": 1,
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.8,
                "fillColorsField": "color",
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "visits"
            }],
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "country",
            "categoryAxis": {
                "gridPosition": "start",
                "gridAlpha": 0,
                "tickPosition": "start",
                "tickLength": 20
            },
            "rotate": rotation,
            "export": {
                "enabled": true
            },


        });
    }

</script>
