
<script>

    $(document).ready(function() {

        // see more options here: http: //bootstrap-table.wenzhixin.net.cn/documentation/
        $('#<?=$tableName?>').bootstrapTable({
            //minimumCountColumns: 30,
            //sortable: true,
            //striped: true,
            //iconsPrefix: 'glyphicon',
            //pageNumber: 1,
            //pageSize:10,
            //showHeader: false,
            //showRefresh: true,
            //showToggle: true,
            classes: 'table table-striped table-bordered table-hover',
            showExport: true,
            showColumns:true,
            rememberOrder: true,
            pageList: [5, 10, 25, 50, 100, 200],
            showPaginationSwitch: true,
            search: true,
            pagination: true,
            exportOptions: {
                fileName: '<?=$infoText?>',
                //ignoreColumn: [0,14]
            },
            formatNoMatches: function () {
                return 'No '+ singularise(0, "<?=$infoText?>") +' found';
            },
            formatDetailPagination: function (totalRows) {
                return 'Showing '+ totalRows +' '+ singularise(totalRows, "<?=$infoText?>");
            },
            formatShowingRows:  function (pageFrom, pageTo, totalRows) {
                return 'Showing '+ pageFrom + ' to '+ pageTo + ' of '+totalRows+' '+ singularise(totalRows, "<?=$infoText?>");
            },
            formatRecordsPerPage: function (pageNumber) {
                return pageNumber +' '+ singularise(pageNumber, "<?=$infoText?>") +' per page';
            }
        });


    });
</script>