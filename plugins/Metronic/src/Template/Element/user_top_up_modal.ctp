<div class="modal fade" id="topUpModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="exampleModalLabel">Top up user account - Current Balance:
                    <?=$currency?>
                        <?=number_format($user->balance)?>
                </h4>
            </div>
            <?=$this->Form->create($user, ['url' => ['prefix' => false, 'controller' => 'GeneralActions', 'action' => 'topUpUserBalance'], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data'])?>

                <div class="modal-body">

                    <?=$this->Form->hidden('id');?>

                        <div class="form-group form-md-line-input">
                            <label class="col-md-3 control-label"> Amount
                                <span class="required" aria-required="true"> * </span>
                            </label>
                            <div class="col-md-6">
                                <div class="col-md-1">
                                    <div class="input-icon">
                                        <i class="fa fa-rupee"></i>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <?=$this->Form->control('balance', ['templates' => ['inputContainer' => '{{content}}'], 'label' => false, 'placeholder' => 'Enter amount...', 'class' => 'form-control', "id" => "amount", 'value' => 500]);?>
                                    <div class="form-control-focus"></div>
                                    <span class="help-block">Enter amount </span>
                                    <div class="error-message"></div>
                                </div>
                            </div>
                        </div>

                      <div class="form-group form-md-line-input">
                          <label class="col-md-3 control-label" for="method">Method<span class="required" aria-required="true"> * </span></label>
                            <div class="col-md-6">

                                <div class="col-md-10 col-md-offset-1">
                                    <?=$this->Form->control('method', ['templates' => ['inputContainer' => '{{content}}'], 'options' => ['Credit Card' => 'Credit card', 'Paypal' => 'PayPal', 'Skrill' => 'Skrill', 'Bank Deposit' => 'Bank Deposit'], 'empty' => true, 'label' => false, 'class' => 'form-control', "required"]);?>
                                    <div class="form-control-focus"></div>
                                </div>
                            </div>
                        </div>



                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-circle" data-dismiss="modal">Close</button>
                    <?=$this->Form->button(__('Submit'), ['type' => 'submit', 'class' => ['btn btn-circle purple']]);?>
                </div>
                <?=$this->Form->end()?>
        </div>
    </div>
</div>
