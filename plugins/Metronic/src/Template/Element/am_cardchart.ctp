
<!-- Chart code -->
<script>
function initCardChart(div){
    var chart = AmCharts.makeChart(div, {
    "pathToImages": App.getGlobalPluginsPath() + "amcharts/amcharts/images/",
    "type": "serial",
    "theme": "light",
    "marginRight":30,
    "legend": {
        "equalWidths": false,
        "periodValueText": "total: [[value.sum]]",
        "position": "top",
        "valueAlign": "left",
        "valueWidth": 100
    },
    "dataProvider": [{
        "year": 1994,
        "cars": 1587,
        "payments": 650,
        "refunds": 121
    }, {
        "year": 1995,
        "cars": 1567,
        "payments": 683,
        "refunds": 146
    }, {
        "year": 1996,
        "cars": 1617,
        "payments": 691,
        "refunds": 138
    }, {
        "year": 1997,
        "cars": 1630,
        "payments": 642,
        "refunds": 127
    }, {
        "year": 1998,
        "cars": 1660,
        "payments": 699,
        "refunds": 105
    }, {
        "year": 1999,
        "cars": 1683,
        "payments": 721,
        "refunds": 109
    }, {
        "year": 2000,
        "cars": 1691,
        "payments": 737,
        "refunds": 112
    }, {
        "year": 2001,
        "cars": 1298,
        "payments": 680,
        "refunds": 101
    }, {
        "year": 2002,
        "cars": 1275,
        "payments": 664,
        "refunds": 97
    }, {
        "year": 2003,
        "cars": 1246,
        "payments": 648,
        "refunds": 93
    }, {
        "year": 2004,
        "cars": 1318,
        "payments": 697,
        "refunds": 111
    }, {
        "year": 2005,
        "cars": 1213,
        "payments": 633,
        "refunds": 87
    }, {
        "year": 2006,
        "cars": 1199,
        "payments": 621,
        "refunds": 79
    }, {
        "year": 2007,
        "cars": 1110,
        "payments": 210,
        "refunds": 81
    }, {
        "year": 2008,
        "cars": 1165,
        "payments": 232,
        "refunds": 75
    }, {
        "year": 2009,
        "cars": 1145,
        "payments": 219,
        "refunds": 88
    }, {
        "year": 2010,
        "cars": 1163,
        "payments": 201,
        "refunds": 82
    }, {
        "year": 2011,
        "cars": 1180,
        "payments": 285,
        "refunds": 87
    }, {
        "year": 2012,
        "cars": 1159,
        "payments": 277,
        "refunds": 71
    }],
    "valueAxes": [{
        "stackType": "regular",
        "gridAlpha": 0.07,
        "position": "left",
        "title": "Card Transactions"
    }],
    "graphs": [{
        "balloonText": "<img src='https://www.amcharts.com/lib/3/images/car.png' style='vertical-align:bottom; margin-right: 10px; width:28px; height:21px;'><span style='font-size:14px; color:#000000;'><b>[[value]]</b></span>",
        "fillAlphas": 0.6,
        "hidden": true,
        "lineAlpha": 0.4,
        "title": "Other",
        "valueField": "cars"
    }, {
        "balloonText": "<img src='https://www.amcharts.com/lib/3/images/motorcycle.png' style='vertical-align:bottom; margin-right: 10px; width:28px; height:21px;'><span style='font-size:14px; color:#000000;'><b>[[value]]</b></span>",
        "fillAlphas": 0.6,
        "lineAlpha": 0.4,
        "title": "payments",
        "valueField": "payments"
    }, {
        "balloonText": "<img src='https://www.amcharts.com/lib/3/images/bicycle.png' style='vertical-align:bottom; margin-right: 10px; width:28px; height:21px;'><span style='font-size:14px; color:#000000;'><b>[[value]]</b></span>",
        "fillAlphas": 0.6,
        "lineAlpha": 0.4,
        "title": "refunds",
        "valueField": "refunds"
    }],
    "plotAreaBorderAlpha": 0,
    "marginTop": 10,
    "marginLeft": 0,
    "marginBottom": 0,
    "chartScrollbar": {},
    "chartCursor": {
        "cursorAlpha": 0
    },
    "categoryField": "year",
    "categoryAxis": {
        "startOnAxis": true,
        "axisColor": "#DADADA",
        "gridAlpha": 0.07,
        "title": "Year",
        "guides": [{
            category: "2001",
            toCategory: "2003",
            lineColor: "#CC0000",
            lineAlpha: 1,
            fillAlpha: 0.2,
            fillColor: "#CC0000",
            dashLength: 2,
            inside: true,
            labelRotation: 90,
            label: "fines for max limit introduced"
        }, {
            category: "2007",
            lineColor: "#CC0000",
            lineAlpha: 1,
            dashLength: 2,
            inside: true,
            labelRotation: 90,
            label: "withdrawal fee introduced"
        }]
    },
    "export": {
    	"enabled": true
     }
});
}
</script>