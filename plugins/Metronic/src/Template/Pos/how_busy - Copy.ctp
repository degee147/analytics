<?php $this->assign('title', 'How Busy | POS ');?>
<!-- BEGIN PAGE HEAD-->
<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>How Busy
            <small>How busy are POS terminals</small>
        </h1>
    </div>
    <!-- END PAGE TITLE -->

</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <?=$this->Html->link('Dashboard', ['prefix' => false, 'controller' => 'dashboard', 'action' => 'index'], ['escape' => false]);?>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        POS
        <i class="fa fa-circle"></i>
    </li>
    <li>
        How Busy
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->
<style>

    .map-marker {
    /* adjusting for the marker dimensions
    so that it is centered on coordinates */
    margin-left: -8px;
    margin-top: -8px;
}
.map-marker.map-clickable {
    cursor: pointer;
}
.pulse {
    width: 10px;
    height: 10px;
    border: 5px solid #f7f14c;
    -webkit-border-radius: 30px;
    -moz-border-radius: 30px;
    border-radius: 30px;
    background-color: #716f42;
    z-index: 10;
    position: absolute;
  }
.map-marker .dot {
    border: 10px solid #fff601;
    background: transparent;
    -webkit-border-radius: 60px;
    -moz-border-radius: 60px;
    border-radius: 60px;
    height: 50px;
    width: 50px;
    -webkit-animation: pulse 3s ease-out;
    -moz-animation: pulse 3s ease-out;
    animation: pulse 3s ease-out;
    -webkit-animation-iteration-count: infinite;
    -moz-animation-iteration-count: infinite;
    animation-iteration-count: infinite;
    position: absolute;
    top: -20px;
    left: -20px;
    z-index: 1;
    opacity: 0;
  }
  @-moz-keyframes pulse {
   0% {
      -moz-transform: scale(0);
      opacity: 0.0;
   }
   25% {
      -moz-transform: scale(0);
      opacity: 0.1;
   }
   50% {
      -moz-transform: scale(0.1);
      opacity: 0.3;
   }
   75% {
      -moz-transform: scale(0.5);
      opacity: 0.5;
   }
   100% {
      -moz-transform: scale(1);
      opacity: 0.0;
   }
  }
  @-webkit-keyframes "pulse" {
   0% {
      -webkit-transform: scale(0);
      opacity: 0.0;
   }
   25% {
      -webkit-transform: scale(0);
      opacity: 0.1;
   }
   50% {
      -webkit-transform: scale(0.1);
      opacity: 0.3;
   }
   75% {
      -webkit-transform: scale(0.5);
      opacity: 0.5;
   }
   100% {
      -webkit-transform: scale(1);
      opacity: 0.0;
   }
  }
</style>

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN ROW -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN CHART PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>
                            <span class="caption-subject bold uppercase font-green-haze"> How Busy</span>
                            <span class="caption-helper">How busy are POS terminals</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"> </a>
                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                            <a href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="fullscreen"> </a>
                            <a href="javascript:;" class="remove"> </a>
                        </div>
                    </div>
                    <div class="portlet-body">


                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group form-md-line-input form-md-floating-label has-info">
                                            <select class="form-control" id="form_control_1">
                                                <option value=""></option>
                                                <option value="1">All</option>
                                                <option value="2">Lagos</option>
                                                <option value="3">Port Harcourt</option>
                                                <option value="4">Abuja</option>
                                                <option value="4">Etc</option>
                                            </select>
                                            <label for="form_control_1"><span style="font-size: .75em;">Displayed POS
                                                    on Map</span></label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-md-line-input form-md-floating-label has-info">
                                            <select class="form-control" id="form_control_1">
                                                <option value=""></option>
                                                <option value="1">All</option>
                                                <option value="2">Airlines</option>
                                                <option value="3">Pharmacies</option>
                                                <option value="4">Etc</option>
                                            </select>
                                            <label for="form_control_1"><span style="font-size: .75em;">Merchant
                                                    Categories</span></label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <!-- <div class="form-group">
                                            <div class="input-group input-large date-picker input-daterange" data-date="10/11/2012"
                                                data-date-format="mm/dd/yyyy" style="width: 100% !important;">
                                                <input type="text" class="form-control" name="from">
                                                <span class="input-group-addon"> to </span>
                                                <input type="text" class="form-control" name="to">
                                            </div>
                                            <span class="help-block"> Date range </span>
                                        </div> -->
                                        <!-- <div id="reportrange" class="btn default" style="width: 100%;">
                                            <i class="fa fa-calendar"></i> &nbsp;
                                            <span> </span>
                                            <b class="fa fa-angle-down"></b>
                                        </div> -->
                                        <div class="input-group" id="defaultrange" style="width: 100%;">
                                            <input type="text" class="form-control">
                                            <span class="input-group-btn">
                                                <button class="btn default date-range-toggle" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <span class="help-block"> Date range </span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- <div id="capitalsdiv" class="" style="height: 400px;width: 100%;"> </div> -->
                                        <!-- <div id="countriesMap" class="chart" style="height: 400px;"> </div> -->
                                        <div id="countryMapDiv" class="chart" style="height: 400px;"> </div>
                                        <!-- <div id="chart_10"></div> -->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p style="margin-bottom: 1px;">Merchant Categories</p>
                                        <div class="row">
                                            <div class="table-scrollable" style="display: inline-flex;">
                                                <div class="col-md-3"><span class="badge bg-blue badge-roundless"> 3
                                                    </span> <br> Airlines</div>
                                                <div class="col-md-3"><span class="badge bg-green-meadow badge-roundless">
                                                        3 </span><br>Automobiles</div>
                                                <div class="col-md-3"><span class="badge bg-purple badge-roundless"> 3
                                                    </span><br>Supermarkets</div>
                                                <div class="col-md-3"><span class="badge bg-red badge-roundless"> 3
                                                    </span><br>Stores</div>
                                                <div class="col-md-3"><span class="badge badge-default badge-roundless">
                                                        3 </span><br><span style="white-space: nowrap;">Drug Stores</span></div>
                                                <div class="col-md-3"><span class="badge badge-default badge-roundless">
                                                        3 </span><br>Pharmacy</div>
                                                <div class="col-md-3"><span class="badge badge-default badge-roundless">
                                                        3 </span><br><span style="white-space: nowrap;"> Express Way</span></div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group form-md-line-input form-md-floating-label has-info">
                                            <select class="form-control" id="form_control_1">
                                                <option value=""></option>
                                                <option value="1">All</option>
                                                <option value="2">Option 2</option>
                                                <option value="3">Option 3</option>
                                                <option value="4">Option 4</option>
                                                <option value="4">Etc</option>
                                            </select>
                                            <label for="form_control_1"><span style="font-size: .75em;">Transaction
                                                    Type</span></label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-md-line-input form-md-floating-label has-info">
                                            <select class="form-control" id="form_control_1">
                                                <option value=""></option>
                                                <option value="">Yearly</option>
                                                <option value="1">Monthly</option>
                                                <option value="2">Weekly</option>
                                                <option value="3">Daily</option>
                                                <option value="4">Etc</option>
                                            </select>
                                            <label for="form_control_1"><span style="font-size: .75em;">View Type</span></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p>Transactions for Selected POS Terminal</p>
                                        <div id="threelines" style="height: 350px;width	: 100%;"></div>
                                        <p>Selected Target Average: &#8358;7,000</p>
                                    </div>
                                    <!-- <div class="col-md-12">
                                        <div id="chart_1" class="chart" style="height: 350px;"> </div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CHART PORTLET-->
            </div>
        </div>
        <!-- END ROW -->


    </div>
</div>
<!-- END PAGE BASE CONTENT -->
<?php //echo $this->element('am_script')?>
<?php //echo $this->element('am_map10_latlong')?>
<?php //echo $this->element('am_capitals')?>
<?php echo $this->element('am_countries')?>
<?php echo $this->element('am_3lines')?>
<?php echo $this->element('date_range_script')?>
<?php echo $this->element('am_customhtmlmarkers')?>
<!-- <script src="https://www.amcharts.com/lib/3/maps/js/worldLow.js"></script> -->
<script>
    jQuery(document).ready(function () {

        //initChartSample10();
        // initCountryMap();
        //startCountriesMap("countryMapDiv");
        startCustomHTMLMap("countryMapDiv");
        //startCountriesMap("countryMapDiv", "percent");
        start3LinesChart('threelines');
        initDateRange('#defaultrange');

        var colors = ['#3598DC', '#1BBC9B', '#8E44AD', '#E7505A'];
        
        //document.getElementById('title').style.color = random_color;

        setTimeout(function(){ 

            //console.log("yes");
        
            //demo that gives all blinking dots random color
            $(".pulse").each(function (index) {
            //console.log( index + ": " + $( this ).text() );
            var random_color = colors[Math.floor(Math.random() * colors.length)];
            //console.log(this);
            //console.log(random_color);
            $(this).css('border-color', random_color);
        });

        }, 10000);

        

    });

</script>
