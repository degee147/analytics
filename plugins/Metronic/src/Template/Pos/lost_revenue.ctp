<?php $this->assign('title', 'Lost Revenue | POS ');?>

<!-- BEGIN PAGE HEAD-->
<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Lost POS Revenue
            <small>How much revenue have is lost due to incomplete transactions or availability issues</small>
        </h1>
    </div>
    <!-- END PAGE TITLE -->
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <?=$this->Html->link('Dashboard', ['prefix' => false, 'controller' => 'dashboard', 'action' => 'index'], ['escape' => false]);?>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        POS
        <i class="fa fa-circle"></i>
    </li>
    <li>
        Lost Revenue
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- BEGIN PAGE BASE CONTENT -->

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN ROW -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN CHART PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>
                            <span class="caption-subject bold uppercase font-green-haze"> Lost Revenue</span>
                            <span class="caption-helper">How much revenue have is lost due to incomplete transactions
                                or availability issues</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="fullscreen"> </a>
                        </div>
                    </div>
                    <div class="portlet-body">

                        <div class="row">
                            <div class="col-md-2" style="text-align: right;">
                                <label for="">Date Range</label>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group" id="defaultrange" style="width: 100%;">
                                    <input type="text" class="form-control">
                                    <span class="input-group-btn">
                                        <button class="btn default date-range-toggle" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                                <!-- <span class="help-block"> Date range </span> -->
                            </div>
                            <div class="col-md-4" style="text-align: right;">
                                <label for=""> <strong> Total Lost Revenue:</strong>
                                    <?=$naira?>45,324,789</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="chart3d" class="chart" style="height: 300px;width: 100%;"> </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h5>Lost Revenue Over Time</h5>
                                <div id="chartlinescrollzoom" class="chart" style="height: 200px;width: 100%;"> </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h5>Transactions Over Time</h5>
                                <div id="chartlinescrollzoom2" class="chart" style="height: 200px;width: 100%;"> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CHART PORTLET-->
            </div>
        </div>
        <!-- END ROW -->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->
<?php echo $this->element('date_range_script')?>
<?php echo $this->element('am_3dbarchart')?>
<?php echo $this->element('am_linewithscrollandzoom')?>
<script>
    jQuery(document).ready(function () {

        //initChartSample10();
        // initCountryMap();
        //startCountriesMap("countryMapDiv");
        //startCountriesMap("countryMapDiv", "percent");
        //start3LinesChart('threelines');
        initDateRange('#defaultrange', "long");
        start3DBarChart('chart3d');
        startLineChartWithScrollAndZoom('chartlinescrollzoom');
        startLineChartWithScrollAndZoom('chartlinescrollzoom2','transactions');

    });

</script>
