<?php $this->assign('title', 'Failing Transactions | POS ');?>
<!-- BEGIN PAGE HEAD-->
<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>POS Failing Transactions
            <small>Why are card transactions at the POS failing</small>
        </h1>
    </div>
    <!-- END PAGE TITLE -->

</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <?=$this->Html->link('Dashboard', ['prefix' => false, 'controller' => 'dashboard', 'action' => 'index'], ['escape' => false]);?>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        POS
        <i class="fa fa-circle"></i>
    </li>
    <li>
        Failing Transactions
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN ROW -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN CHART PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>
                            <span class="caption-subject bold uppercase font-green-haze"> Failing Transactions</span>
                            <span class="caption-helper">Why are card transactions at the POS failing</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"> </a>
                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                            <a href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="fullscreen"> </a>
                            <a href="javascript:;" class="remove"> </a>
                        </div>
                    </div>
                    <div class="portlet-body">


                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group form-md-line-input form-md-floating-label has-info">
                                            <select class="form-control" id="form_control_1">
                                                <option value=""></option>
                                                <option value="1">All</option>
                                                <option value="2">Lagos</option>
                                                <option value="3">Port Harcourt</option>
                                                <option value="4">Abuja</option>
                                                <option value="4">Etc</option>
                                            </select>
                                            <label for="form_control_1"><span style="font-size: .75em;">Displayed POS
                                                    on Map</span></label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-md-line-input form-md-floating-label has-info">
                                            <select class="form-control" id="form_control_1">
                                                <option value=""></option>
                                                <option value="1">All</option>
                                                <option value="2">Airlines</option>
                                                <option value="3">Pharmacies</option>
                                                <option value="4">Etc</option>
                                            </select>
                                            <label for="form_control_1"><span style="font-size: .75em;">Merchant
                                                    Categories</span></label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <!-- <div class="form-group">
                                            <div class="input-group input-large date-picker input-daterange" data-date="10/11/2012"
                                                data-date-format="mm/dd/yyyy" style="width: 100% !important;">
                                                <input type="text" class="form-control" name="from">
                                                <span class="input-group-addon"> to </span>
                                                <input type="text" class="form-control" name="to">
                                            </div>
                                            <span class="help-block"> Date range </span>
                                        </div> -->
                                        <!-- <div id="reportrange" class="btn default" style="width: 100%;">
                                            <i class="fa fa-calendar"></i> &nbsp;
                                            <span> </span>
                                            <b class="fa fa-angle-down"></b>
                                        </div> -->
                                        <div class="input-group" id="defaultrange" style="width: 100%;">
                                            <input type="text" class="form-control">
                                            <span class="input-group-btn">
                                                <button class="btn default date-range-toggle" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <span class="help-block"> Date range </span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- <div id="capitalsdiv" class="" style="height: 400px;width: 100%;"> </div> -->
                                        <!-- <div id="countriesMap" class="chart" style="height: 400px;"> </div> -->
                                        <div id="countryMapDiv" class="chart" style="height: 400px;"> </div>
                                        <!-- <div id="chart_10"></div> -->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p style="margin-bottom: 1px;">Merchant Categories</p>
                                        <div class="row">
                                            <div class="table-scrollable" style="display: inline-flex;">
                                                <div class="col-md-3"><span class="badge bg-blue badge-roundless"> 3
                                                    </span> <br> Airlines</div>
                                                <div class="col-md-3"><span class="badge bg-green-meadow badge-roundless">
                                                        3 </span><br>Automobiles</div>
                                                <div class="col-md-3"><span class="badge bg-purple badge-roundless"> 3
                                                    </span><br>Supermarkets</div>
                                                <div class="col-md-3"><span class="badge bg-red badge-roundless"> 3
                                                    </span><br>Stores</div>
                                                <div class="col-md-3"><span class="badge badge-default badge-roundless">
                                                        3 </span><br><span style="white-space: nowrap;">Drug Stores</span></div>
                                                <div class="col-md-3"><span class="badge badge-default badge-roundless">
                                                        3 </span><br>Pharmacy</div>
                                                <div class="col-md-3"><span class="badge badge-default badge-roundless">
                                                        3 </span><br><span style="white-space: nowrap;"> Express Way</span></div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group form-md-line-input form-md-floating-label has-info">
                                            <select class="form-control" id="form_control_1">
                                                <option value=""></option>
                                                <option value="1">All</option>
                                                <option value="2">Option 2</option>
                                                <option value="3">Option 3</option>
                                                <option value="4">Option 4</option>
                                                <option value="4">Etc</option>
                                            </select>
                                            <label for="form_control_1"><span style="font-size: .75em;">Transaction
                                                    Type</span></label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-md-line-input form-md-floating-label has-info">
                                            <select class="form-control" id="form_control_1">
                                                <option value=""></option>
                                                <option value="">Yearly</option>
                                                <option value="1">Monthly</option>
                                                <option value="2">Weekly</option>
                                                <option value="3">Daily</option>
                                                <option value="4">Etc</option>
                                            </select>
                                            <label for="form_control_1"><span style="font-size: .75em;">View Type</span></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p>Failures for Selected POS Terminal</p>
                                        <div id="threelines" style="height: 250px;width	: 100%;"></div>
                                        <p>Selected Target Average: &#8358;7,000</p>
                                    </div>
                                    <div class="col-md-12">
                                        <p>Top Failures</p>
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th scope="col" style="width:450px !important"> POS Terminal
                                                        </th>
                                                        <th scope="col"> Failure Count </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td> POS34436546 </td>
                                                        <td> 1,200 </td>
                                                    </tr>
                                                    <tr>
                                                        <td> POS777784465 </td>
                                                        <td> 410 </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CHART PORTLET-->
            </div>
        </div>
        <!-- END ROW -->


    </div>
</div>
<!-- END PAGE BASE CONTENT -->
<?php //echo $this->element('am_script')?>
<?php //echo $this->element('am_map10_latlong')?>
<?php //echo $this->element('am_capitals')?>
<?php echo $this->element('am_countries')?>
<?php echo $this->element('am_3lines')?>
<?php echo $this->element('date_range_script')?>
<!-- <script src="https://www.amcharts.com/lib/3/maps/js/worldLow.js"></script> -->
<script>
    jQuery(document).ready(function () {

        //initChartSample10();
        // initCountryMap();
        // startCountriesMap("countryMapDiv");
        startCountriesMap("countryMapDiv", "percent", "Hover to see failure percentage");
        start3LinesChart('threelines');
        initDateRange('#defaultrange');


        
    });

</script>
