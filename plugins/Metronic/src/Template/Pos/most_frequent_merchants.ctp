<?php $this->assign('title', 'Merchant Frequency | POS ');?>

<!-- BEGIN PAGE HEAD-->
<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Most Frequent Merchants
            <small>Which merchants are frequented the most</small>
        </h1>
    </div>
    <!-- END PAGE TITLE -->

</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <?=$this->Html->link('Dashboard', ['prefix' => false, 'controller' => 'dashboard', 'action' => 'index'], ['escape' => false]);?>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        POS
        <i class="fa fa-circle"></i>
    </li>
    <li>
        Most Frequent Merchants
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN ROW -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN CHART PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>
                            <span class="caption-subject bold uppercase font-green-haze"> Most Frequent Merchants</span>
                            <span class="caption-helper">Most Frequent Merchants</span>
                        </div>
                        <div class="tools">                            
                            <a href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="fullscreen"> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="row">
                                <div class="col-md-2" style="text-align: right;">
                                    <label for="">Date Range</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="input-group" id="defaultrange" style="width: 100%;">
                                        <input type="text" class="form-control">
                                        <span class="input-group-btn">
                                            <button class="btn default date-range-toggle" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                    <!-- <span class="help-block"> Date range </span> -->
                                </div>

                            </div> 
                            <div class="row">
                                <div class="col-md-6">
                                    <h4 style="text-align: center;">Customer Transaction Volume</h4>
                                    <div id="chart3dpie" class="chart" style="height: 500px;width:100%"> </div>
                                    <div class="well margin-top-20">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label class="text-left">Top Radius:</label>
                                                <input class="chart_7_chart_input" data-property="topRadius" type="range"
                                                    min="0" max="1.5" value="1" step="0.01" /> </div>
                                            <div class="col-sm-3">
                                                <label class="text-left">Angle:</label>
                                                <input class="chart_7_chart_input" data-property="angle" type="range"
                                                    min="0" max="89" value="30" step="1" /> </div>
                                            <div class="col-sm-3">
                                                <label class="text-left">Depth:</label>
                                                <input class="chart_7_chart_input" data-property="depth3D" type="range"
                                                    min="1" max="120" value="40" step="1" /> </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h4 style="text-align: center;">Customer Transaction Value</h4>
                                    <div id="chart3dpie2" class="chart" style="height: 500px;width:100%"> </div>
                                    <div class="well margin-top-20">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label class="text-left">Top Radius:</label>
                                                <input class="chart_7_chart_input2" data-property="topRadius" type="range"
                                                    min="0" max="1.5" value="1" step="0.01" /> </div>
                                            <div class="col-sm-3">
                                                <label class="text-left">Angle:</label>
                                                <input class="chart_7_chart_input2" data-property="angle" type="range"
                                                    min="0" max="89" value="30" step="1" /> </div>
                                            <div class="col-sm-3">
                                                <label class="text-left">Depth:</label>
                                                <input class="chart_7_chart_input2" data-property="depth3D" type="range"
                                                    min="1" max="120" value="40" step="1" /> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <p style="margin-bottom: 1px;">Merchants</p>
                                    <div class="row">
                                        <div class="table-scrollable" style="display: inline-flex;">
                                            <div class="col-md-3"><span class="badge bg-blue badge-roundless"> 
                                                </span> <br> Air Peace</div>
                                            <div class="col-md-3"><span class="badge bg-green-meadow badge-roundless">
                                                     </span><br>Obi Car Services</div>
                                            <div class="col-md-3"><span class="badge bg-purple badge-roundless"> 
                                                </span><br>AB Cleaners</div>
                                            <div class="col-md-3"><span class="badge bg-red badge-roundless"> 
                                                </span><br>Super Mall</div>
                                            <div class="col-md-3"><span class="badge bg-blue-ebonyclay badge-roundless">
                                                     </span><br>Get Well Pharmacy</div>
                                            <div class="col-md-3"><span class="badge bg-yellow-haze badge-roundless">
                                                     </span><br><span style="white-space: nowrap;"> Brain Child School</span></div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CHART PORTLET-->
            </div>
        </div>
        <!-- END ROW -->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
<?php echo $this->element('am_3dpiechart')?>
<?php echo $this->element('date_range_script')?>
<script>
    jQuery(document).ready(function () {
        initDateRange('#defaultrange', 'long');
        init3dPieChart('chart3dpie', 'chart_7_chart_input');
        init3dPieChart('chart3dpie2', 'chart_7_chart_input2');

    });

</script>
