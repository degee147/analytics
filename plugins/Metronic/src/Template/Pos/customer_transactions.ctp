<?php $this->assign('title', 'Customer Transactions | POS ');?>
<!-- BEGIN PAGE HEAD-->
<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>POS Customer Transactions
            <small>How various customer segments choose to interact</small>
        </h1>
    </div>
    <!-- END PAGE TITLE -->

</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <?=$this->Html->link('Dashboard', ['prefix' => false, 'controller' => 'dashboard', 'action' => 'index'], ['escape' => false]);?>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        POS
        <i class="fa fa-circle"></i>
    </li>
    <li>
        Customer Transactions
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN ROW -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN CHART PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>
                            <span class="caption-subject bold uppercase font-green-haze"> Customer Transactions</span>
                            <span class="caption-helper">How various customer segments choose to interact</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"> </a>
                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                            <a href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="fullscreen"> </a>
                            <a href="javascript:;" class="remove"> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group" id="" style="width: 100%;">
                                            <input type="number" value="14" min="10" max="30" class="form-control">
                                            <span class="input-group-btn">
                                                <button class="btn default date-range-toggle" type="button">
                                                    <i class="fa fa-users"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <span class="help-block"> Customers to Show </span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-md-line-input form-md-floating-label has-info">
                                            <select class="form-control" id="form_control_1">
                                                <option value=""></option>
                                                <option value="1">Card</option>
                                                <option value="2">Bank</option>
                                            </select>
                                            <label for="form_control_1"><span style="font-size: .75em;">Displayed by</span></label>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-scrollable" style="display: inline-flex;">
                                            <div class="" style="width:1600px;">
                                                <h4 style="text-align: center;">Cashback</h4>
                                                <div id="memmbermap" class="chart" style="height: 300px; width:400px">
                                                </div>
                                            </div>
                                            <div class="" style="width:800px;">
                                                <h4 style="text-align: center;">Purchases</h4>
                                                <div id="memmbermap2" class="chart" style="height: 300px; width:400px;">
                                                </div>
                                            </div>
                                            <div class="" style="width:800px;">
                                                <h4 style="text-align: center;">Refunds</h4>
                                                <div id="memmbermap3" class="chart" style="height: 300px; width:400px">
                                                </div>
                                                <!-- <div id="chart_10"></div> -->
                                            </div>
                                            <div class="" style="width:800px;">
                                                <h4 style="text-align: center;">Total Transactions</h4>
                                                <div id="memmbermap4" class="chart" style="height: 300px; width:400px">
                                                </div>
                                                <!-- <div id="chart_10"></div> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- <p style="margin-bottom: 1px;">Color Codes</p> -->
                                        <div class="row">
                                            <div class="table-scrollable" style="display: inline-flex;">
                                                <div class="col-md-6"><span class="badge bg-yellow-gold badge-roundless">
                                                    </span> &nbsp;&nbsp;Master Card</div>
                                                <div class="col-md-6"><span class="badge bg-green-meadow badge-roundless">
                                                    </span> &nbsp;&nbsp;Visa Card</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group" id="defaultrange" style="width: 100%;">
                                            <input type="text" class="form-control">
                                            <span class="input-group-btn">
                                                <button class="btn default date-range-toggle" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <span class="help-block"> Date range </span>
                                    </div>
                                  
                                    <div class="col-md-6">
                                        <div class="form-group form-md-line-input form-md-floating-label has-info">
                                            <select class="form-control" id="form_control_1">
                                                <option value=""></option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">Etc</option>
                                            </select>
                                            <label for="form_control_1"><span style="font-size: .75em;">Hour Selection</span></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>Transactions by amount</h4>
                                        <div id="threelines" style="height: 350px;width	: 100%;"></div>
                                        <!-- <p>Selected Target Average: &#8358;7,000</p> -->
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CHART PORTLET-->
            </div>
        </div>
        <!-- END ROW -->


    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<?php echo $this->element('am_verticalhosrizontalbar')?>
<?php echo $this->element('date_range_script')?>
<?php echo $this->element('am_3lines')?>
<!-- <script src="https://www.amcharts.com/lib/3/maps/js/worldLow.js"></script> -->
<script>
    jQuery(document).ready(function () {

        initDateRange('#defaultrange');

        startVerticalHorizontalBarChart('memmbermap', true);
        startVerticalHorizontalBarChart('memmbermap2', true);
        startVerticalHorizontalBarChart('memmbermap3', true);
        startVerticalHorizontalBarChart('memmbermap4', true);
        start3LinesChart('threelines');



    });

</script>
