<?php $this->assign('title', 'Profile | ATM Channels ');?>
<!-- BEGIN PAGE HEAD-->
<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Profile
            <small>Overall performance of ATM fleet</small>
        </h1>
    </div>
    <!-- END PAGE TITLE -->

</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <?=$this->Html->link('Dashboard', ['prefix' => false, 'controller' => 'dashboard', 'action' => 'index'], ['escape' => false]);?>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        ATM Channels
        <i class="fa fa-circle"></i>
    </li>
    <li>
        Profile
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->
<?= $this->element('am_naija_map_styles')?>
<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN ROW -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN CHART PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>
                            <span class="caption-subject bold uppercase font-green-haze"> Profile</span>
                            <span class="caption-helper">Overall performance of ATM fleet</span>
                        </div>
                        <div class="tools">
                            <!-- <a href="javascript:;" class="collapse"> </a> -->
                            <!-- <a href="#portlet-config" data-toggle="modal" class="config"> </a> -->
                            <a href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="fullscreen"> </a>
                            <!-- <a href="javascript:;" class="remove"> </a> -->
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group" id="defaultrange" style="width: 100%;">
                                            <input type="text" class="form-control">
                                            <span class="input-group-btn">
                                                <button class="btn default date-range-toggle" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <span class="help-block"> Date range </span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-md-line-input form-md-floating-label has-info">
                                            <select class="form-control" id="form_control_1">
                                                <option value=""></option>
                                                <option value="1">ATM Group</option>
                                                <option value="2">Owner</option>
                                            </select>
                                            <label for="form_control_1"><span style="font-size: .75em;">Show By</span></label>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="countryMapDiv" class="chart" style="height: 400px;width:100%"> </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p style="margin-bottom: 1px;">Color Code</p>
                                        <div class="row">
                                            <div class="table-scrollable" style="display: inline-flex;">
                                                <div class="col-md-3"><span class="badge bg-blue badge-roundless">
                                                    </span> <br> Other Banks</div>
                                                <div class="col-md-3"><span class="badge bg-red badge-roundless">
                                                    </span><br>Parent Banks</div>
                                                <div class="col-md-3"><span class="badge bg-purple badge-roundless">
                                                    </span><br>Generic</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">

                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- <p>Selected Target Average: &#8358;7,000</p> -->
                                        <h4 style="text-align: center;">Transaction Volume</h4>
                                        <div id="memmbermap" class="chart" style="height: 400px; width:100%">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- <div id="countryMapDiv" class="chart" style="height: 400px;width:100%"> </div> -->
                                    </div>

                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Number of Transactions Exceeding Specified
                                    <?=$naira?> Amount</h4>

                                    <div id="stackbar1" class="chart" style="height: 400px;width:100%"> </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Transaction Revenue Composition</h4>
                                    <div id="stackbar2" class="chart" style="height: 400px;width:100%"> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CHART PORTLET-->
            </div>
        </div>
        <!-- END ROW -->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->
<?php echo $this->element('date_range_script')?>
<?php echo $this->element('am_customhtmlmarkers')?>
<?php echo $this->element('am_verticalhosrizontalbar')?>
<?php echo $this->element('am_stackedbarchat2')?>
<?php echo $this->element('am_stackedbarchat3')?>
<script>
    jQuery(document).ready(function () {

        initDateRange('#defaultrange');

        //startVerticalHorizontalBarChart('memmbermap', true);
        //startVerticalHorizontalBarChart('memmbermap2', true);
        //start3LinesChart('threelines');

        startCustomHTMLMap("countryMapDiv", null, "ATM Positioning");
        startVerticalHorizontalBarChart('memmbermap', true, false); //true means rotate | false means color off 

        init2StackedBarChart('stackbar1');
        init3StackedBarChart('stackbar2');
        //init4StackedBarChart('stackbar1');

        //startCountriesMap("countryMapDiv", "percent");
        //start3LinesChart('threelines');
        //initTimeBasedMap('timeMapDiv');
        var colors = ['#3598DC', '#1BBC9B', '#8E44AD', '#E7505A'];
        setTimeout(function () {

            //console.log("yes");

            //demo that gives all blinking dots random color
            $(".pulse").each(function (index) {
                //console.log( index + ": " + $( this ).text() );
                var random_color = colors[Math.floor(Math.random() * colors.length)];
                //console.log(this);
                //console.log(random_color);
                $(this).css('border-color', random_color);
            });

        }, 10000);



    });

</script>
