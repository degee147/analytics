<?php $this->assign('title', 'Placement | ATM Channels ');?>
<!-- BEGIN PAGE HEAD-->
<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Placement
            <small>Where should the next ATMs be placed</small>
        </h1>
    </div>
    <!-- END PAGE TITLE -->

</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <?=$this->Html->link('Dashboard', ['prefix' => false, 'controller' => 'dashboard', 'action' => 'index'], ['escape' => false]);?>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        ATM Channels
        <i class="fa fa-circle"></i>
    </li>
    <li>
        Placement
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->
<?= $this->element('am_naija_map_styles')?>
<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN ROW -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN CHART PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>
                            <span class="caption-subject bold uppercase font-green-haze"> Placement</span>
                            <span class="caption-helper">Where should the next ATMs be placed</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="fullscreen"> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div id="countryMapDiv" class="chart" style="height: 400px;width:100%"> </div>
                            </div>
                            <div class="col-md-6">
                                <h4 style="text-align:center">Transactions overview for first Quarter</h4>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th> # </th>
                                                <th> Name </th>
                                                <th> Total </th>
                                                <th> Profit </th>
                                                <th> Loss </th>
                                                <th> Withdrawal % </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td> 1 </td>
                                                <td> ATM0003 </td>
                                                <td> <?=$naira?>270Million </td>
                                                <td> <?=$naira?>35Million </td>
                                                <td> <?=$naira?>12Million </td>
                                                <td> 89 </td>
                                            </tr>
                                            <tr>
                                                <td> 1 </td>
                                                <td> ATM0003 </td>
                                                <td> <?=$naira?>270Million </td>
                                                <td> <?=$naira?>35Million </td>
                                                <td> <?=$naira?>12Million </td>
                                                <td> 89 </td>
                                            </tr>
                                            <tr>
                                                <td> 1 </td>
                                                <td> ATM0003 </td>
                                                <td> <?=$naira?>270Million </td>
                                                <td> <?=$naira?>35Million </td>
                                                <td> <?=$naira?>12Million </td>
                                                <td> 89 </td>
                                            </tr>
                                            <tr>
                                                <td> 1 </td>
                                                <td> ATM0003 </td>
                                                <td> <?=$naira?>270Million </td>
                                                <td> <?=$naira?>35Million </td>
                                                <td> <?=$naira?>12Million </td>
                                                <td> 89 </td>
                                            </tr>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <h4 style="text-align:center">Average Transactions/ATM</h4>
                                <div id="logmap" class="chart" style="height: 400px;width:100%"> </div>
                            </div>
                            <div class="col-md-12">
                                <h4 style="text-align:center">Customer Density</h4>
                                <div id="heatmap" class="chart" style="height: 400px;width:100%"> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CHART PORTLET-->
            </div>
        </div>
        <!-- END ROW -->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
<?php echo $this->element('am_customhtmlmarkers')?>
<?php echo $this->element('am_logarithmic')?>
<?php echo $this->element('am_heatmap')?>
<script>
    jQuery(document).ready(function () {

        //initChartSample10();
        // initCountryMap();
        //startCountriesMap("countryMapDiv");
        startCustomHTMLMap("countryMapDiv", null, "ATM Locations");
        initLogMap('logmap');
        initHeatMap('heatmap');
        //startCountriesMap("countryMapDiv", "percent");
        //start3LinesChart('threelines');

        //initDateRange('#defaultrange', 'long');
        //start3DBarChart('map2');

        //init3DColumnChart('map2');
        //initLogMap('logmap');



    });

</script>
