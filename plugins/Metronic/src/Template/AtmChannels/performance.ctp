<?php $this->assign('title', 'Performance | ATM Channels ');?>
<!-- BEGIN PAGE HEAD-->
<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Performance
            <small>How Profitable are the ATMs</small>
        </h1>
    </div>
    <!-- END PAGE TITLE -->

</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <?=$this->Html->link('Dashboard', ['prefix' => false, 'controller' => 'dashboard', 'action' => 'index'], ['escape' => false]);?>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        ATM Channels
        <i class="fa fa-circle"></i>
    </li>
    <li>
        Performance
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->
<?= $this->element('am_naija_map_styles')?>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN ROW -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN CHART PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>
                            <span class="caption-subject bold uppercase font-green-haze"> Performance</span>
                            <span class="caption-helper">How Profitable are the ATMs</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="fullscreen"> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label has-info">
                                    <select class="form-control" id="form_control_1">
                                        <option value=""></option>
                                        <option value="1">All</option>
                                        <option value="2">By Segment</option>
                                    </select>
                                    <label for="form_control_1"><span style="font-size: .75em;">Display ATMs on Map</span></label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group" id="defaultrange" style="width: 100%;">
                                    <input type="text" class="form-control">
                                    <span class="input-group-btn">
                                        <button class="btn default date-range-toggle" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                                <span class="help-block"> Date range </span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div id="countryMapDiv" class="chart" style="height: 400px;width:100%"> </div>
                            </div>
                            <div class="col-md-6"> 
                                <h4 style="text-align:center">Transactions for Selected ATM</h4>
                                <div id="map2" class="chart" style="height: 400px;width:100%"> </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <h4 style="text-align:center">Average Transactions/ATM</h4>
                                <div id="logmap" class="chart" style="height: 400px;width:100%"> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CHART PORTLET-->
            </div>
        </div>
        <!-- END ROW -->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
<?php echo $this->element('am_customhtmlmarkers')?>
<?php echo $this->element('date_range_script')?>
<?php echo $this->element('am_3dcolumnchart')?>
<?php echo $this->element('am_logarithmic')?>
<script>
    jQuery(document).ready(function () {

        //initChartSample10();
        // initCountryMap();
        //startCountriesMap("countryMapDiv");
        startCustomHTMLMap("countryMapDiv", null, "ATM Locations");
        //startCountriesMap("countryMapDiv", "percent");
        //start3LinesChart('threelines');

        initDateRange('#defaultrange', 'long');
        //start3DBarChart('map2');

        init3DColumnChart('map2');
        initLogMap('logmap');



    });

</script>
